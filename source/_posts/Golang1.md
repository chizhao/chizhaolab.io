---
title: Go语言编程系列(一) - Hello World!
date: 2019-03-02 14:12:29
tags: 
- Go
- 毕业论文
toc: True
---



**毕业设计是与区块链相关的一个项目,打算选用Go语言来实现,该系列文章用来记录Go语言的学习过程.**

Go语言作为一个编译型语言,它给我最大的感受是,拥有Python语法的简洁,C语言的性能.

<!-- more -->


### 开始

[Golang官网](https://golang.org)

[The Go Playground](https://play.golang.org)是Google官方可以在线运行Go代码的平台.

我选用的编辑器为VSCode,此外还有Sublime Text,Golan和LiteIDE等等,
安装不再赘述.

首先说一下Go_Projec的目录结构:

GoProjects是我电脑里面用于存放Go项目的文件夹,它的结构如下图
所示.

![mark](http://image.i-ll.cc/blog/20190303/mPD9EIcocdAT.png)\

### Go-第一个程序

```Go
package main //在Go语言中,每一个程序都属于一个包

import "fmt" //import用于导入外部包,fmt用于格式化并输出数据

func main(){ //func定义一个函数 main是主函数,是程序的入口
	fmt.Println("Hello World!")  //调用fmt包中的Println函数用于输出"Hello World!"
}
```

#### go Build 与 go Run

**编译程序**

![mark](http://image.i-ll.cc/blog/20190303/gGODYGCFkzj6.png)\

可以看到执行go bulid Hello.go之后程序目录下多了一个Hello.exe,在Doc环境下,执行Hello.exe程序就会输出.

当然我们也可以利用go bulid -o来指定生成的程序名,例如:

![mark](http://image.i-ll.cc/blog/20190303/NQmXJtbwLYaQ.png)\

![mark](http://image.i-ll.cc/blog/20190303/01HAmR1ALYwM.png)\

我们使用go run Hello.go 之后,可以看到,同样输出了"Hello World!"但是没有生成Hello.exe文件.

![mark](http://image.i-ll.cc/blog/20190303/rXki71ksvR7y.png)\

这样的话,我们很容易可以看到:

1. go bulid 是先将.go文件进行编译,然后生成一个可执行文件,我们可以将这个可执行文件放到没有Go环境的电脑上运行.
2. go run 是编译运行一步到位,必须在拥有Go环境的电脑上运行.

#### 包的导入

可以编写多个导入语句,例如:
```go
import "fmt"
import "math"
```
更优雅的一种方法,以"分组"形式导入
```go
package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Printf("%g", math.Pi)
}
```
>运行结果
3.141592653589793

### Go语言的特点-开发注意事项

1. 需要在每一个.go文件头部声明,程序所属包的名称;
2. Go语言严格区分大小写;
3. 应用程序的执行入口是main
4. Go程序每个语句后面无需添加分号(添加了Go语言会自动省略);
5. 编写代码的时候请删除未使用的引用,否则会编译报错,不需要任何多余的代码;
6. Go语言的转义字符(escape char)和C语言大多都一样.

### 转义字符

```go
package main

import "fmt"

func main() {
	fmt.Println("Hello\tWorld!") //转义字符\t
	fmt.Println("Hello\nWorld!") //转义字符\n
	fmt.Println("Hello\\World!") //转义字符\\
	fmt.Println("Hello\"World!") //转义字符\"
	fmt.Println("Hello\rWorld!") //转义字符\r
}
```
运行结果如下:

![mark](http://image.i-ll.cc/blog/20190303/BF1ljyopeig5.png)\

最后一行\r的意思是换行,用World!替换Hello,最终输出World!类似于按一下键盘上的insert然后在Hello前面输入World!

### 注释

Go语言的注释和C语言一样的.

1. 单行注释

	```Go 
	//这是一个注释
	```

2. 多行注释

	```Go
	/*
	这是一个注释
	*/
	```
### Golang官方教程

1.  Golang官方教程
	![mark](http://image.i-ll.cc/blog/20190303/ljkrCjUjaSLs.png)\

	中文版下载地址

	```shell
	go get -u github.com/Go-zh/tour  
	.\bin\tour
	```

	**执行完本行代码,会在GOPATH下的bin文件夹下生成一个tour.exe文件,所以我们执行第二条命令就可以在本地浏览器打开Go的官方文档.**
2. Golang API
	* [官方API](https://golang.org/pkg)
	* [Go语言中文网-Golang标准库文档](https://studygolang.com/pkgdoc)

