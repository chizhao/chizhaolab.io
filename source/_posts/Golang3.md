---
title: Go语言编程系列(三) - Go语言数据类型
date: 2019-03-04 22:06:08
tags:
- 毕业论文
- Go
toc: true
---


### Go语言数据类型概览

在 Go 编程语言中, 数据类型用于声明函数和变量. 

数据类型的出现是为了把数据分成所需内存大小不同的数据, 编程的时候需要用大数据的时候才需要申请大内存, 就可以充分利用内存. 

Go 语言按类别有以下几种数据类型：

<!-- more -->

![mark](http://image.i-ll.cc/blog/20190304/t1jRSgll4p1M.png)\

### 整数类型的使用细节

1. Golang 各整数类型分: 有符号和无符号, int uint 的大小和系统有关;
2. Golang 的整型默认声明为int型;
    ```go
    //如何查看某个变量的数据类型
    var a = 100
    fmt.Printf("a的类型是: %T", a) //fmt.Printf()可以用作格式化输出
    ```
    输出如下:
    >a的类型是: int
3. 在程序种通过引入unsafe包可以查看某个变量的占用字节大小;
    ```go
    //在程序种查看某个变量的占用字节大小和数据类型
    var b int64 = 888
    //unsafe.Sizeof(b)是unsafe包的一个函数, 可以返回变量b占用的字节数
    fmt.Printf("b的数据类型 %T , 占用的字节数是 %d", b, unsafe.Sizeof(b))
    ```
    输出结果:

    >b的数据类型 int64 , 占用的字节数是 8
4. Golang中在保证程序正确运行下, 尽量使用占用空间小的数据类型.
5. bit: 计算机中的最小存储单位. byte:计算机中基本存储单元. 1 byte = 8 bit

### 浮点类型的使用细节

1. 浮点数 = 数符 + 阶码(含阶符) + 尾数, 具体的可以看另一篇文章 - 计算机中数的表示(二);
2. 尾数部分可能丢失, 造成精度损失, 推荐使用float64,因为float64比float32精度更高;
    ```go
    // float32 与 float64 精度对比
	var n1 float32 = -123.0000901
	var n2 float64 = -123.0000901
    fmt.Println("n1=", n1, "n2=", n2)
    ```
    输出结果:

    >n1= -123.00009 n2= -123.0000901
3. Golang的浮点类型默认声明为float64类型;
    ```go
    var n = 1.2
    fmt.Printf("n的数据类型为 %T", n)
    ```
    输出结果:

    >n的数据类型为 float64
4. 浮点类型支持十进制数和科学计数法表示.

### 字符类型

Golang中没有专门的字符类型, 如果要存储单个字符(字母), 一般使用byte来保存. 

字符串就是一串固定长度的字符连接起来的字符序列. Go 的字符串是由单个字节连接起来的. 也
就是说对于传统的字符串是由字符组成的, 而Go的字符串不同, 它是由字节组成的. 

#### 案例演示

```go
//Golang字符演类型的使用
func main() {

	var c1 = 'a'
	var c2 = '0'
	//当我们直接输出byte值时, 输出的实际上是输出字符对应的ASCII码值
	fmt.Println("c1=", c1)
	fmt.Println("c2=", c2)
	//如果我们希望输出对应字符, 需要使用格式化输出
	fmt.Printf("c1=%c,c2=%c \n", c1, c2)
	//var c3 byte = '北' // constant 21271 overflows byte 溢出
	var c3 int = '北'
	fmt.Printf("c3=%c c3对应的码值=%d", c3, c3)

}
```
输出结果:
```
c1= 97
c2= 48
c1=a,c2=0
c3=北 c3对应的码值=21271
```
对上述代码的说明:
1. 如果我们保存的字符在ASCII表中, 可以直接保存到byte;
2. 如果我们保存的字符对应码值大于255, 这时可以采用int保存;
3. 如果需要按照字符的方式输出, 这时候需要格式化输出.

#### 字符类型的使用细节

1. 字符常量是用单引号('')括起来的单个字符. 例如：var c1 byte = 'a';
2. Go 中允许使用转义字符'\'来将其后的字符转变为特殊字符型常量. 例如：var c3 byte = '\\n' , '\\n'表示换行符;
3. Go 语言的字符使用UTF-8 编码,  如果想查询字符对应的utf8 码值[http://www.mytju.com/classcode/tools/encode_utf8.asp](http://www.mytju.com/classcode/tools/encode_utf8.asp) 英文字母 - 1 个字节 汉字 - 3 个字节;
4. 在Go 中, 字符的本质是一个整数, 直接输出时, 是该字符对应的UTF-8 编码的码值.;
5. 可以直接给某个变量赋一个数字, 然后按格式化输出时%c, 会输出该数字对应的unicode字符.
   ```go
   	var c4 int = 21271 //21271->'北'
    fmt.Printf("c4=%c", c4)
    ```
    输出结果:

    >c4=北
6. 字符类型是可以进行运算的,相当于一个整数,因为它都对应有Unicode码.
    ```go
    //字符类型的运算
    var c5 = 10 + 'a' //10+97+107 107->'k'
    fmt.Printf("c5对应的码值等于%d, 对应的字符串=%c", c5, c5)
    ```
    输出结果:

    >c5对应的码值等于107, 对应的字符串=k

#### 字符类型本质

1. 字符型存储到计算机中, 需要将字符对应的码值(整数)找出来;
    - 存储: 字符->对应码值->二进制->存储
    - 读取: 二进制-> 码值-> 字符-> 读取
2. 字符和码值的对应关系是通过字符编码表决定(规定);
3. Go语言的编码都统一成了utf-8. 非常的方便, 很统一, 再也没有编码乱码的困扰了.

### 布尔(bool)类型

1. 布尔类型也叫bool类型, bool类型数据只允许取值true和false, 不可以用0或者非0代替false和true, 这里和C不通;
2. bool 类型占1 个字节;
    ```go
    var a = true
    // bool类型占用的存储空间是1个字节
    fmt.Println("a的占用空间=", unsafe.Sizeof(a))
    ```
    输出结果:
    
    >a的占用空间= 1
3. bool 类型适于逻辑运算, 一般用于程序流程控制.

### 字符串类型的使用细节

1. Go语言的字符串的字节使用UTF-8编码标识Unicode文本, 这样Golang统一使用UTF-8编码, 中文乱码问题不会再困扰程序员;
2. 字符串一旦赋值了, 字符串就不能修改了,** 在Golang中字符串是不可变的**;

    ![mark](http://image.i-ll.cc/blog/20190304/QRn513lLIbRj.png)\

3. 字符串的两种表示形式:
   1. 双引号: 会识别转义字符;
   2. 反引号(ESC下面): 原样输出,包括换行和特殊字符，可以实现防止攻击、输出源代码等效果.
    ```go
    str2 := "abc\tabc"
    fmt.Println(str2)
    str3 := `abc\tabc`
    fmt.Println(str3)
    ```
    输出结果:
    ```
    abc     abc
    abc\tabc
    ```
4. 字符串拼接:
    ```go
    //Golang字符串拼接
	var str = "Hello" + "\tGolang"
	str += "!"
    fmt.Println(str)
    ```
    输出结果:

    >Hello   Golang!
5. 当一行字符串太长时候, 可以做如下处理:
    ```go
    //当一个拼接的操作很长时,可以分行写,+号不能作为行的开头
	var str = "Hello" + "\tGolang" + "Hello" + "\tGolang" +
		"Hello" + "\tGolang" + "Hello" + "\tGolang" +
		"Hello" + "\tGolang" + "Hello" + "\tGolang" +
		"Hello" + "\tGolang" + "Hello" + "\tGolang"
    fmt.Println(str)
    ```
    输出结果:

    >Hello   GolangHello     GolangHello     GolangHello     GolangHello     GolangHello     GolangHello     GolangHello     Golang

### 基本数据类型默认值

| 数据类型 | 默认值 |
| :------: | :----: |
|   整型   |   0    |
|  浮点型  |   0    |
|  字符串  |   ""   |
| 布尔类型 | FALSE  |

```go
//基本数据类型默认值
var a int     //0
var b float32 //0
var c float64 //0
var d string  //""
var e bool    // FALSE
// %v表示按照变量的值输出
fmt.Printf("a=%d,b=%f,c=%f,d=%v,e=%v", a, b, c, d, e)
```
输出结果:

>a=0,b=0.000000,c=0.000000,d=,e=false
