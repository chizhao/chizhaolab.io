---
title: Go语言编程系列(十三) - Go语言面向对象编程(一) - Struct
date: 2019-03-27 13:14:53
tags:
- Go
- 毕业论文
toc: True
---

### 面向对象编程的必要性

用上一节的栗子来说, 一个人员管理系统, 需要包含这个人的姓名, 性别, 年龄等字段. 如果采用传统方法(如: 单独定义变量, 数组), 不利于数据的管理和维护, 因为性别,年龄,姓名都是属于同一个人, 但是是分开保存的. 如果需要对一个人的属性进行操作, 也很不方便. 利用map也可以实现, 但是定义起来比较麻烦. 所有引入结构体. 


<!-- more -->

### Golang 语言面向对象编程说明

1) Golang 也支持面向对象编程(OOP), 但是和传统的面向对象编程有区别, 并不是纯粹的面向对象语言. 所以我们说 Golang 支持面向对象编程特性是比较准确的. 
2) Golang 没有类(class), Go 语言的结构体(struct)和其它编程语言的类(class)有同等的地位, 你可以理解 Golang 是基于 struct 来实现 OOP 特性的. 
3) Golang 面向对象编程非常简洁, 去掉了传统 OOP 语言的继承、方法重载、构造函数和析构函数、隐藏的 this 指针等等
4) Golang 仍然有面向对象编程的**继承, 封装和多态**的特性, 只是实现的方式和其它 OOP 语言不一样, 比如继承 ：Golang 没有 extends 关键字, 继承是通过匿名字段来实现. 
5)  Golang 面向对象(OOP)很优雅, OOP 本身就是语言类型系统(type system)的一部分, 通过接口(interface)关联, 耦合性低, 也非常灵活. 后面同学们会充分体会到这个特点. 也就是说在 Golang 中面向接口编程是非常重要的特性. 

### 结构体与结构体变量(实例/对象)的关系

1. 将一类事物(人,猫,狗或者工具类)的特性提取出来, 形成一个新的数据类型, 就是一个结构体;
2. 结构体里面可以存放属性/字段,也可以存放方法, 也就是这个事物的行为;
3. 通过这个结构体, 我们可以创建多个变量(实例/对象). 

#### 结构体解决学生信息管理问题

```go
type student struct {
	Name    string
	Age     int
	Address string
}

func main() {

	var student1 student
	fmt.Println("student1=", student1) //student1= { 0 } string默认为空, int默认为0
	student1.Name = "alice"
	student1.Address = "上海"
	student1.Age = 18
	fmt.Println(student1) //{alice 18 上海}
}
```

#### 结构体和结构体变量(实例)的区别和联系

通过上面的案例和讲解我们可以看出:

1) 结构体是自定义的数据类型, 代表一类事物.
2) 结构体变量(实例)是具体的, 实际的, 代表一个具体变量


#### 结构体变量(实例)在内存的布局

```go

type student struct {
	Name    string
	Age     int
	Address string
}

func main() {

	var student1 student
	fmt.Println("student1=", student1) //student1= { 0 } string默认为空, int默认为0
	student1.Name = "alice"
	student1.Address = "上海"
	student1.Age = 18
	fmt.Println(student1)
	fmt.Printf("student1的地址=%p\n", &student1)
	//string数据类型占用16字节空间, 前8字节是一个指针, 指向字符串值的地址, 后八个字节是一个整数, 标识字符串的长度. 变量之间也是连续存放的.
	fmt.Printf("student1中Name的地址=%p\n", &student1.Name)
	fmt.Printf("student1中Age的地址=%p\n", &student1.Age)
	fmt.Printf("student1中Address地址=%p\n", &student1.Address)

}
```

输出结果:

```
student1= { 0 }
{alice 18 上海}
student1的地址=0xc0000762d0
student1中Name的地址=0xc0000762d0
student1中Age的地址=0xc0000762e0
student1中Address地址=0xc0000762e8
```


### 结构体的声明

**基本语法**

```go
type 结构体名称 struct {
    field1 type
    field2 type
}
```

栗子

```
type student struct {
	Name    string
	Age     int
	Address string
}
```

### 结构体字段注意事项和细节说明

1) 从概念或叫法上看： 结构体字段 = 属性 = field  （即授课中, 统一叫字段)
2) 字段是结构体的一个组成部分, 一般是基本数据类型、数组,也可是引用类型. 比如我们前面定义猫结构体 的 Name string   就是属性
3) 字段声明语法同变量, 示例：字段名 字段类型
4) 字段的类型可以为：基本类型、数组或引用类型
5) 在创建一个结构体变量后, 如果没有给字段赋值, 都对应一个零值(默认值), 规则同前面讲的一样:布尔类型是 false, 数值是 0 , 字符串是 "". 数组类型的默认值和它的元素类型相关, 比如 score [3]int 则为[0, 0, 0]. **指针, slice, 和 map 的零值都是 nil**, 即还没有分配空间. 
    ```go
    type person struct {
        Name   string
        Age    int
        Scores [5]float64
        ptr    *int              //指针
        slice  []int             //切片
        map1   map[string]string //map
    }

    func main() {

        var p1 person
        fmt.Println(p1) //{ 0 [0 0 0 0 0] <nil> [] map[]}
        if p1.ptr == nil {
            fmt.Println("ok1")
        }
        if p1.slice == nil {
            fmt.Println("ok2")
        }
        if p1.map1 == nil {
            fmt.Println("ok3")
        }
        //使用map slice 一定要先make
        p1.slice = make([]int, 5)
        fmt.Println(p1) //{ 0 [0 0 0 0 0] <nil> [0 0 0 0 0] map[]}
        p1.map1 = make(map[string]string, 5)
        p1.map1["key1"] = "tom"
        fmt.Println(p1) //{ 0 [0 0 0 0 0] <nil> [0 0 0 0 0] map[key1:tom]}
    }
    ```
6) 不同结构体变量的字段是独立, 互不影响, 一个结构体变量字段的更改, 不影响另外一个, 结构体是值类型. 

### 创建结构体变量和访问结构体字段的方式

1. 直接声明: var student01 student;
2. 方式2:student02 := student{};
3. 方式3:var student03 = new(student);
4. 方式4:var student04 = &student{}. 

栗子如下:

```go

type person struct {
	Name string
	Age  int
}

func main() {

	//方式1 直接创建
	var p1 person //{ 0}
	fmt.Println(p1)
	//方式2
	p2 := person{} //{ 0}
	fmt.Println(p2)
	//方式3
	var p3 = new(person)
    //因为p3是一个指针, 字段的标准赋值方式为(*p3).Name = "bob"
    //(*p3).Name不能写成*p3.Name. 因为"."的优先级比"*"高.
	//(*p3).Name = "bob"也可以写成p3.Name = "bob"
	//原因是Go的设计者为了让程序员使用方便, 底层会对p3.Name= "bob" 做一个取值预算 (*p3).Name = "bob"
	(*p3).Name = "bob"
	p3.Age = 20
	fmt.Println(*p3) //{bob 20}
	//方式4
	var p4 = &person{}
	//因为p4是指针, 所以字段的赋值方法和方式3相同.
	p4.Name = "alice"
	(*p4).Age = 16
	fmt.Println(*p4) //{alice 16}
	//下面的语句也可以直接对变量赋值
	var p5 = &person{"jack", 30}
	fmt.Println(*p5) //{jack 30}
}
```

**对方式3,4的补充说明**

1) 第 3 种和第 4 种方式返回的是 结构体指针. 
2) 结构体指针访问字段的标准方式应该是：(*结构体指针).字段名 , 比如 (*person).Name = "tom"
3) 但 go 做了一个简化, 也支持 结构体指针.字段名, 比如 person.Name = "tom". 更加符合程序员使用的习惯, go 编译器底层 对 person.Name 做了转化 (*person).Name. 


### struct类型的内存深入详解&&结构体指针

**看一个栗子**

```go
type person struct {
	Name string
	Age  int
}

func main() {

	var p1 person
	p1.Age = 10
	p1.Name = "小明"
	fmt.Println(p1)
    var p2 = &p1   //p2是一个结构体指针, 里面存放着p1的地址
    //因为p2是一个结构体指针,所以通过p2可以取到p1的值进行修改. 此时p1.Name p2.Name都会发生变化
	p2.Name = "老王" //p2.Name等同于(*p2).Name, 原因同上
	fmt.Printf("p1.Name=%v\t p2.Name=%v\n", p1.Name, p2.Name)
	fmt.Printf("p1的地址是%p\n", &p1)
	//p2自己也占有一块内存,这块内存中存放的就是p1的地址
	fmt.Printf("p2的值是%p\n", p2)
    fmt.Printf("p2的地址是%p\n", &p2)

}
```

输出结果:

```
{小明 10}
p1.Name=老王     p2.Name=老王
p1的地址是0xc00005a400
p2的值是0xc00005a400
p2的地址是0xc00008a020
```

### 结构体使用注意事项和细节

1. 结构体的所有字段在内存中是连续的, 这个在**结构体变量(实例)在内存的布局**中提到过; 下面用另外一个栗子看一下:
    ```go
    type point struct {
        x int
        y int
    }
    type Rect struct {
        leftUp, rightDown point
    }
    type Rect2 struct {
        leftUp, rightDown *point
    }

    func main() {
        r1 := Rect{point{1, 2}, point{3, 4}}
        //r1中有四个int,在内存中连续分布
        fmt.Printf("r1.leftUp.x的地址是%p\n", &r1.leftUp.x)
        fmt.Printf("r1.leftUp.y的地址是%p\n", &r1.leftUp.y)
        fmt.Printf("r1.rightDown.x的地址是%p\n", &r1.rightDown.x)
        fmt.Printf("r1.rightDown.y的地址是%p\n", &r1.rightDown.y)

        r2 := Rect2{&point{10, 20}, &point{300, 40}}
        //r2有两个*point类型, 这两个*point类型的地址是连续的
        fmt.Printf("r2.leftUp本身的地址是%p\n", &r2.leftUp)
        fmt.Printf("r2.rightDown本身的地址是%p\n", &r2.rightDown)
        //但是它们指向的地址不一定连续, 也有可能连续
        fmt.Printf("r2.leftUp指向的地址是%p\n", r2.leftUp)
        fmt.Printf("r2.rightDown指向的地址是%p\n", r2.rightDown)

    }
    ```

    输出结果:

    ```
    r1.leftUp.x的地址是0xc0000620c0
    r1.leftUp.y的地址是0xc0000620c8
    r1.rightDown.x的地址是0xc0000620d0
    r1.rightDown.y的地址是0xc0000620d8
    r2.leftUp本身的地址是0xc0000501c0
    r2.rightDown本身的地址是0xc0000501c8
    r2.leftUp指向的地址是0xc000064080
    r2.rightDown指向的地址是0xc000064090
    ```
2) 结构体是用户单独定义的类型，和其它类型进行转换时需要有完全相同的字段(名字、个数和类型)
    ```go
    type A struct {
        x int
        y int
    }
    type B struct {
        x int
        y int
    }

    func main() {

        var a A
        var b B
        fmt.Println(a, b)
        //a = b //错误 不能将B类型的数据赋给A类型的数据, 下面的代码是正确的.
        a = A(b)
        fmt.Println(a, b)

    }
    ```
3) 结构体进行type 重新定义(相当于取别名)，Golang 认为是新的数据类型，但是相互间可以强转, 同样的栗子. 
    ```go
    type A struct {
        x int
        y int
    }
    type B A

    func main() {

        var a A
        var b B
        fmt.Println(a, b)
        //a = b //错误 不能将B类型的数据赋给A类型的数据, 下面的代码是正确的.
        a = A(b)
        fmt.Println(a, b)

    }
    ```
4) struct 的每个字段上，可以写上一个tag, 该tag 可以通过**反射机制**获取，常见的使用场景就是**序列化和反序列化**。

    序列化的使用场景:

    ![mark](http://image.i-ll.cc/blog/20190327/birHfGIWsdMu.png)\

    ```go
    package main

    import (
        "encoding/json"
        "fmt"
    )

    type Monstrt struct {
        Name  string `json:"name"` //`json:"name"` 就是struct tag
        Age   int    `json:"age"`
        Skill string `json:"skill"`
    }

    func main() {

        //1. 创建一个Monster变量
        Monstrt1 := Monstrt{"牛魔王", 600, "芭蕉扇"}
        //2. 将Monster序列化为json格式字符串
        jsonStr, err := json.Marshal(Monstrt1) //通过别的包调用当前包的结构体, 如果结构体字段是小写,则无法调用
        if err != nil {
            fmt.Println("json错误", err)
        }
        fmt.Println("jsonStr", string(jsonStr))
    }
    ```

    输出结果:

    ```
    jsonStr {"name":"牛魔王","age":600,"skill":"芭蕉扇"}
    ```