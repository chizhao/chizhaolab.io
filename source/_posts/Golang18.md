---
title: Go语言编程系列(十三) - Go语言面向对象编程(三) - 面向对象编程三大特性
date: 2019-03-28 17:53:51
tags:
- Go
- 毕业论文
toc: True
---


### 抽象

将一类事物共有的属性(字段)和行为(方法)提取出来, 形成一个物理模型(结构体). 这种研究问题的方法就叫做抽象.


<!-- more -->

**一个存取款栗子**

```go
package main

import (
	"fmt"
)

//定义结构体
type Account struct {
	AccountNo string
	Pwd       string
	Balance   float64
}

//定义结构体的方法
//存款
func (account *Account) SaveMoney(money float64, pwd string) {
	//查看密码是否正确
	if pwd != account.Pwd {
		fmt.Println("密码错误,请重新输入")
		return
	}
	//看看存款余额是否正确
	if money <= 0 {
		fmt.Println("输入的金额不正确")
		return
	}
	account.Balance += money
	fmt.Println("存款成功")

}

//查询余额

func (account *Account) getBanlance(pwd string) {

	//查看密码是否正确
	if pwd != account.Pwd {
		fmt.Println("密码错误,请重新输入")
		return
	}
	fmt.Printf("你的账号:%v\t余额为%.2f\n", account.AccountNo, account.Balance)
}

//取款
func (account *Account) WithDraw(money float64, pwd string) {
	//查看密码是否正确
	if pwd != account.Pwd {
		fmt.Println("密码错误,请重新输入")
		return
	}
	//看看取款余额是否正确
	if money <= 0 || money > account.Balance {
		fmt.Println("输入的金额不正确")
		return
	}
	account.Balance -= money
	fmt.Println("取款成功")
}

func main() {

	account := Account{
		AccountNo: "666666",
		Pwd:       "999999",
		Balance:   100.0,
	}

	account.getBanlance("999999")    //你的账号:666666 余额为100.00
	account.SaveMoney(200, "999999") //存款成功
	account.WithDraw(60, "999999")   //取款成功
	account.getBanlance("999999")    //你的账号:666666 余额为240.00

}
```

### 面向对象编程三大特性

Golang 仍然有面向对象编程的继承, 封装和多态的特性, 只是实现的方式和其它OOP 语言不一样. 

- 封装: 通过方法实现
- 继承: 通过匿名字段实现
- 多态: 通过接口实现


#### 封装

##### 封装介绍

封装(encapsulation)就是把抽象出的**字段和对字段的操作**封装在一起, 数据被保护在内部, 程序的其他包只有通过被授权的操作(方法), 才能对字段进行操作. 


##### 封装的好处


1) 隐藏实现细节
2) 提可以对数据进行验证, 保证安全合理. 

##### 如何体现封装?

1) 对结构体中的属性进行封装
2) 通过方法, 包 实现封装

##### 封装的实现


1) 将结构体、字段(属性)的**首字母小写**(类似 private);
2) 给结构体所在包提供一个工厂模式的函数, 首字母大写. 类似一个构造函数;
3) 提供一个首字母大写的 Set 方法(类似其它语言的 public), 用于对属性判断并赋值
	```go 
	func (var   结构体类型名) SetXxx(参数列表) (返回值列表) {
	//加入数据验证的业务逻辑
	var.字段 =  参数
	}
	```
4) 提供一个首字母大写的 Get 方法(类似其它语言的 public), 用于获取属性的值.
	```go 
	func (var 结构体类型名) GetXxx() {
	return var.age;
	}
	```


##### 快速入门案例

```go
/*
	要求: 编写一个程序,不能随便查看人的年龄,工资等隐私, 并对输入的年龄进行合理的验证. 
	
	思路:设计: model 包(person.go) main 包(main.go 调用 Person 结构体), 隐私字段小写(私有)
*/

/* person.go */

package model

import (
	"fmt"
)

type person struct {
	Name   string
	age    int //其他包不能访问
	salary float64
}

//写一个工厂模式的函数, 相当于构造函数

func NewPerson(name string) *person {
	return &person{
		Name: name,
	}
}

//为了访问age和salary, 我们编写一堆Setxx和Getxx的方法

func (p *person) SetAge(age int) {
	if age > 0 && age < 150 {
		p.age = age
	} else {
		fmt.Println("年龄范围不正确..")
	}
}

func (p *person) GetAge() int {
	return p.age
}

func (p *person) SetSalary(salary float64) {
	if salary > 3000 && salary < 50000 {
		p.salary = salary
	} else {
		fmt.Println("年龄范围不正确..")
	}
}

func (p *person) GetSalary() float64 {
	return p.salary
}

/* main.go */

package main

import (
	"code/chapter11/encapsulate/model"
	"fmt"
)

func main() {
	p := model.NewPerson("Alice")
	fmt.Println(*p)
	p.SetAge(18)
	p.SetSalary(20000)
	fmt.Printf("姓名:%v\n年龄:%v\n薪水:%v\n", p.Name, p.GetAge(), p.GetSalary())
}
```


输出结果:

```
{Alice 0 0}
姓名:Alice
年龄:18
薪水:20000
```

#### 继承


#### 引出继承的必要性

当两个对象字段和方法几乎一样时, 代码复用性会降低从而造成代码冗余严重的, 且不利于维护和扩展功能. 继承的出现解决了这个问题.

#### 继承的介绍

当多个结构体存在相同的属性(字段)和方法时,可以从这些结构体中抽象出结构体, 在该结构体中定义这些相同的属性和方法. 其他的结构体不需要重新定义这些字段和方法, 只需要嵌入一个匿名结构体就可以. 也就是说在Golang 中, 如果一个struct 嵌套了另一个匿名结构体, 那么这个结构体可以直接访问匿名结构体的字段和方法, 从而实现了继承特性. 

#### 嵌套匿名结构体的基本语法

```go
type Goods struct {
	Name string
	Price int
}
type Book struct {
	Goods //这里就是嵌套匿名结构体 Goods 
	Writer string
}
```

对以上代码的说明: 每一个 商品都有Name和Price, 而特殊的商品Book却拥有Writer. 所以可以将Goods匿名结构体嵌入Book结构体中.

#### 快速入门案例

```go
package main

import (
	"fmt"
)

/*
	编写一个学生考试管理系统, 大学生和小学生的考试内容不一样, 其他结构都相同
*/
type Student struct {
	Name  string
	Age   int
	Score float64
}

type Puppil struct {
	Student //Puppil中包含Student中的所有字段
}

type Graduate struct {
	Student
}

func (student *Student) setScore(score float64) {
	//这个方法是大学生小学生共有的
	student.Score = score
}

func (student *Student) showScore() {
	fmt.Println("学生成绩为", student.Score)
}

func (graduate *Graduate) testing() {
	fmt.Println("大学生正在考试中,考试科目为高等数学!!!")
}
func (puppil *Puppil) testing() {
	fmt.Println("小学生正在考试中,考试科目为初等数学!!!")
}

func main() {

	pupp := &Puppil{Student{"Tom", 9, 0}} //初始化小学生信息
	pupp.testing()
	pupp.setScore(90) //pupp.setScore(90)等价于pupp.Student.setScore(90)
	pupp.showScore()

	grad := &Graduate{Student{"Alice", 18, 0}} //初始化大学生信息
	grad.testing()
	grad.setScore(99)
	grad.showScore()

}
```

输出结果:

```
小学生正在考试中,考试科目为初等数学!!!
学生成绩为 90
大学生正在考试中,考试科目为高等数学!!!
学生成绩为 99
```

#### 继承的深入讨论

1. 结构体**可以使用嵌套匿名结构体所有的字段和方法**, 即: 首字母大写或者小写的字段、方法, 都可以使用;
2. 匿名结构体字段访问可以简化;(匿名结构体中的字段特殊结构体中没有的情况下)
3. 如果匿名结构体和特殊结构体中有相同字段的话, 编译器会采用就近原则. 默认调用特殊结构体中的字段, 此时如果需要访问匿名结构体中字段的话, 需要先调用匿名结构体;
4. 结构体嵌入两个(或多个)匿名结构体, 如两个匿名结构体有相同的字段和方法(同时结构体本身没有同名的字段和方法), 在访问时, 就必须明确指定匿名结构体名字, 否则编译报错. 
5. 如果一个struct 嵌套了一个有名结构体, 这种模式就是组合, 如果是组合关系, 那么在访问组合的结构体的字段或方法时, 必须带上结构体的名字;
6. 嵌套匿名结构体后, 也可以在创建结构体变量(实例)时, 直接指定各个匿名结构体字段的值. 栗子如下:

	```go
	package main

	import (
		"fmt"
	)

	//商品
	type Goods struct {
		Name  string
		Price float64
	}

	//品牌
	type Brand struct {
		Name    string
		Address string
	}

	type TV struct {
		Goods
		Brand
	}
	type TV2 struct {
		*Goods //匿名字段, 结构体指针类型
		*Brand
	}

	func main() {

		tv := TV{Goods{"电视机001", 5000}, Brand{"海尔", "山东"}}

		tv2 := TV{
			Goods{
				Price: 5000,
				Name:  "电视机002",
			},
			Brand{
				Name:    "夏普",
				Address: "北京",
			},
		}

		fmt.Println(tv)  //{{电视机001 5000} {海尔 山东}}
		fmt.Println(tv2) //{{电视机002 5000} {夏普 北京}}

		tv3 := TV2{&Goods{"电视机003", 4999}, &Brand{"创维", "河南"}}

		fmt.Println(tv3)                    //{0xc00005a420 0xc00005a440}
		fmt.Println(*tv3.Brand, *tv3.Goods) //{创维 河南} {电视机003 4999} //注意:*tv.Brand是取tv.Brand的值, 因为"."的优先级大于"*"

	}
	```
7. 基本数据类型也可以做匿名字段.

	```go
	package main

	import (
		"fmt"
	)

	type Monster struct {
		Name string
		Age  int
	}

	type E struct {
		Monster
		int //基本数据类型做匿名字段
	}

	func main() {

		var e E
		e.Name = "牛魔王"
		e.Age = 200
		e.int = 20
		fmt.Println(e)

	}
	```

	对以上代码的补充说明:

   - 如果一个结构体有基本数据类型的匿名字段, 就不能有第二个了;
   - 如果一定要有第二个, 则必须给字段指定名称.
8. 如果一个 struct 嵌套了多个匿名结构体, 那么该结构体可以直接访问嵌套的匿名结构体的字段和方法, 从而实现了多重继承. (尽量不要使用多重继承,栗子看第六条)

#### 接口&&多态

##### 为什么需要接口

USB插槽就是现实中的接口, 我们可以将手机, U盘, 相机等都插在usb接口上, 而不用担心哪个插槽接哪个设备. 因为做usb接口的厂家和做设备的厂家都遵从统一的标准.

##### 接口快速入门案例

模拟一下上面是实际场景.

```go
package main

import (
	"fmt"
)

//声明/定义一个接口
type Usber interface {
	/*声明了两个没有实现的方法*/
	Start()
	Stop()
}

type Phone struct {
}

//让 Phone 实现 Usber 接口的方法
func (p Phone) Start() {
	fmt.Println("手机开始工作. . . ")
}
func (p Phone) Stop() {
	fmt.Println("手机停止工作. . . ")
}

type Camera struct {
}

//让 Camera 实现  Usber 接口的方法
func (c Camera) Start() {
	fmt.Println("相机开始工作. . . ")
}
func (c Camera) Stop() {
	fmt.Println("相机停止工作. . . ")
}

//计算机

type Computer struct {
}

//编写一个方法 Working  方法, 接收一个 Usber 接口类型变量
//只要是实现了 Usber 接口 （所谓实现 Usber 接口, 就是指实现了 Usber 接口声明所有方法）
func (c Computer) Working(usb Usber) { //usb 变量会根据传入的实参, 来判断到底是 Phone,还是 Camera

	//通过 usb 接口变量来调用 Start 和 Stop 方法
	usb.Start()
	usb.Stop()
}
func main() {
	//测试
	//先创建结构体变量
	computer := Computer{}
	phone := Phone{}
	camera := Camera{}

	computer.Working(phone)
	computer.Working(camera)
}
```

输出结果:

```
手机开始工作. . . 
手机停止工作. . . 
相机开始工作. . . 
相机停止工作. . . 
```

##### 接口基本介绍


在 Go 语言中, 接口(interface)是一个自定义类型, 接口类型具体描述了一系列方法的集合. 

1. 接口是用来定义行为的类型. 这些**被定义的行为不由接口直接实现**, 而是通过方法**由用户定义的类型实现**, 一个实现了这些方法的具体类型是这个接口类型的实例. 
2. 接口类型是一种抽象的类型, 它不会暴露出它所代表的对象的内部值的结构和这个对象支持的基础操作的集合, 它们只会展示出它们自己的方法. 因此接口类型不能将其实例化. 
3. 如果用户定义的类型实现了某个接口类型声明的所有方法, 那么这个用户定义的类型的值就可以赋给这个接口类型的值. 这个赋值会把用户定义的类型的值存入接口类型的值. 

**基本语法**

```go
/* 定义接口 */
type 接口名 interface{

	method1(参数列表) 返回值列表
	method2(参数列表) 返回值列表
}

/* 实现接口里面的所有方法 */

func (t 自定义类型) method1(参数列表) 返回值列表{
	//方法实现
}
func (t 自定义类型) method2(参数列表) 返回值列表{
	//方法实现
}
```


- 接⼝命名习惯以 er  结尾;
- 接口只有方法声明, 没有实现, 没有数据字段;
- 接口可以匿名嵌入其它接口, 或嵌入到结构中.

##### 接口使用注意事项和细节

1) 接口本身不能创建实例,但是可以指向一个实现了该接口的自定义类型的变量(实例)
	```go
	type Ainterface interface {
		Say()
	}

	type Stu struct {
	}

	func (stu Stu) Say() {
		fmt.Println("Stu Say")
	}

	func main() {
		////panic: runtime error: invalid memory address or nil pointer dereference
		//var a Ainterface
		//a.Say()

		var Stu Stu
		var a Ainterface = Stu
		a.Say() //Stu Say
	}
	```
2) 接口中所有的方法都没有方法体,即都是没有实现的方法;
3) 在Golang 中, 一个自定义类型需要将某个接口的所有方法都实现, 我们说这个自定义类型实现了该接口;
4) 一个自定义类型只有实现了某个接口, 才能将该自定义类型的实例(变量)赋给接口类型
5) 只要是自定义数据类型, 就可以实现接口, 不仅仅是结构体类型;
	```go
	type Ainterface interface {
		Say()
	}

	type integer int

	func (i integer) Say() {
		fmt.Println("integer Say i =", i)
	}


	func main() {

		var i integer = 10
		var b Ainterface = i
		b.Say() //integer Say i = 10
	}
	```
6) 一个自定义类型可以实现多个接口;
7) Golang 接口中不能有任何变量;
8) 一个接口(比如C 接口)可以继承多个别的接口(比如A, B接口), 这时如果要实现C 接口, 也必须将A,B接口的方法也全部实现;
   
	![mark](http://image.i-ll.cc/blog/20190329/DNr4JL9nWKv0.png)\

	```go
	package main

	import (
		"fmt"
	)

	type Ainterface interface {
		test01()
	}

	type Binterface interface {
		test02()
	}

	type Cinterface interface {
		Ainterface
		Binterface
		test03()
	}

	//如果需要实现Cinterface, 就需要将Binterface和Cinterface的方法都实现

	type Stu struct {
	}

	func (stu Stu) test01() {
		fmt.Print("test01\n")
	}
	func (stu Stu) test02() {
		fmt.Print("test02\n")
	}
	func (stu Stu) test03() {
		fmt.Print("test03\n")
	}

	func main() {

		var stu Stu
		var c Cinterface = stu
		c.test01() //test01
		c.test02() //test02
		c.test03() //test03

	}
	```
9) interface 类型默认是一个指针(引用类型), 如果没有对interface 初始化就使用, 那么会输出nil. 对上面的代码增加2行.
	```go
	var a Ainterface
	fmt.Printf("a的类型为%T\n", a) //a的类型为<nil>
	```
10) 空接口interface{} 没有任何方法, *所以所有类型都实现了空接口*, 即我们可以把任何一个变量赋给空接口. 

	```go
	var v1 interface{} = 1     // 将 int 类型赋值给 interface{}
	fmt.Println(v1)            //1
	var v2 interface{} = "abc" // 将 string 类型赋值给 interface{}
	fmt.Println(v2)            //abc
	var v3 interface{} = &v2   // 将*interface{}类型赋值给 interface{}
	fmt.Println(v3)            //0xc0000501c0
	var v4 interface{} = struct{ X int }{1}
	fmt.Println(v4) //{1}
	var v5 interface{} = &struct{ X int }{1}
	fmt.Println(v5) //&{1}
	```

	当然空接口也可以在函数体外声明.
11) 一个接口中不能有两个一模一样的方法, 不同的接口中可以有相同的方法.

	![mark](http://image.i-ll.cc/blog/20190329/DBhwg4x0IXmF.png)

	因为Cinterface中有两个一模一样的Test01() 所以编译器会报重复定义错误.

12) 注意*T与T类型的区别

	```go
	package main

	import (
		"fmt"
	)

	type Test interface {
		say()
	}

	//如果需要实现Cinterface, 就需要将Binterface和Cinterface的方法都实现

	type Stu struct {
	}

	func (stu *Stu) say() {
		fmt.Print("say\n")
	}

	func main() {

		var stu Stu = Stu{}
		//var test Test = stu //Stu类型没有实现Test接口 需要改为&stu
		var test Test = &stu
		test.say()
		fmt.Println("test:", test)

	}
	```

##### 接口最佳实践

需要用到以下函数和接口

![mark](http://image.i-ll.cc/blog/20190329/6278UCaNz4fk.png)\ 

![mark](http://image.i-ll.cc/blog/20190329/rliy1tQvOW6E.png)\

```go
package main

import (
	"fmt"
	"math/rand"
	"sort"
)

/*
	实现对Hero 结构体切片的排序: sort.Sort(data Interface)
*/

// 1. 声明Hero结构体
type Hero struct {
	Name string
	Age  int
}

//2. 声明Hero结构体切片类型
type HeroSlice []Hero

//3. 实现Interface接口 //查阅文档
func (hs HeroSlice) Len() int {
	// Len is the number of elements in the collection.
	return len(hs)
}

//Less方法决定使用什么标准进行排序
func (hs HeroSlice) Less(i, j int) bool {
	// Less reports whether the element with index i should sort before the element with index j.
	//按年龄进行从小到大排序, 如果返回值为True, 则应该将带有索引i的元素排到带有索引j的元素之前
	//如果是从大到小排序, 则将＜变成＞
	//也可以使用按姓名排序
	return hs[i].Age < hs[j].Age
}

func (hs HeroSlice) Swap(i, j int) {
	// Swap swaps the elements with indexes i and j.
	// temp := hs[i]
	// hs[i] = hs[j]
	// hs[j] = temp
	//下面一句话等价于上面三句话
	hs[i], hs[j] = hs[j], hs[i]
}

func main() {
	// 先定义一个数组/切片
	var intSlice = []int{0, -1, 10, 7, 90}
	//要求对intSlice进行排序
	//1. 冒泡排序...略
	//2. 使用系统提供的方法
	fmt.Println(intSlice)
	sort.Ints(intSlice) //调用sort.Ints()排序
	fmt.Println(intSlice)

	//对一个结构体切片进行排序
	//1. 冒泡排序
	//2. 系统方法 func Sort(data Interface)

	//测试一下看看是否可以对结构体切片进行排序

	var heroes HeroSlice
	//随机生成hero
	for i := 0; i < 10; i++ {
		hero := Hero{
			Name: fmt.Sprintf("英雄~%d", rand.Intn(100)),
			Age:  rand.Intn(100),
		}
		//将hero append到heroes切片
		heroes = append(heroes, hero)
	}
	//看看排序前的顺序
	for _, v := range heroes {
		fmt.Println(v)
	}

	//调用sort.Sort()
	sort.Sort(heroes)
	fmt.Println("----------------------排序后的顺序------------------------")
	//排序后的顺序
	for _, v := range heroes {
		fmt.Println(v)
	}
}
```

输出结果:

```
[0 -1 10 7 90]
[-1 0 7 10 90]
{英雄~81 87}
{英雄~47 59}
{英雄~81 18}
{英雄~25 40}
{英雄~56 0}
{英雄~94 11}
{英雄~62 89}
{英雄~28 74}
{英雄~11 45}
{英雄~37 6}
----------------------排序后的顺序------------------------
{英雄~56 0}
{英雄~37 6}
{英雄~94 11}
{英雄~81 18}
{英雄~25 40}
{英雄~11 45}
{英雄~47 59}
{英雄~28 74}
{英雄~81 87}
{英雄~62 89}
```

##### 接口和继承的比较

![mark](http://image.i-ll.cc/blog/20190329/nLw8j9CkLKGg.png)\

猴子天生会爬树, 但是有一只小猴子通过学习, 学到了鸟和鱼的本领. 飞翔和游泳. 飞翔和游泳并不是猴子天生就有的. 所以需要用利用接口对继承进行补充.

```go
package main

import (
	"fmt"
)

//Monkey结构体
type Monkey struct {
	Name string
}

//声明接口
type BirdAble interface {
	Flying()
}

type FishAble interface {
	Swimming()
}

func (this *Monkey) Climbing() {
	fmt.Println(this.Name, "生来会爬树")
}

//LittleMonkey结构体

type LittleMonkey struct {
	Monkey //继承
}

//让LittleMonkey学会BirdAble

func (this *LittleMonkey) Flying() {
	fmt.Println(this.Name, "通过学习, 学会飞翔...")
}

//让LittleMonkey学会FishAble

func (this *LittleMonkey) Swimming() {
	fmt.Println(this.Name, "通过学习, 学会游泳...")
}

func main() {
	//创建一个LittleMonkey实例
	monkey := LittleMonkey{Monkey{"悟空"}}
	monkey.Climbing()
	monkey.Flying()
	monkey.Swimming()
}
```

1) 当 A 结构体继承了 B 结构体, 那么 A 结构就自动的继承了 B 结构体的字段和方法, 并且可以直接使用;
2) 当 A 结构体需要扩展功能, 同时不希望去破坏继承关系的合理性, 则可以去实现某个接口即可, 因此我们可以认为: 实现接口是对继承机制的补充.
   
	![mark](http://image.i-ll.cc/blog/20190329/a0BRQlzaN4X6.png)\

**两者的区别**

1. 接口和继承解决的问题不同:
   
	- **继承的价值**主要在于: 解决代码的复用性和可维护性.
	- **接口的价值**主要在于: 设计好规范(方法), 让其他自定义类型取实现这些规范(方法)
  
2. 接口比继承更加灵活: 继承是满足is - a的关系, 接口只需满足like - a的关系.
3. 接口在一定程度上实现代码解耦.

##### 多态

**基本介绍**

变量(实例)具有多种形态. 面向对象的第三大特征, 在Go 语言, 多态特征是通过接口实现的. 可以按照统一的接口来调用不同的实现. 这时接口变量就呈现不同的形态. 

接口的快速入门案例中的Working方法, usb Usber既可以接收phone也可以接受camera变量, 就体现了Usber接口的多态性. 

```go
func (c Computer) Working(usb Usber) { //usb 变量会根据传入的实参, 来判断到底是 Phone,还是 Camera

	//通过 usb 接口变量来调用 Start 和 Stop 方法
	usb.Start()
	usb.Stop()
}
```

**接口提现多态的两种形式**

1. 多态参数, usb Usber就是这样一个栗子;
2. 多态数组:

	```go
	package main

	import (
		"fmt"
	)

	type Greeter interface {
		SayHi()
	}

	type student struct {
		Name string
	}
	type Monster struct {
		Name string
	}
	type Monkey struct {
		Name string
	}

	func (ms *Monster) SayHi() {
		fmt.Printf("%s\tSayHi\n", ms.Name)
	}
	func (mk *Monkey) SayHi() {
		fmt.Printf("%s\tSayHi\n", mk.Name)
	}
	func (stu *student) SayHi() {
		fmt.Printf("%s\tSayHi\n", stu.Name)
	}

	func main() {
		var greet [3]Greeter
		ms := &Monster{"牛魔王"}
		mk := &Monster{"孙悟空"}
		stu := &Monster{"小明"}
		//这三个都是不同类型的元素，但是他们实现了interface同一个接口, greet就是一个多态数组
		greet[0], greet[1], greet[2] = ms, mk, stu
		for _, v := range greet {
			v.SayHi()
		}
	}
	```

	输出结果:

	```
	牛魔王  SayHi
	孙悟空  SayHi
	小明    SayHi
	```

### 类型断言

#### 类型断言快速入门

当我们需要将一个接口变量, 赋给具体类型的变量时, 就需要用到类型断言.

类型断言 提供了访问接口值底层具体值的方式。

>t := i.(T)

该语句断言接口值 i 保存了具体类型 T，并将其底层类型为 T 的值赋予变量 t。

若 i 并未保存 T 类型的值，该语句就会触发一个panic。

为了 判断 一个接口值是否保存了一个特定的类型，类型断言可返回两个值：其底层值以及一个报告断言是否成功的布尔值。

>t, ok := i.(T)

若 i 保存了一个 T，那么 t 将会是其底层值，而 ok 为 true。

否则，ok 将为 false 而 t 将为 T 类型的零值，程序并不会产生panic。

```go
package main

import "fmt"

func main() {
	var i interface{} = "hello"

	s := i.(string)
	fmt.Println(s) //hello

	s, ok := i.(string)
	fmt.Println(s, ok) //hello true

	f, ok := i.(float64)
	fmt.Println(f, ok) //0 false

	f = i.(float64) // 报错(panic)
	fmt.Println(f)
}

```

**switch类型测试**

```go
package main

import (
	"fmt"
)

type Element interface{}

type Person struct {
	name string
	age  int
}

//这个函数可以判断传入参数的类型

func TypeJudge(items ...interface{}) {

	for index, element := range items {

		switch value := element.(type) {
		case int:
			fmt.Printf("items[%d] is an int and its value is %d\n", index, value)

		case string:
			fmt.Printf("items[%d] is a string and its value is %s\n", index, value)
		case Person:
			fmt.Printf("items[%d] is a Person and its value is [%s, %d]\n", index, value.name, value.age)
		case *Person:
			fmt.Printf("items[%d] is a *Person and its value is [%s, %d]\n", index, value.name, value.age)
		default:
			fmt.Println("items[%d] is of a different type", index)

		}
	}

}

func main() {
	n := 1         //an int
	str := "Hello" //a string
	per01 := Person{"mike", 18}
	per02 := &Person{"alice", 18}

	TypeJudge(n, str, per01, per02)
}
```

输出结果:

```
items[0] is an int and its value is 1
items[1] is a string and its value is Hello
items[2] is a Person and its value is [mike, 18]
items[3] is a *Person and its value is [alice, 18]
```
#### 类型断言最佳实践


对刚开始的Usber案例进行改进, 如果插入的是相机, 自动开始传输照片.

```go
package main

import (
	"fmt"
)

//声明/定义一个接口
type Usber interface {
	/*声明了两个没有实现的方法*/
	Start()
	Stop()
}

type Phone struct {
}

//让 Phone 实现 Usber 接口的方法
func (p Phone) Start() {
	fmt.Println("手机开始工作. . . ")
}
func (p Phone) Stop() {
	fmt.Println("手机停止工作. . . ")
}

type Camera struct {
}

//让 Camera 实现  Usber 接口的方法
func (c Camera) Start() {
	fmt.Println("相机开始工作. . . ")
}
func (c Camera) Stop() {
	fmt.Println("相机停止工作. . . ")
}
func (c Camera) TransferPhotos() {
	fmt.Println("相机开始传输照片. . . ")
}

//计算机

type Computer struct {
}

//编写一个方法 Working  方法, 接收一个 Usber 接口类型变量
//只要是实现了 Usber 接口 （所谓实现 Usber 接口, 就是指实现了 Usber 接口声明所有方法）
func (c Computer) Working(usb Usber) { //usb 变量会根据传入的实参, 来判断到底是 Phone,还是 Camera

	//通过 usb 接口变量来调用 Start 和 Stop 方法
	usb.Start()
	//这时增加一个功能, 相机特有的, 开始工作的时候自动传输照片
	//如果usb是指向camera的结构体变量, 则调用TransferPhotos方法. 需要用到类型断言
	if Camera, ok := usb.(Camera); ok == true {
		Camera.TransferPhotos()
	}
	usb.Stop()
}
func main() {
	//测试
	//先创建结构体变量
	computer := Computer{}
	phone := Phone{}
	camera := Camera{}

	computer.Working(phone)
	computer.Working(camera)
}
```