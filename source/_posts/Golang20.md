---
title: Go语言编程系列(十五) - 单元测试
date: 2019-03-31 18:31:33
tags:
- Go
- 毕业论文
toc: True
---

### 单元测试的引入

在我们工作中, 我们会遇到这样的情况, 就是去确认一个函数, 或者一个模块的结果是否正确. 传统方式就是在main()函数中调用这个函数, 看看实际输出和预计输出是否一样, 如果一样说明函数正确, 否则函数有误, 需要修改. 传统测试方法有以下缺点:

1)  不方便,  我们需要在 main 函数中去调用, 这样就需要去修改 main 函数, 如果现在项目正在运行, 就可能去停止项目. 
2) 不利于管理, 因为当我们测试多个函数或者多个模块时, 都需要写在 main 函数, 不利于我们管理和清晰我们思路
3) 引出单元测试. -> testing 测试框架 可以很好解决问题. 

<!-- more -->

### 单元测试介绍

Go 语言中自带有一个轻量级的测试框架[testing](https://studygolang.com/static/pkgdoc/pkg/testing.htm) 和自带的go test 命令来实现单元测试和性能测试, testing 框架和其他语言中的测试框架类似, 可以基于这个框架写针对相应函数的测试用例, 也可以基于该框架写相应的压力测试用例. 通过单元测试, 可以解决如下问题:

1) 确保每个函数是可运行, 并且运行结果是正确的
2) 确保写出来的代码性能是好的, 
3) 单元测试能及时的发现程序设计或实现的**逻辑错误**, 使问题及早暴露, 便于问题的定位解决, 而**性能测试**的重点在于发现程序设计上的一些问题, 让程序能够在高并发的情况下还能保持稳定.

### 单元测试 - 快速入门

使用Go 的单元测试对函数 add和sub进行测试. 

calc.go 与 calc_test.go 在同一个包中. 

**calc.go**

```go
package calc

func add(x, y float64) float64 {
	return x + y
}

func sub(x, y float64) float64 {
	return x - y
}
```

**calc_test.go**

```go
package calc

import (
	"fmt"
	"testing" //引入testing包
)

func TestAdd(t *testing.T) {
	//调用
	var res = add(15.5, 25.5)
	if res != 41 {
		//fmt.Printf("add()函数执行错误, 期望结果是%v, 实际结果是%v", 41, res)
		t.Fatalf("add()函数执行错误, 期望结果是%v, 实际结果是%v", 41, res) //输出日志,并退出
	}
	//如果正确, 输出日志
	t.Logf("add(15.5,25.5)执行正确")
}

func TestSub(t *testing.T) {
	//调用
	var res = sub(15.5, 25.5)
	if res != -10 {
		//fmt.Printf("sub()函数执行错误, 期望结果是%v, 实际结果是%v", -10, res)
		t.Fatalf("sub()函数执行错误, 期望结果是%v, 实际结果是%v", -10, res) //输出日志,并退出
	}
	//如果正确, 输出日志
	t.Logf("sub(15.5,25.5)执行正确")
}

func TestHello(t *testing.T) {
	fmt.Println("TestHello 被调用")
}
```

```sh
$ go test -v
=== RUN   TestAdd
--- PASS: TestAdd (0.00s)
    calc_test.go:16: add(15.5,25.5)执行正确
=== RUN   TestSub
--- PASS: TestSub (0.00s)
    calc_test.go:27: sub(15.5,25.5)执行正确
=== RUN   TestHello
TestHello 被调用
--- PASS: TestHello (0.00s)
PASS
ok      code/chapter15/testingdemo01/calc       (cached)
```

当我们故意修改函数, 导致错误结果的输出如下:

```
=== RUN   TestAdd
--- PASS: TestAdd (0.00s)
    calc_test.go:16: add(15.5,25.5)执行正确
=== RUN   TestSub
--- FAIL: TestSub (0.00s)
    calc_test.go:24: sub()函数执行错误, 期望结果是-10, 实际结果是41
=== RUN   TestHello
TestHello 被调用
--- PASS: TestHello (0.00s)
FAIL
FAIL    code/chapter15/testingdemo01/calc       0.292s
```

**这里是一个多组测试用例的栗子**

将上面的TestAdd修改为如下代码

```go
func TestAdd(t *testing.T) {
	var tests = []struct {
		x      float64
		y      float64
		expect float64
	}{
		{1, 1, 2},
		{2, 2, 4},
		{3, 2, 5},
	}

	for _, tt := range tests {

		actual := add(tt.x, tt.y)

		if actual != tt.expect {
			t.Errorf("add(%.2f, %.2f):expect%.2f,actual%.2f", tt.x, tt.y, tt.expect, actual)
			//Fatalf与Errorf的区别是, Fatalf只要失败了就立即终止当前测试函数. 而Errorf失败后会继续执行当前测试函数
		}
	}
}

```

```sh
$ go test -v
=== RUN   TestSub
--- PASS: TestSub (0.00s)
    calc_test.go:27: sub(15.5,25.5)执行正确
=== RUN   TestHello
TestHello 被调用
--- PASS: TestHello (0.00s)
=== RUN   TestAdd
--- PASS: TestAdd (0.00s)
PASS
ok      code/chapter15/testingdemo01/calc    
```

故意改错结果如下:

```
=== RUN   TestSub
--- PASS: TestSub (0.00s)
    calc_test.go:27: sub(15.5,25.5)执行正确
=== RUN   TestHello
TestHello 被调用
--- PASS: TestHello (0.00s)
=== RUN   TestAdd
--- FAIL: TestAdd (0.00s)
    calc_test.go:50: add(1.00, 1.00):expect2.00,actual0.00
    calc_test.go:50: add(2.00, 2.00):expect4.00,actual0.00
    calc_test.go:50: add(3.00, 2.00):expect5.00,actual1.00
FAIL
FAIL    code/chapter15/testingdemo01/calc       0.327s
```

### 单元测试注意事项和细节

1) 测试用例文件名必须以 _test.go 结尾.  比如 cal_test.go , cal 不是固定的. XXX_test.go必须与被测试的函数在同一个包中; 
2) 测试用例函数必须以 Test 开头, 一般来说就是 TestXxx, 比如 TestAdd其中 Xxx 可以是任何字母数字字符串（但第一个字母不能是 [a-z]）, 用于识别测试例程;
3) TestAdd(t *tesing.T)   的形参类型必须是 *testing.T;
4) 一个测试用例文件中, 可以有多个测试用例函数, 比如 TestAdd、TestSub
5) 运行测试用例指令

    ```sh
    $ go test     [如果运行正确, 无日志, 错误时, 会输出日志]
    $ cmd>go test -v     [运行正确或是错误, 都输出日志]
    ```

6) 当出现错误时, 可以使用 t.Fatalf 来格式化输出错误信息, 并退出程序
7) t.Logf 方法可以输出相应的日志
8) 测试用例函数, 并没有放在 main 函数中, 也执行了, 这就是测试用例的方便之处;
9) PASS 表示测试用例运行成功, FAIL  表示测试用例运行失败
10) 测试单个文件, 一定要带上被测试的原文件
    ```shell
    $ go test -v  calc_test.go calc.go
    ```
11) 测试单个方法
    
    ```shell    
    $ go test -v -test.run   TestAdd
    ```

### 单元测试最佳实践


**monster.go**

```go
package monster

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type Monster struct {
	Name  string
	Age   int
	Skill string
}

//给Monster绑定方法Store, 可以将一个Monster对象序列化后保存到文件中

func (this *Monster) Store() bool {
	//先序列化
	data, err := json.Marshal(this)

	if err != nil {
		fmt.Println("marshal err = ", err)
		return false
	}

	//保存到文件

	filePath := "d:/monster.ser"
	err = ioutil.WriteFile(filePath, data, 0666)
	if err != nil {
		fmt.Println("write file  err = ", err)
		return false
	}
	return true
}

//给Monster绑定方法ReStore, 可以将一个序列化的Monster, 从文件中读取
//并反序列化为Monster对象, 检查反序列化, 名字正确
func (this *Monster) ReStore() bool {
	//1. 先从文件中，读取序列化的字符串
	filePath := "d:/monster.ser"
	data, err := ioutil.ReadFile(filePath)
	if err != nil {
		fmt.Println("ReadFile err =", err)
		return false
	}
	//2.使用读取到data []byte ,对反序列化
	err = json.Unmarshal(data, this)
	if err != nil {
		fmt.Println("UnMarshal err =", err)
		return false
	}
	return true
}
```

**monster_test.go**

```go
package monster

import (
	"testing"
)

//测试用例, 测试Store方法

func TestStore(t *testing.T) {
	//先创建一个Monster实例
	monster := &Monster{
		Name:  "红孩儿",
		Age:   200,
		Skill: "吐火",
	}
	res := monster.Store()
	if !res {
		t.Fatalf("monster.Store()错误, 希望为%v, 实际为%v", true, res)
	}
	t.Logf("monster.Store() 测试成功!")
}

//测试Restore方法

func TestRestore(t *testing.T) {

	var monster Monster
	res := monster.ReStore()
	if !res {
		t.Fatalf("monster.Restore()错误, 希望为%v 实际为%v", true, res)
	}
	//进一步判断
	if monster.Name != "红孩儿" {
		t.Fatalf("monster.Restore()错误, 希望为%v 实际为%v", "红孩儿", res)
	}
	t.Logf("monster.Restore() 测试成功!")

}
```

