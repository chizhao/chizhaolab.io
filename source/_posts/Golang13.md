---
title: Go语言编程系列(十一) - 数组和切片
date: 2019-03-17 13:33:18
tags:
- Go
- 毕业论文
toc: True
---


### 数组概述

数组是指一系列同一类型数据的集合. 数组中包含的每个数据被称为数组元素（element）, 一个数组包含的元素个数被称为数组的长度. **数组⻓度必须是常量**, 且是类型的组成部分. [2]int 和[3]int 是不同类型. 

**类型 [n]T 表示拥有 n 个 T 类型的值的数组.**

例如:

```go
var a [10]int
```

会将变量 a 声明为拥有 10 个整数的数组. 


```go
package main

import "fmt"

func main() {
	var a [2]string
	a[0] = "Hello"
	a[1] = "World"
	fmt.Println(a[0], a[1])
	fmt.Println(a)

	primes := [6]int{2, 3, 5, 7, 11, 13}
	fmt.Println(primes)
}
```

输出结果:

```
Hello World
[Hello World]
[2 3 5 7 11 13]
```

使用数组可以让程序更加利于维护, 增加代码的可读性. 
 
<!-- more -->


### 数组的内存布局


![mark](http://image.i-ll.cc/blog/20190317/S7obfDseAn7M.png)\

**一个简单的栗子**:

```go
package main

import (
	"fmt"
)

func main() {

	var intArr [3]int //我是64位系统 默认是int64占用8个字节, 而int32占用4个字节
	//当我们定义完数组后, 数组的每个元素都有默认值 0

	fmt.Println(intArr)

	fmt.Printf("intArr的地址=%p intArr[0]的地址=%p", &intArr, &intArr[0])
	fmt.Println()
	fmt.Printf("intArr[1]的地址=%p intArr[2]的地址=%p", &intArr[1], &intArr[2])
	fmt.Println()

	var intArr2 [3]int32

	fmt.Println(intArr2)
	fmt.Printf("intArr2的地址=%p intArr2[0]的地址=%p", &intArr2, &intArr2[0])
	fmt.Println()
	fmt.Printf("intArr2[1]的地址=%p intArr2[2]的地址=%p", &intArr2[1], &intArr2[2])

}
```

输出结果:

```
[0 0 0]
intArr的地址=0xc0000600c0 intArr[0]的地址=0xc0000600c0
intArr[1]的地址=0xc0000600c8 intArr[2]的地址=0xc0000600d0
[0 0 0]
intArr2的地址=0xc0000620a4 intArr2[0]的地址=0xc0000620a4
intArr2[1]的地址=0xc0000620a8 intArr2[2]的地址=0xc0000620ac
```

**对以上代码的解读:**

1. 数组的地址可以通过数组名来获取;
2. 数组的第一个元素的地址就是该数组的地址;
3. 数组元素的地址是连续的;
4. 后一个数组元素的地址等于前一个元素地址加上数组中元素类型所占的字节数. 1byte=8bit


### 数组的使用

**访问数组元素**

**数组名[下标]** 比如: a[2]代表访问a数组的第三个元素.

**来一个栗子**:

```go
/*
    从终端循环输入5个成绩, 保存到float64数组, 并输出
*/
var score [5]float64
for i := 0; i < len(score); i++ {
    fmt.Printf("请输入第%d个元素的值\n", i)
    fmt.Scanln(&score[i])
}

for i := 0; i < len(score); i++ {
    fmt.Printf("score[%d] = %v\n", i, score[i])
}
```

**四种初始化数组的方式**

```go
/*
    四种初始化数组的方式
*/

var numArr01 [3]int = [3]int{1, 2, 3}
fmt.Println(numArr01)

var numArr02 = [3]int{5, 6, 7}
fmt.Println(numArr02)

var numArr03 = [...]int{8, 9, 10} //[...]是固定写法
fmt.Println(numArr03)

var numArr04 = [...]int{1: 11, 0: 10, 2: 12} //:前面代表下标
fmt.Println(numArr04)
```

输出结果:

```
[1 2 3]
[5 6 7]
[8 9 10]
[10 11 12]
```

当然数组也可以使用类型推导.


### 数组的遍历方式

方式1就是使用for循环.

**for-range结构遍历**

基本语法:

```go
for index, value := range array01{
    ...
}
```

说明:

1. 第一个返回值index是数组的下标;
2. 第二个value是数组在该下标位置的值;
3. 他们都是仅仅在for循环内部可见的局部变量;
4. 遍历数组元素的时候, 如果不想使用下标index, 可以直接把下表index标为下划线_;
5. index,value的名字不是固定的.

**for-range案例**

```go
//for-range遍历数组
heros := [...]string{"宋江", "吴用", "卢俊义"}
for index, value := range heros {
    fmt.Printf("index=%v value=%v\n", index, value)
}
//忽略下标
for _, value := range heros {
    fmt.Printf("value=%v\n", value)
}
```

输出结果:

```
index=0 value=宋江
index=1 value=吴用
index=2 value=卢俊义
value=宋江
value=吴用
value=卢俊义
```


### 数组使用注意事项和细节

1) 数组是多个相同类型数据的组合, 一个数组一旦声明/定义了, 其长度是固定的, 不能动态变化;
2) var arr []int 这时arr 就是一个slice切片, 切片在下面;
3) 数组中的元素可以是任何数据类型, 包括值类型和引用类型, 但是不能混用;
4) 数组创建后, 如果没有赋值, 具有默认值;
   
    ```go
    /*
        1. 数字类型数组默认值全为0
        2. string默认值为""
        3. bool默认值全为false
    */
    var arr01 [3]float64
    var arr02 [3]string
    var arr03 [3]bool
    fmt.Printf("arr01=%v,arr02=%v,arr03=%v", arr01, arr02, arr03)
    ```
5) 使用数组的步骤:1. 声明数组并开辟空间;2. 给数组赋值;3. 使用数组;
6) 数组的下标是从0开始的;
7) 数组下标必须在指定范围内使用, 否则报panic：数组越界, 比如
   
    var arr [5]int 则有效下标为0-4
8) Go 的数组属于**值类型**, 在默认情况下是值传递,  因此会进行值拷贝. 数组间不会相互影响:
    ```go

    func test(arr [3]int) {
        arr[0] = 88
        fmt.Println("test arr=", arr)

    }

    func main() {

        arr := [3]int{11, 22, 33}
        fmt.Println("main arr=", arr)
        test(arr)
        fmt.Println("main arr=", arr)

    }
    ```

    输出结果:
    
    ```
    main arr= [11 22 33]
    test arr= [88 22 33]
    main arr= [11 22 33]
    ```

    可以看出执行了test()函数并没有修改原本的arr数组.
9) 如想在其它函数中, 去修改原来的数组, 可以使用引用传递(指针方式)

    ```go
    func test(arr *[3]int) {
        (*arr)[0] = 88
        fmt.Println("test arr=", *arr)

    }

    func main() {

        arr := [3]int{11, 22, 33}
        fmt.Println("main arr=", arr)
        test(&arr)
        fmt.Println("main arr=", arr)

    }
    ```

    输出结果:

    ```
    main arr= [11 22 33]
    test arr= [88 22 33]
    main arr= [88 22 33]
    ```
10) 长度是数组类型的一部分, 在传递函数参数时需要考虑数组的长度, 看下面案例

    ![mark](http://image.i-ll.cc/blog/20190317/Dvr6L1el3wOI.png)\

### 数组应用案例

```go
package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	/*
		1. 创建一个byte 类型的26 个元素的数组, 分别放置'A'-'Z‘. 使用for 循环访问所有元素并打印出来. 
			提示：字符数据运算'A'+1 -> 'B'
	*/
	var arr01 [26]byte
	for i := 0; i < len(arr01); i++ {
		arr01[i] = 'A' + byte(i) //需要将i转为byte
	}
	for i := 0; i < len(arr01); i++ {
		fmt.Printf("%c ", arr01[i])
	}
	fmt.Println()
	/*
		2. 请求出一个数组的最大值, 并得到对应的下标. 
	*/
	var arr02 = [...]int{1, -1, 8, 10, 11} //定义一个数组
	maxVal := arr02[0]
	maxValIndex := 0
	for i := 0; i < len(arr02); i++ {
		//从第二个元素开始循环比较,如果发现有更大的,就交换
		if maxVal < arr02[i] {
			maxVal = arr02[i]
			maxValIndex = i
		}
	}
	fmt.Printf("maxVal=%v maxValIndex=%v", maxVal, maxValIndex)
	fmt.Println()
	/*
		3. 请求出一个数组的和和平均值. for-range
	*/
	var arr03 = [...]int{1, -1, 8, 53, 11} //定义一个数组
	sum := 0
	for _, val := range arr03 {
		//累计求和
		sum += val
	}
	//平均值保留小数, 用到强制转换
	fmt.Printf("sum = %v mean =%v\n", sum, float64(sum)/float64(len(arr03)))

	/*
		4. 随机生成五个数,, 并将其反转打印
			rand.Intn()函数
	*/
	var arr04 [5]int
	len := len(arr04) //将数组长度存到len变量里面,后面就不用调用len()函数了
	//为了每次生成的随机数不一样,我们需要给一个seed值
	rand.Seed(time.Now().UnixNano())
	for i := 0; i < len; i++ {
		arr04[i] = rand.Intn(100) //生成范围属于[0,100)的随机数
	}
	//原始数组打印
	fmt.Println(arr04)
	//反转打印,交换的次数是len/2, 倒数第一个和第一个元素交换...
	temp := 0 //临时变量
	for i := 0; i < len/2; i++ {
		temp = arr04[i]
		arr04[i] = arr04[len-1-i]
		arr04[len-1-i] = temp
	}
	fmt.Println(arr04)

}
```

输出结果:

```
A B C D E F G H I J K L M N O P Q R S T U V W X Y Z
maxVal=11 maxValIndex=4
sum = 72 mean =14.4
[16 1 41 33 79]
[79 33 41 1 16]
``` 

### 切片快速入门

每个数组的大小都是固定的. 而切片则为数组元素提供动态大小的、灵活的视角. 在实践中, 切片比数组更常用. 

类型 []T 表示一个元素类型为 T 的切片. 

切片通过两个下标来界定, 即一个上界和一个下界, 二者以冒号分隔：

```go
a[low : high]
```

它会选择一个半开区间, 包括第一个元素, 但排除最后一个元素. 

以下表达式创建了一个切片, 它包含 a 中下标从 1 到 3 的元素：

```go
a[1:4]
```

```go
primes := [6]int{2, 3, 5, 7, 11, 13}

var s []int = primes[1:4]
fmt.Println(s)
```

输出结果:

```
[3 5 7]
```

**切片就像是数组的引用**,是引用类型, 在进行传递时, 遵守引用传递的机制. 它并不存储任何数据, 它只是描述了底层数组中的一段. 

更改切片的元素会修改其底层数组中对应的元素. 

与它共享底层数组的切片都会观测到这些修改, 并且切片的切片还是指向底层数组.

对以上代码稍作修改:

```go
primes := [6]int{2, 3, 5, 7, 11, 13}

var s []int = primes[1:4]
fmt.Println(s)
s[0] = 33
s1 :=s[1:]
s1[0]=44
fmt.Println(primes)
```

输出结果:

```
[3 5 7]
[2 33 44 7 11 13]
```

**切片的长度与容量**

切片的长度就是它所包含的元素个数. 

**切片的容量是从它的第一个元素开始数, 到其底层数组元素末尾的个数. **

切片 s 的长度和容量可通过表达式 len(s) 和 cap(s) 来获取. 

```go
package main

import "fmt"

func main() {
	s := []int{2, 3, 5, 7, 11, 13}
	printSlice(s) //len=6 cap=6 [2 3 5 7 11 13]

	// 截取切片使其长度为 0
	s = s[:0]
	printSlice(s) //len=0 cap=6 []

	// 拓展其长度
	s = s[:4] 
	printSlice(s) //len=4 cap=6 [2 3 5 7]

	// 舍弃前两个值
	s = s[2:] 
	printSlice(s) //len=2 cap=4 [5 7]
}

func printSlice(s []int) {
	fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
}
```


**切片的截取**


|      操 作      |                                      含 义                                      |
| :-------------: | :-----------------------------------------------------------------------------: |
|      s[n]       |                        切 片 s 中 索 引 位 置 为 n 的 项                        |
|      s[:]       |                从切片 s 的索引位置 0 到 len(s)-1 处所获得的切片                 |
|     s[low:]     |               从切片 s 的索引位置 low 到 len(s)-1 处所获得的切片                |
|    s[:high]     |             从切片 s 的索引位置 0 到 high 处所获得的切片, len=high              |
|   s[low:high]   |          从切片 s 的索引位置 low 到 high 处所获得的切片, len=high-low           |
| s[low:high:max] | 从切片 s 的索引位置 low 到 high 处所获得的切片, len=high-low, max是切片最大容量 | cap=max-low |
|     len(s)      |                       切 片 s 的 长 度 ,  总 是 <=cap(s)                        |
|     cap(s)      |                       切 片 s 的 容 量 ,  总 是 >=len(s)                        |

示例说明：

>array := []int{0, 1, 2, 3, 4, 5, 6, 7, 8, 9}

|    操 作    |         结 果         |  len  |  cap  |       说 明       |
| :---------: | :-------------------: | :---: | :---: | :---------------: |
| array[:6:7] |     [0 1 2 3 4 5]     |   6   |   7   |     省 略 low     |
|  array[5:]  |      [5 6 7 8 9]      |   5   |   5   | 省 略 high 、 max |
|  array[:3]  |        [0 1 2]        |   3   |  10   | 省 略 high 、 max |
|  array[:]   | [0 1 2 3 4 5 6 7 8 9] |  10   |  10   |    全 部 省 略    |

### 切片的使用

1. 定义一个切片, 让这个切片去引用前面以及创建好的数组, 比如前面的案例:
    ```go
    primes := [6]int{2, 3, 5, 7, 11, 13}

    var s []int = primes[1:4]
    fmt.Println(s)
    ```
2. 通过make来创建切片
    
    ![mark](http://image.i-ll.cc/blog/20190317/qgzpRqKUpGXY.png)\

    语法:

    ```go
    
    var 切片名 []type = make([]type, len, [cap])

    ```

    
    参数说明: type: 就是数据类型 len : 大小 cap  ：指定切片容量, 可选,  如果你分配了 cap,则要求 cap>=len.


    栗子如下:

    ```go
    //演示切片make方法的使用,对于切片,必须make后才能使用,切片的零值是nil
	var slice []float64
    fmt.Println(slice) //[]
    if slice == nil {
		fmt.Println("nil!")//nil
	}

	slice1 := make([]float64, 5, 10)
    fmt.Println(slice1) //[0 0 0 0 0]
    ```

    对以上代码的说明:

      1) 通过make 方式创建切片可以指定切片的大小和容量;
      2) 如果只定义了一个空切片,那么切片的零值是 nil. nil 切片的长度和容量为 0 且没有底层数组;
      3) 如果没有给切片的各个元素赋值, 那么就会使用默认值[int , float=> 0 string =>”” bool =>false];
      4) 通过make 方式创建的切片对应的数组是由make 底层维护, 对外不可见, 即只能通过slice 去访问各个元素.
   
3. 定义一个切片, 直接就指定具体数组, 使用原理类似make 的方式
    ```go
    slice2 := []string{"tom", "jack", "mary"}
    fmt.Printf("slice2=%v\tlen(slice2)=%v\tcap(slice2)=%v", slice2, len(slice2), cap(slice2))
    ```

    输出结果:

    ```
    slice2=[tom jack mary]  len(slice2)=3   cap(slice2)=3
    ```
**方式1和方式2的区别**:

1. 方式1是直接引用数组, 这个数组是事先存在的, 程序员是可见的;
2. 方式2是通过make来创建切片, 同时make也会创建一个数组, 是由切片在底层进行维护, 程序员是不可见的.

**切片的遍历**

切片的遍历和数组一样:

1. for循环方式;
2. for-range方式.


### 切片使用的注意细节


1) 切片初始化时 var slice = arr[startIndex:endIndex]
说明：从 arr 数组下标为 startIndex, 取到 下标为 endIndex 的元素(不含 arr[endIndex]). 
2) 切片初始化时, 仍然不能越界. 范围在 [0-len(arr)] 之间, 但是可以动态增长.
    
    切片下界的默认值为 0, 上界则是该切片的长度. 
    对于数组
    
    ```go
    var a [10]int
    ```
    
    来说, 以下切片是等价的:

    ```go
    a[0:10]
    a[:10]
    a[0:]
    a[:]
    ```
3) cap 是一个内置函数, 用于统计切片的容量, 即最大可以存放多少个元素; 
4) 切片定义完后, 还不能使用, 因为本身是一个空的,零值为nil, 需要让其引用到一个数组, 或者 make 一个空间供切片来使用;
5) 切片可以继续切片;
6) 用append 内置函数，可以对切片进行动态追加;
    ```go
    package main

    import "fmt"

    func main() {
        var s []int
        printSlice(s)

        // 添加一个空切片
        s = append(s, 0)
        printSlice(s)

        // 这个切片会按需增长
        s = append(s, 1)
        printSlice(s)

        // 可以一次性添加多个元素
        s = append(s, 2, 3, 4)
        printSlice(s)

        // 可以通过append(s,s...)将切片s追加给s
        s = append(s, s...)
        printSlice(s)

    }

    func printSlice(s []int) {
        fmt.Printf("len=%d cap=%d %v\n", len(s), cap(s), s)
    }
    ```

    输出结果:

    ```
    len=0 cap=0 []
    len=1 cap=1 [0]
    len=2 cap=2 [0 1]
    len=5 cap=6 [0 1 2 3 4]
    len=10 cap=12 [0 1 2 3 4 0 1 2 3 4]
    ```

    append 的结果是一个包含原切片所有元素加上新添加元素的切片。

    当 s 的底层数组太小，不足以容纳所有给定的值时，它就会分配一个更大的数组。返回的切片会指向这个新分配的数组。

    **切片append操作的底层原理分析:**

    1. 切片 append 操作的本质就是对数组扩容;
    2. go 底层会创建一下新的数组 newArr(安装扩容后大小);
    3. 将 slice 原来包含的元素拷贝到新的数组 newArr;
    4. 将要追加的元素放入newArr;
    5. 引用newArr;
    
        **注意** newArr 是在底层来维护的，程序员不可见.
7) 切片的拷贝操作;

    ```go
    // 切片的拷贝操作

	slice4 := []int{1, 2, 3, 4, 5}
	slice5 := make([]int, 10)
	copy(slice5, slice4)           //slice copy 给slice5
	fmt.Println("slice4=", slice4) // 1 2 3 4 5
	fmt.Println("slice5=", slice5) // 1 2 3 4 5 0 0 0 0 0
	slice5[0] = 5
	copy(slice4, slice5)
	fmt.Println("slice4=", slice4) // 5 2 3 4 5
	fmt.Println("slice5=", slice5) // 5 2 3 4 5 0 0 0 0 0
    ```

    对上面代码的说明:

    1) copy(para1, para2) 参数的数据类型是切片;
    2) 按照上面的代码来看, slice4 和slice5 的数据空间是独立，相互不影响，也就是说slice4[0]= 999, slice5[0] 仍然是1;
    3) copy操作对切片大小没有大小限制.

9) 切片是引用类型，所以在传递时，遵守引用传递机制.

    ```go
    func test(slice []int) {
        slice[0] = 100//这里修改slice[0],会改变实参
    }

    func main() {

        slice := []int{1, 2, 3}
        fmt.Println(slice) //1 2 3
        test(slice)
        fmt.Println(slice) //100 2 3

    }
    ```
### String和Slice

1) string 底层是一个byte 数组，因此string 也可以进行切片处理案例演示:

    ```go
    str := "Hello Golang"
	//使用切片获取到Golang
	slice := str[6:]
    fmt.Println(slice)
    ```

2) string 是不可变的，也就说不能通过str[0] = 'z' 方式来修改字符串;
3) 如果需要修改字符串，可以先将string -> []byte / 或者[]rune -> 修改-> 重写转成string

    ```go
    //如果需要修改字符串，可以先将string -> []byte / 或者[]rune -> 修改-> 重写转成string

    arr1 := []byte(str)
    arr1[0] = 'z'
    str = string(arr1)
    fmt.Printf("%s", str)
    fmt.Println()

    //细节: 转成[]byte后, 可以处理英文和数字, 但是不能处理中文
    //原因: []byte是按字节来处理, 而一个汉字占3个字节, 因此会出现乱码
    //解决方法是将string转成[]rune, 因为[]rune是按字符处理,兼容汉字

    arr2 := []rune(str)
    arr2[0] = '北'
    str = string(arr2)
    fmt.Printf("%s", str)
    ```

    输出结果:

    ```
    zello Golang
    北ello Golang
    ```

### 切片课堂练习

```go
func fbn(n int) []uint64 {
    /*

        说明：编写一个函数fbn(n int) ，要求完成

        1) 可以接收一个n int
        2) 能够将斐波那契的数列放到切片中
        3) 提示, 斐波那契的数列形式:
        arr[0] = 1; arr[1] = 1; arr[2]=2; arr[3] = 3; arr[4]=5; arr[5]=8
    */
	slice := make([]uint64, n)
	if n <= 2 {
		for i := 0; i < n; i++ {
			slice[i] = 1
		}
	} else {
		slice[0] = 1
		slice[1] = 1
		for i := 2; i < n; i++ {
			slice[i] = slice[i-1] + slice[i-2]
		}
	}
	return slice
}

func main() {


	slice1 := fbn(10)
	fmt.Println(slice1)

}

```

输出结果:

```
[1 1 2 3 5 8 13 21 34 55]
```