---
title: Go语言编程系列(十三) - Go语言面向对象编程(二) - 方法&&工厂模式
date: 2019-03-27 17:47:23
tags:
- Go
- 毕业论文
toc: True
---


### 方法的基本介绍

方法就是一类带特殊的接收者参数的函数. 

Golang 中的方法是作用在指定的数据类型上的(即: 和指定的数据类型绑定), 因此自定义类型, 都可以有方法, 而不仅仅是struct. 

<!-- more -->

#### 方法的声明和调用

```go
func (receiver ReceiverType) funcName(parameters) returnType{
    otherCode...
}
```

**一个栗子**


```go
type A struct {
	Num int
}

//给A类型绑定一个方法
func (a A) test() {
	a.Num = 10
	fmt.Println("test()", a.Num)
}

func main() {

	var a A
	a.test() //调用方法

}
```

输出结果:

```
test() 10
```

对以上代码的说明:

1) func (a A) test()   {}   表示方法test与结构体为A的类型绑定, 也就是说只能由A类型的变量调用
2) (a A) 体现 test 方法是和 A 类型绑定的
3) (a A)里面的a 是由程序员指定的, 代表类型为A的变量, 可以任意指定.

#### 方法快速入门


方法实际上就是一个带有接收者参数的函数. 写法除了前面加了接收者参数, 其他和正常函数的写法是一样的. 

（注: 就是接收者的类型定义和方法声明必须在同一包内；不能为内建类型声明方法. ）



下面是Go语言官方给的一个实例:

```go
package main

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

func Abs(v Vertex) float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func main() {
	v := Vertex{3, 4}
	fmt.Println(Abs(v)) //5
}

```

```go
package main

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func main() {
	v := Vertex{3, 4}
	fmt.Println(v.Abs()) //5
}
```


**值/指针接收者的栗子**

```go
package main

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func (v Vertex) Scale(f float64) { //值作为接收者
	v.X = v.X * f
	v.Y = v.Y * f
}

func main() {
	v := Vertex{3, 4}
	v.Scale(10) //5
	fmt.Println(v.Abs())
}
```


```go
package main

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func (v *Vertex) Scale(f float64) { //指针作为接收者
	v.X = v.X * f
	v.Y = v.Y * f
}

func main() {
    v := Vertex{3, 4}
    //因为Scale绑定的接收者类型是 *Vertex, 所以需要使用(&v).Scale来调用这个方法
    //因为编译器做了优化, (&v).Scale()与v.Scale()等价
	v.Scale(10) //50 
	fmt.Println(v.Abs())
}
```

**方法的调用和传参机制**

1) 在通过一个变量去调用方法时, 其调用机制和函数一样;
2) 不一样的地方时, 变量调用方法时, 该变量本身也会作为一个参数传递到方法(如果变量是值类型, 则进行值拷贝, 如果变量是引用类型, 则进行地址拷贝)

### 对方法定义的详细说明

**语法**

```go
func (recevier type) methodName（参数列表） (返回值列表){   
    
    方法体
    return 返回值
}
```


1) 参数列表: 表示方法输入;
2) recevier type : 表示这个方法和 type 这个类型进行绑定, 或者说该方法作用于 type 类型;
3) receiver type : type 可以是结构体, 也可以其它的自定义类型;
4) receiver : 就是 type 类型的一个变量(实例), 比如 : Person 结构体 的一个变量(实例);
5) 返回值列表: 表示返回的值, 可以多个;
6) 方法主体: 表示为了实现某一功能代码块;
7) return 语句不是必须的. 

### 方法注意事项和细节


1) 结构体类型是值类型, 在方法调用中, 遵守值类型的传递机制, 是值拷贝传递方式;
2) 如程序员希望在方法中, 修改结构体变量的值, 可以通过结构体指针的方式来处理;

    ![mark](http://image.i-ll.cc/blog/20190327/AW6Cu31PDxk2.png)\

3) Golang 中的方法作用在指定的数据类型上的(即：和指定的数据类型绑定), 因此自定义类型, 都可以有方法, 而不仅仅是struct,  比如int , float32 等都可以有方法;(这时需要自定义类型 内建类型不能直接作为接收类型)
4) 方法的访问范围控制的规则, 和函数一样. 方法名首字母小写, 只能在本包访问, 方法首字母大写, 可以在本包和其它包访问;
5) 如果一个类型实现了String()这个方法, 那么fmt.Println 默认会调用这个变量的String()进行输出.

    ```go
    type Student struct {
        Name string
        Age  int
    }

    func (stu *Student) String() string {
        str := fmt.Sprintf("Name = [%v]\tAge = [%v]", stu.Name, stu.Age)
        return str
    }

    func main() {

        stu := Student{
            Name: "Alice",
            Age:  20,
        }
        //如果实现了 *Student类型的String()方法, 就会自动调用
        fmt.Println(&stu, stu.String()) //Name = [Alice]  Age = [20] Name = [Alice]       Age = [20]

    }
    ```

    ```go
    type Student struct {
        Name string
        Age  int
    }

    func main() {

        stu := Student{
            Name: "Alice",
            Age:  20,
        }
        //如果实现了 *Student类型的String()方法, 就会自动调用
        fmt.Println(&stu) //&{Alice 20}

    }
    ```

### 练习 - 数组转置

```go
type array2Dim [3][3]int

func (arr *array2Dim) transpose() array2Dim {
	var t int
	for i := 0; i < len(arr); i++ {
		for j := i; j < len(arr[i]); j++ {
			if i != j {
				t = arr[i][j]
				arr[i][j] = arr[j][i]
				arr[j][i] = t
			}
		}
	}

	return *arr
}

func main() {

	arr := array2Dim{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}
	fmt.Println(arr) //[[1 2 3] [4 5 6] [7 8 9]]
	fmt.Println(arr.transpose()) //[[1 4 7] [2 5 8] [3 6 9]]

}
```

### 方法和函数的区别

1. 调用方式不一样
   
    - 函数的调用方式: 函数名(实参列表)
    - 方法的调用方式: 变量.方法名(实参列表)
2. 对于普通函数, 接收者为值类型时, 不能将指针类型的数据直接传递, 反之亦然
3. 对于方法（如 struct 的方法）, 接收者为值类型时, 可以直接用指针类型的变量调用方法, 反过来同样也可以
    ```go
    type Person struct {
        Name string
    }

    func (p Person) test() {
        p.Name = "Alice"
        fmt.Println("test()", p.Name)
    }
    func (p *Person) test01() {
        p.Name = "Alice"
        fmt.Println("test01()", p.Name)
    }

    func main() {

        p := Person{"Tom"}
        p.test()                      //test() Alice
        fmt.Println("main()", p.Name) //main() Tom

        (&p).test()                   //形式上传入地址, 实际上仍然是值拷贝 test() Alice
        fmt.Println("main()", p.Name) //main() Tom

        (&p).test01()                 //test01() Alice
        p.test01()                    //test01() Alice p.test01()等价于(&p).test01() 原因是编译器在底层做了优化
        fmt.Println("main()", p.Name) //main() Alice

    }
    ```     
    - 不管调用形式如何, 真正决定是值拷贝还是地址拷贝, 看这个方法是和哪个类型绑定.
    - 如果是和值类型, 比如  (p Person), 则是值拷贝, 如果和指针类型, 比如是 (p *Person)  则是地址拷贝. 

### 创建结构体变量时指定字段值



```go
type Student struct {
	Name   string
	Age    int
	Gender string
}

func main() {

	//方式1: 创建结构体变量时, 就直接指定字段的值

	//方式1的第一种写法, 需要严格按照字段顺序赋值
	var stu1 = Student{"Tom", 18, "male"}
	stu2 := Student{"Tom~", 18, "male"}

	//方式1的第二种写法, 不依赖字段顺序
	var stu3 = Student{
		Name:   "Tom",
		Gender: "male",
		Age:    20,
	}
	stu4 := Student{
		Name:   "Tom~",
		Gender: "male",
		Age:    20,
	}
	fmt.Println(stu1, stu2, stu3, stu4) //{Tom 18 male} {Tom~ 18 male} {Tom 20 male} {Tom~ 20 male}

	//方式2: 返回结构体的指针类型

	var stu5 = &Student{"Alice", 20, "female"} //stu5是一个结构体指针 存放着&Student{"Alice", 20, "female"} 的地址, 地址指向Student{"Alice", 20, "female"}
	stu6 := &Student{"Alice~", 20, "female"}

	var stu7 = &Student{
		Name:   "Alice",
		Gender: "female",
		Age:    20,
	}
	stu8 := &Student{
		Name:   "Alice~",
		Gender: "female",
		Age:    20,
	}
	fmt.Println(stu5, stu6, stu7, stu8) //&{Alice 20 female} &{Alice~ 20 female} &{Alice 20 female} &{Alice~ 20 female}

	fmt.Println(*stu5, *stu6, *stu7, *stu8) //{Alice 20 female} {Alice~ 20 female} {Alice 20 female} {Alice~ 20 female}

}
```

### Golang工厂模式详解

Golang 的结构体没有构造函数，通常可以使用工厂模式来解决这个问题。


一个结构体的声明是这样的:
```go
package model

type Student struct {
	Name  string
	Age   int
	Score float64
}
```

因为这里的 Student 的首字母 S 是大写的，如果我们想在其它包创建 Student 的实例(比如 main 包)， 引入 model 包后，就可以直接创建 Student 结构体的变量(实例)。如果Student中S变成了s, 就不能直接引用了. 需要用到工厂模式.

利用工厂模式挎包创建对象的一个示例:

```go
/* student.go*/

package model

//结构体首字母小写, 因此只能在model包中使用
type student struct {
	Name  string
	Age   int
	score float64 //如果字段首字母小写, 则在其他包不可以直接使用
}

func NewStudent(n string, a int, s float64) *student {

	return &student{
		Name:  n,
		Age:   a,
		score: s,
	}
}

func (s *student) GetScore() float64 {
	return s.score
}

/* main.go */

package main

import (
	"code/chapter10/factory/model"
	"fmt"
)

func main() {

	//创建Student实例
	// var stu1 = model.Student{
	// 	Name:  "Tom",
	// 	Age:   20,
	// 	Score: 80.8,
	// }
	// fmt.Println(stu1)

	//当Student结构体是首字母小写, 可以通过工厂模式来解决

	var stu = model.NewStudent("Tom", 20, 80.8)
	fmt.Println(stu)  //&{Tom 20 80.8}
	fmt.Println(*stu) //{Tom 20 80.8}
	//fmt.Println(stu.score) //无法访问s.score
	fmt.Println(stu.GetScore()) //80.8
}
```

