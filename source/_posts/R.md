---
title: Reports of the course Applied Statistics in R
date: 2020-05-25 19:36:16
tags:
- R
- Statistics
toc: True
---

Here are the reports of the course **Applied Statistics in R**.

Including the following 7 parts.
<!-- more -->

1. [Lecture 1 - Descriptive Statistics - Zhao Chi - 19.M09](/R/Report1.html)
2. [Lecture 2 - Tests - Zhao Chi - 19.M09](/R/Report2.html)
3. [Lecture 3 - Nonparametric Tests - Zhao Chi - 19.M09](/R/Report3.html)
4. [Lecture 4 - Linear Regression - Zhao Chi - 19.M09](/R/Report4.html)
5. [Lecture 5 - Ridge and Quantile Regressions - Zhao Chi - 19.M09](/R/Report5.html)
6. [Lecture 6 - Binary Regression - Zhao Chi - 19.M09](/R/Report6.html)
7. [Lecture 7 - Cluster Analysis - Zhao Chi - 19.M09](/R/Report7.html)