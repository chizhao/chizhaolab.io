---
title: Go语言编程系列(十四) - 文件操作
date: 2019-03-30 20:38:17
tags:
- Go
- 毕业论文
toc: True
---


### 文件操作以及常用方法介绍

文件在程序中是以流的形式来操作的. 

![mark](http://image.i-ll.cc/blog/20190330/symUrutIeRYK.png)\ 

文件操作需要用到包"os", File 是一个结构体, os.File 封装所有文件相关操作. 


![mark](http://image.i-ll.cc/blog/20190330/Ot02kLjYlE9X.png)\

[文档查阅地址](https://studygolang.com/static/pkgdoc/pkg/os.htm#File)


<!-- more -->

### 打开文件和关闭文件


![mark](http://image.i-ll.cc/blog/20190330/uT0XtyBQM3vY.png)\

以上两种读文件方式区别是:

1. Open打开一个名称为name的文件, 但是是只读方式, 内部实现其实调用了OpenFile. 
2. OpenFile打开名称为name的文件, flag是打开的方式, 只读、读写等, perm是权限.

![mark](http://image.i-ll.cc/blog/20190330/kqYiu9Ki0Tju.png)\

```go
package main

import (
	"fmt"
	"os"
)

func main() {
	//打开文件
	/*
		1. file叫file对象
		2. file叫file指针
		3. file叫file文件句柄
	*/
    file, err := os.Open("D:/1.txt")
	if err != nil {
		fmt.Println("open file error = ", err)
	}
	//输出文件内容, file就是一个指针
	fmt.Printf("file=%v\n", file)

	//关闭文件
	err = file.Close()
	if err != nil {
		fmt.Println("close file error = ", err)
    }
    /*
        如果文件不存在,则会输出以下内容
        open file error =  open D:/01.txt: The system cannot find the file specified.
        file=<nil>
        close file error =  invalid argument

    */
}
```

输出结果:

```
file=&{0xc00008e780}
```

### 读文件操作实例

1) 读取文件的内容并显示在终端(带缓冲区的方式), 使用 os.Open(), file.Close(), bufio.NewReader(),reader.ReadString()函数和方法. [bufio中文标准库](https://studygolang.com/static/pkgdoc/pkg/bufio.htm#Reader)
    ```go
    package main

    import (
        "bufio"
        "fmt"
        "io"
        "os"
    )

    func main() {

        file, err := os.Open("D:/1.txt")
        if err != nil {
            fmt.Println("open file error = ", err)
        }
        //当函数退出时, 要及时关闭file
        defer file.Close() //及时关闭file,否则会有内存泄漏

        //创建一个 *Reader, 是带缓冲的
        /*
            const (
            defaultBufSize = 4096 //默认缓冲区为4096字节
            )
        */
        reader := bufio.NewReader(file)
        //循环读取文件内容
        for {
            str, err := reader.ReadString('\n') //读到一个换行就结束
            if err == io.EOF {                  //io.EOF表示文件末尾
                break
            }
            //输出内容
            fmt.Print(str)
        }
        fmt.Println("文件读取结束")

    }
    ```

    输出结果:

    ```
    Hello
    World!
    文件读取结束
    ```
2) 读取文件的内容并显示在终端(使用 ioutil 一次将整个文件读入到内存中), 这种方式适用于文件不大的情况. 相关方法和函数(ioutil.ReadFile())
    ```go
    package main

    import (
        "fmt"
        "io/ioutil"
    )

    func main() {
        //使用ioutil.ReadFile一次将文件读取到为
        file := "d:/1.txt"
        content, err := ioutil.ReadFile(file)
        if err != nil {
            fmt.Println("read file error", err)
        }
        //把读取到的内容显示到终端
        fmt.Printf("%v\n", content) //[]byte
        //因为没有显式的Open文件, 所以不需要显式的close
        //因为,文件的Open和Close被封装到ReadFile函数内部
        fmt.Printf("%v\n", string(content)) //因为content是[]byte类型, 所以需要转成字符串在输出
    }
    ```

    输出结果

    ```
    [72 101 108 108 111 13 10 87 111 114 108 100 33 13 10]
    Hello
    World!

    ```

### 写文件操作实例

#### os.OpenFile()介绍

![mark](http://image.i-ll.cc/blog/20190330/YJWTfC3HhC9n.png)\


第二个参数: 文件打开模式

```go
const (
    O_RDONLY int = syscall.O_RDONLY // 只读模式打开文件
    O_WRONLY int = syscall.O_WRONLY // 只写模式打开文件
    O_RDWR   int = syscall.O_RDWR   // 读写模式打开文件
    O_APPEND int = syscall.O_APPEND // 写操作时将数据附加到文件尾部
    O_CREATE int = syscall.O_CREAT  // 如果不存在将创建一个新文件
    O_EXCL   int = syscall.O_EXCL   // 和O_CREATE配合使用, 文件必须不存在
    O_SYNC   int = syscall.O_SYNC   // 打开文件用于同步I/O
    O_TRUNC  int = syscall.O_TRUNC  // 如果可能, 打开时清空文件
)
```

第三个参数: 权限控制(只在Linux/Unix有效) 

![mark](http://image.i-ll.cc/blog/20190330/7tgEFdP3Os8J.png)\

**文件权限的字符与数字表示**

|  权限项  |  读   |  写   | 执行  |
| :------: | :---: | :---: | :---: |
| 字符表示 |   r   |   w   |   x   |
| 数字表示 |   4   |   2   |   1   |

比如说: 0777 第一位0代表八进制, 第二位代表文件所有者, 第三位代表文件所属组的权限, 最后一位代表其他用户权限. 7代表可读可写可执行.

#### 写文件应用案例

1. 创建一个新文件, 写入内容5 句"hello, Gardon";

    ```go
    package main

    import (
        "bufio"
        "fmt"
        "os"
    )

    func main() {
        //创建一个新文件, 写入内容5 句"hello, Gardon";
        // 1. 打开文件 d:/abc.txt (文件不存在)
        filePath := "d:/abc.txt"
        file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_CREATE, 0666)
        if err != nil {
            fmt.Printf("open file err=%v", err)
            return
        }
        //及时关闭file句柄
        defer file.Close()

        //准备写入5句 hello Gardon
        str := "hello Gardon\n"
        //写入时, 使用带缓存的*Writer
        writer := bufio.NewWriter(file)
        for i := 0; i < 5; i++ {
            writer.WriteString(str)
        }

        //因为writer是带缓存的, 因此在调用WriteString方法时, 内容是先写入到缓存的,
        //所以需要调用Flush方法, 将缓存中的数据真正写入到文件中, 否则文件中会没有数据
        writer.Flush()
    }
    ```
2. 打开一个存在的文件中, 将原来的内容覆盖成新的内容10 句"你好, 世界!", 对以上代码稍作修改即可;
    ```go
    package main

    import (
        "bufio"
        "fmt"
        "os"
    )

    func main() {

        // 	2. 打开一个存在的文件中, 将原来的内容覆盖成新的内容10 句"你好, 世界!"
        filePath := "d:/abc.txt" //文件存在
        file, err := os.OpenFile(filePath, os.O_WRONLY|os.O_TRUNC, 0666)
        if err != nil {
            fmt.Printf("open file err=%v", err)
            return
        }
        //及时关闭file句柄
        defer file.Close()

        //准备写入10句 你好, 世界!
        str := "你好, 世界!\r\n" //\r\n表示换行. 为了更好的适应编辑器
        //写入时, 使用带缓存的*Writer
        writer := bufio.NewWriter(file)
        for i := 0; i < 10; i++ {
            writer.WriteString(str)
        }
        //因为writer是带缓存的, 因此在调用WriteString方法时, 内容是先写入到缓存的,
        //所以需要调用Flush方法, 将缓存中的数据真正写入到文件中, 否则文件中会没有数据
        writer.Flush()

    }
    ```
3. 打开一个存在的文件, 在原来的内容追加内容"ABC! ENGLISH!", 同样也是对以上代码稍作修改. (将os.O_TRUNC改成os.APPEND,并将写入的内容改掉即可);
4. 打开一个存在的文件, 将原来的内容读出显示在终端, 并且追加5 句"hello,北京!" 代码实现: 
    ```go
    package main

    import (
        "bufio"
        "fmt"
        "io"
        "os"
    )

    func main() {

        // 	4. 打开一个存在的文件, 将原来的内容读出显示在终端, 并且追加5 句"hello,北京!"
        filePath := "d:/abc.txt" //文件存在
        file, err := os.OpenFile(filePath, os.O_RDWR|os.O_APPEND, 0666)
        if err != nil {
            fmt.Printf("open file err=%v", err)
            return
        }
        //及时关闭file句柄
        defer file.Close()

        //先读取原来文件内容并显示在终端
        reader := bufio.NewReader(file)
        //循环读取文件内容
        for {
            str, err := reader.ReadString('\n') //读到一个换行就结束
            if err == io.EOF {                  //io.EOF表示文件末尾
                break
            }
            //输出内容到终端
            fmt.Print(str)
        }

        //准备写入5句 hello,北京!!
        str := "hello,北京!\r\n" //\r\n表示换行. 为了更好的适应编辑器
        //写入时, 使用带缓存的*Writer
        writer := bufio.NewWriter(file)
        for i := 0; i < 5; i++ {
            writer.WriteString(str)
        }
        //因为writer是带缓存的, 因此在调用WriteString方法时, 内容是先写入到缓存的,
        //所以需要调用Flush方法, 将缓存中的数据真正写入到文件中, 否则文件中会没有数据
        writer.Flush()

    }
    ```
5. 编程一个程序, 将一个文件的内容, 写入到另外一个文件. 注：这两个文件已经存在了
    ```go
    package main

    import (
        "fmt"
        "io/ioutil"
    )

    func main() {

        /*
            编程一个程序, 将一个文件的内容, 写入到另外一个文件. 注：这两个文件已经存在了.
            说明：使用ioutil.ReadFile / ioutil.WriteFile 完成写文件的任务.
        */
        //1. 首先 将d:/abc.txt内容读取到内存
        //2. 将读取到的内容写入到 d:/1.txt

        file1Path := "d:/abc.txt"
        file2Path := "d:/1.txt"
        content, err := ioutil.ReadFile(file1Path)
        if err != nil {
            //说明文件读取有误
            fmt.Println("read file err:", err)
            return
        }

        ioutil.WriteFile(file2Path, content, 0666)
        if err != nil {
            fmt.Println("write file err:", err)
            return
        }

    }
    ```

### 文本编程应用案例

#### 判断文件是否存在 

判断文件是否存在: 可以通过os.Stat(path)函数返回的错误值进行判断:

    - 如果返回的错误为nil, 则说明文件或文件夹存在;
    - 如果返回的错误类型使用os.IsNotExist()判断为true, 说明文件或文件夹不存在;
    - 如果返回的错误为其他类型, 则不确定是否存在. 
    
    ![mark](http://image.i-ll.cc/blog/20190330/AjDFBzz1nrmG.png)\

#### 拷贝文件

- 说明：将一张图片/电影/mp3 拷贝到另外一个文件 e:/门神.jpg io 包
- func Copy(dst Writer, src Reader) (written int64, err error) 
- **注意**: Copy  函数是 io 包提供的.

```go
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

func CopyFile(srcFileName string, dstFileName string) (written int64, err error) {
	srcFile, err := os.Open(srcFileName)
	if err != nil {
		fmt.Println("open file error:", err)
	}

	defer srcFile.Close()

	//通过scrfile, 获取到reader
	reader := bufio.NewReader(srcFile)

	//打开通过dstFileName
	dstFile, err := os.OpenFile(dstFileName, os.O_WRONLY|os.O_CREATE, 0666)
	if err != nil {
		fmt.Printf("open file err=%v", err)
		return
	}
	//通过dstFileName, 获取到Writer
	writer := bufio.NewWriter(dstFile)
	defer dstFile.Close()

	return io.Copy(writer, reader)
}

func main() {

	/*
		编程一个程序, 将D:/门神.jpg 拷贝到 E:/门神.jpg
		说明：使用ioutil.ReadFile / ioutil.WriteFile 完成写文件的任务.
	*/

	srcFile := "d:/门神.jpg"
	dstFile := "e:/门神.jpg"

	_, err := CopyFile(srcFile, dstFile)
	if err == nil {
		fmt.Println("拷贝完成")
	} else {
		fmt.Println("copy error ", err)
	}
}
```

#### 统计英文、数字、空格和其他字符数量

```go
package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
)

type CharCount struct {
	EnCount    int //记录英文个数
	NumCount   int //记录数字个数
	SpaCount   int //记录空格个数
	OtherCount int //记录其他字符个数
}

func main() {
	/*
		说明：统计一个文件中含有的英文、数字、空格及其它字符数量

		思路: 打开一个文件, 创建一个Reader
		每读取一行,就去统计该行有多少个英文、数字、空格及其它字符
		然后将结果保存到一个结构体
	*/
	fileName := "D:/abc.txt"
	file, err := os.Open(fileName)
	if err != nil {
		fmt.Println("open file error:", err)
	}
	defer file.Close()
	//定义一个CharCount实例
	var count CharCount
	//创建一个Reader
	reader := bufio.NewReader(file)

	//开始循环读取fileName的内容
	for {
		str, err := reader.ReadString('\n')
		if err == io.EOF {
			break
		}
		//遍历str,进行统计
		for _, v := range str {
			switch {
			case v >= 'a' && v <= 'z':
				fallthrough //穿透
			case v >= 'A' && v <= 'Z':
				count.EnCount++
			case v == ' ' || v == '\t':
				count.SpaCount++
			case v >= '0' && v <= '9':
				count.NumCount++
			default:
				count.OtherCount++
			}
		}
	}
	fmt.Printf("英文字符的数量:%v\n数字的数量:%v\n空格的数量:%v\n其他字符的数量:%v\n",
		count.EnCount, count.NumCount, count.SpaCount, count.OtherCount)
}
```

### 命令行参数

如果我们希望能够获取到命令行输入的各种参数, 应该如何处理? 

os.Args 是一个string 的切片, 用来存储所有的命令行参数

举例说明: 请编写一段代码, 可以获取命令行各个参数

```go
package main

import (
	"fmt"
	"os"
)

func main() {

	/*
		请编写一段代码, 可以获取命令行各个参数
	*/

	fmt.Printf("命令行的参数有%v个\n", len(os.Args))

	//遍历os.Args切片, 就可以得到所有的命令行输入参数值

	for i, v := range os.Args {
		fmt.Printf("args[%v]=%v\n", i, v)
	}
}
```

```shell
go build .\src\code\chapter14\argsdemo\main.go
.\main.exe a b c d
```

输出结果:

```
命令行的参数有5个
args[0]=F:\alw\Documents\2019_ZhaoChi\GoProjects\main.exe
args[1]=a
args[2]=b
args[3]=c
args[4]=d
```

说明:  前面的方式是比较原生的方式, 对解析参数不是特别的方便, 特别是带有指定参数形式的命令行. 比如：cmd>main.exe -u root -pwd root -h 127.0.0.1 -port 3306  这样的形式命令行, go 设计者给我们提供了[flag包](http://docscn.studygolang.com/pkg/flag/), 可以方便的解析命令行参数, 而且参数顺序可以随意. **注意**在所有的flag注册好之后需要执行	flag.Parse()函数. 


![mark](http://image.i-ll.cc/blog/20190331/j5dsfcUaeRgy.png)\

**一个栗子:**

```go
package main

import (
	"flag"
	"fmt"
)

func main() {
	//定义几个变量, 用于接收命令行的参数值
	var user string
	var pwd string
	var host string
	var port int
	//&user就是接收用户命令行中输入的 -u后面的参数值
	//"u" 就是指定参数xx.exe -u
	//""默认值
	//"用户名, 默认为空" 说明
	flag.StringVar(&user, "u", "", "用户名, 默认为空")
	flag.StringVar(&pwd, "pwd", "", "密码, 默认为空")
	flag.StringVar(&host, "h", "localhost", "主机名, 默认为localhost")
	flag.IntVar(&port, "port", 3306, "端口号, 默认为3306")

	//这里有一个非常重要的操作, 转换, 必须调用该方法
	flag.Parse()

	//输出结果:
	fmt.Printf("user=%v\tpwd=%v\thost=%v\tport=%v",
		user, pwd, host, port)
}
```

```
go build .\src\code\chapter14\argsdemo02\main.go
.\main.exe -u root -port 3306 -pwd biubiu -h 127.0.0.1
```

输出结果:

```
user=root       pwd=biubiu      host=127.0.0.1  port=3306
```

### JSON处理

#### JSON介绍

JSON  （JavaScript Object Notation）是一种比 XML 更**轻量级的数据交换格式**, 在易于人们阅读和编写的同时, 也易于程序解析和生成 ,并有效地提升网络传输效率, 通常程序在网络传输时会先将数据(结构体, map等)**序列化**成json字符串, 到接收方得到json字符串时, 在**反序列化**恢复成原来的数据类型(结构体, map等). 尽管 JSON 是JavaScript 的一个子集, 但 JSON 采用完全独立于编程语言的文本格式, 且表现为**键/值对集合**的文本描述形式（类似一些编程语言中的字典结构）, 这使它成为较为理想的、跨平台、跨语言的数据交换语言. 

开发者可以用 JSON 传输简单的字符串、数字、布尔值, 也可以传输一个数组, 或者一个更复杂的复合结构. 在 Web 开发领域中,  JSON 被广泛应用于 Web 服务端程序和客户端之间的数据通信. 

Go 语言内建对 JSON 的支持. 使用 Go 语言内置的 encoding/json 标准库, 开发者可以轻松使用Go 程序生成和解析 JSON 格式的数据. 

#### JSON数据格式说明

在JS语言中, 一切都是对象. 因此, 任何的数据类型都可以通过json表示. 例如: 字符串、数字、对象、数组、map,结构体等.

**JSON键值对**是用来保存数据的一种方式, 键/值对组合中的键名写在前面, 并用双引号包裹, 冒号分隔, 然后紧跟值. 如果值有多个, 可以写成数组形式. 例如:

```json
{
    "name":"淡淡",
    "subjects":[
        "Golang",
        "C",
        "Python"
    ],
    "age":18
}
```

[JSON数据在线解析](https://www.json.cn/), 这个网站可以验证一个json 格式的数据是否正确. 尤其是在我们编写比较复杂的json 格式数据时, 很有用. 


#### JSON序列化


json 序列化是指, 将有 key-value 结构的数据类型(比如结构体、map、切片)序列化成 json 字符串的操作. 这里我们介绍一下结构体、map 和切片的序列化, 其它数据类型的序列化类似. 

使用**json.Marshal()**函数可以对一组数据进行JSON格式的编码. 

```go
func Marshal(v interface{}) ([]byte, error) {} //json.Marshal()函数原型
```

**栗子如下**

```go
package main

import (
	"encoding/json"
	"fmt"
)

type Monster struct {
	Name     string
	Age      int
	Birthday string
	Sal      float64
	Skill    string
}

//将Struct序列化
func testStruct() {
	//演示
	monster := Monster{
		Name:     "牛魔王",
		Age:      500,
		Birthday: "1555-05-05",
		Sal:      5000,
		Skill:    "牛魔拳",
	}
	//将monster序列化
	data, err := json.Marshal(&monster)
	if err != nil {
		fmt.Println("序列化失败 error:", err)
	}
	//输出序列化后的结果
	fmt.Printf("monster序列化后=%v\n", string(data))
}

//将map序列化

func testMap() {
	//定义一个map
	var a map[string]interface{}
	//使用map之前需要make
	a = make(map[string]interface{})
	a["name"] = "红孩儿"
	a["age"] = 300
	a["address"] = "火云洞"
	//将a序列化
	data, err := json.Marshal(&a)
	if err != nil {
		fmt.Println("序列化失败 error:", err)
	}
	//输出序列化后的结果
	fmt.Printf("a序列化后=%v\n", string(data))

}

//对切片进行序列化 []map[string]interface{}

func testSlice() {
	var slice []map[string]interface{}
	var m1 map[string]interface{}
	//使用map前, 需要先make
	m1 = make(map[string]interface{})
	m1["name"] = "jack"
	m1["age"] = "7"
	m1["address"] = "北京"
	slice = append(slice, m1)
	var m2 map[string]interface{}
	//使用map前, 需要先make
	m2 = make(map[string]interface{})
	m2["name"] = "tom"
	m2["age"] = "8"
	m2["address"] = [2]string{"墨西哥", "夏威夷"}
	slice = append(slice, m2)

	//将slice进行序列化操作
	//将slice序列化
	data, err := json.Marshal(&slice)
	if err != nil {
		fmt.Println("序列化失败 error:", err)
	}
	//输出序列化后的结果
	fmt.Printf("slice序列化后=%v\n", string(data))

}

//对基本数据类型序列化, 对基本数据类型序列化意义不大

func testFloat64() {
	var num1 float64 = 2345.6789
	//将num1序列化
	data, err := json.Marshal(&num1)
	if err != nil {
		fmt.Println("序列化失败 error:", err)
	}
	//输出序列化后的结果
	fmt.Printf("num1序列化后=%v\n", string(data))
}

func main() {
	testStruct()
	testMap()
	testSlice()
	testFloat64()
}
```

输出结果:

```
monster序列化后={"Name":"牛魔王","Age":500,"Birthday":"1555-05-05","Sal":5000,"Skill":"牛魔拳"}
a序列化后={"address":"火云洞","age":300,"name":"红孩儿"}
slice序列化后=[{"address":"北京","age":"7","name":"jack"},{"address":["墨西哥","夏威夷"],"age":"8","name":"tom"}]
num1序列化后=2345.6789
```

注意事项:

对于结构体的序列化, 如果我们希望序列化后的key 的名字, 又我们自己重新制定, 那么可以给struct指定一个tag 标签, 如下所示

```go
type Monster struct {
	Name     string  `json:"monster_name"` //反射机制
	Age      int     `json:"monster_age"`
	Birthday string  `json:"monster_birthday"`
	Sal      float64 `json:"monster_sal"`
	Skill    string  `json:"monster_skill"`
}
```

则对Monster序列化之后的输出变为:

```
monster序列化后={"monster_name":"牛魔王","monster_age":500,"monster_birthday":"1555-05-05","monster_sal":5000,"monster_skill":"牛魔拳"}
```

#### JSON反序列化

json 反序列化是指, 将json 字符串反序列化成对应的数据类型(比如结构体、map、切片)的操作

可以使用**json.Unmarshal()**函数将JSON格式的文本解码为Go里面预期的数据结构. 

```go
func Unmarshal(data []byte, v interface{}) error {} //json.Unmarshal()函数原型
```

该函数的第一个参数是输入, 即JSON格式的文本（比特序列）, 第二个参数表示目标输出容器, 用于存放解码后的值. 

**栗子如下:**

```go
package main

import (
	"encoding/json"
	"fmt"
)

type Monster struct {
	Name     string
	Age      int
	Birthday string
	Sal      float64
	Skill    string
}

//演示将json字符串, 反序列化成struct

func unserialSturct() {
	//str在项目开发中,是通过网络传输获取到的... 或者读取文件获取到的
	str := "{\"Name\":\"牛魔王\",\"Age\":500,\"Birthday\":\"1555-05-05\",\"Sal\":5000,\"Skill\":\"牛魔拳\"}" //双引号里面的双引号需要转义

	//定义一个Monster实例
	var monster Monster
	err := json.Unmarshal([]byte(str), &monster)
	if err != nil {
		fmt.Println("Unmarshal error :", err)
	}
	fmt.Printf("反序列化后monster =%v\t monster.Name=%v\n", monster, monster.Name)
}

//演示将json字符串,反序列化成map

func unserialMap() {
	str := "{\"address\":\"火云洞\",\"age\":300,\"name\":\"红孩儿\"}" //双引号里面的双引号需要转义
	//定义一个map
	var a map[string]interface{}
	//反序列化
	//注意: 反序列化map 不需要make, 因为make操作被封装到Unmarshal函数中了
	err := json.Unmarshal([]byte(str), &a)
	if err != nil {
		fmt.Println("Unmarshal error :", err)
	}
	fmt.Printf("反序列化后a =%v\n", a)
}

//演示将json字符串,反序列化成slice

func unserialSlice() {
	str := "[{\"address\":\"北京\",\"age\":\"7\",\"name\":\"jack\"}," +
		"{\"address\":[\"墨西哥\",\"夏威夷\"],\"age\":\"8\",\"name\":\"tom\"}]"
	//定义一个slice
	var slice []map[string]interface{}
	//反序列化
	//注意: 反序列化map 不需要make, 因为make操作被封装到Unmarshal函数中了
	err := json.Unmarshal([]byte(str), &slice)
	if err != nil {
		fmt.Println("Unmarshal error :", err)
	}
	fmt.Printf("反序列化后slice =%v\n", slice)

}

func main() {
	unserialSturct()
	unserialMap()
	unserialSlice()

}
```

**注意事项:**

1) 在反序列化一个json 字符串时, 要确保反序列化后的数据类型和原来序列化前的数据类型一致. 
2) 如果 json 字符串是通过程序获取到的, 则不需要再对 “  转义处理. 
3) 如果结构体带了tag标签, 则获取到的字符串中key应该和tag中保持一致. 

    ![mark](http://image.i-ll.cc/blog/20190331/9NW8J0F45Lyw.png)\