---
title: Go语言编程系列(十) - 函数(一)
date: 2019-03-11 15:02:31
tags:
- Go
- 毕业论文
toc: True
---


### 函数的定义

在编程中, 通常会有同一个功能的重复使用, 这时, 我们如果不定义函数的话, 可能会造成以下问题:

1. 代码冗余严重;
2. 代码不易维护;

这个时候就需要函数了. 这时候可以得出**函数的定义** - 为实现某一个功能的程序指令的集合. 

在Go语言中, 函数分为: 自定义函数和系统函数(查看Golang文档).

<!-- more -->

### 函数的基本语法 

```go
func 函数名(形参列表) (返回值类型列表){
    执行语句...
    return 返回值列表
}
```

1. 形参列表: 表示函数的输入;
2. 返回值类型列表: 表示返回值的类型, 如果返回值只有一个, 可以不带括号;
3. 返回值列表: 表示函数的返回值(返回值可有可无);
4. 执行语句: 为了实现某一个功能的代码块.

**一个栗子**

```go
func calc(n1 float64, n2 float64, operator byte) float64 {
	var res float64
	switch operator {
	case '+':
		res = n1 + n2
	case '-':
		res = n1 - n2
	case '*':
		res = n1 * n2
	case '/':
		res = n1 / n2
	default:
		fmt.Println("操作符号错误,请重新输入")
	}
	return res

}

func main() {
	var n1 float64
	var n2 float64
	var operate byte
	var res float64
	fmt.Println("请输入你要计算的式子 数字与操作符用空格隔开!")
	fmt.Scanf("%v %c %v", &n1, &operate, &n2)
	res = calc(n1, n2, operate)
	fmt.Println(res)
}

```

输出结果:

```
请输入你要计算的式子 数字与操作符用空格隔开!
9 + 9
18
```

### 包

#### 包的引入

1) 在实际的开发中, 我们往往需要在不同的文件中, 去调用其它文件的定义的函数, 比如 main.go中, 去使用 utils.go 文件中的函数, 如何实现？ -》包
2)  现在有两个程序员共同开发一个 Go 项目,程序员 xiaoming 希望定义函数 Cal ,程序员 xiaoqiang也想定义函数也叫 Cal. 两个程序员为此还吵了起来,怎么办? -》包

#### 包的本质

**包的本质实际上就是创建不同的文件夹, 来存放程序文件. Go的每一个文件都是属于一个包的, 也就是说 go 是以包的形式来管理文件和项目目录结构的.**


#### 包的作用

1. 区分相同名字的函数、变量等标识符
2. 当程序文件很多时,可以很好的管理项目
3. 控制函数、变量等访问范围, 即作用域

#### 包的使用

**打包基本语法**

```go
package 包名
```

**引入包的基本语法**

```go
import "包的路径"
```

**一个栗子**

我们将上面案例中的函数calc放到另外一个包utils中, 可以做如下操作:

1. 新建文件夹utils;
2. 在文件夹中新建utils.go文件;
3. 然后在main.go中引入utils.go中的函数;

需要注意的事情是, Go中没有public和private的声明, 默认函数开头大写为public, 小写为private. 

目录结构如图所示:

![mark](http://image.i-ll.cc/blog/20190311/VHXE7qEivDdO.png)\

**utils.go**

```go
package utils

import "fmt"

func Calc(n1 float64, n2 float64, operator byte) float64 {
	var res float64
	switch operator {
	case '+':
		res = n1 + n2
	case '-':
		res = n1 - n2
	case '*':
		res = n1 * n2
	case '/':
		res = n1 / n2
	default:
		fmt.Println("操作符号错误,请重新输入")
	}
	return res

}
```

**main.go**

```go
package main

import (
	"code/chapter06/funcdemo/utils"
	"fmt"
)

func main() {
	var n1 float64
	var n2 float64
	var operate byte
	var res float64
	fmt.Println("请输入你要计算的式子 数字与操作符用空格隔开!")
	fmt.Scanf("%v %c %v", &n1, &operate, &n2)
	res = utils.Calc(n1, n2, operate)
	fmt.Println(res)
}
```

运行结果和之前的栗子是一样的. 

#### 包使用的注意事项和细节讨论

1. 在给一个文件打包时, 该包对应一个文件夹, 比如这里的 utils 文件夹对应的包名就是 utils, 文件的包名**通常**和文件所在的文件夹名一致, 一般为小写字母;
2. 当一个文件要使用其它包函数或变量时, 需要先引入对应的包;
   1. 方式1: import "包名"
   2. 方式2:
        ```go
        import   (
            "包名"
            "包名"
        )
        ```
    3. package  指令在 文件第一行, 然后是 import 指令. 
    4. 在 import 包时, 路径从 \$GOPATH的src下开始, 不用带 src,编译器会自动从src下开始引入. 
3. 为了让其它包的文件, 可以访问到本包的函数, 则该函数名的首字母需要大写, 类似其它语言的public, 这样才能跨包访问; 
4. 在访问其它包函数, 变量时, 其语法是包名.函数名;
5. 如果包名较长, Go支持给包取别名, 注意细节: 取别名后, 原来的包名就不能使用了; 如下所示:
    ```go
    package main
    import (
        util "code/chapter06/funcdemo/utils"
        "fmt"
    )

    func main() {
        var n1 float64
        var n2 float64
        var operate byte
        var res float64
        fmt.Println("请输入你要计算的式子 数字与操作符用空格隔开!")
        fmt.Scanf("%v %c %v", &n1, &operate, &n2)
        res = util.Calc(n1, n2, operate)
        fmt.Println(res)
    }
    ```
6.  在同一**包**下, 不能有相同的函数名(也不能有相同的全局变量名), 否则报重复定义;
7.  如果你要编译成一个可执行程序文件, 就需要将这个包声明为 main, 即 package main .这个就是一个语法规范, 如果你是写一个库 , 包名可以自定义.
    1.  首先切换工作目录到GOPATH路径下;
    2.  目录结构如下:

        ![mark](http://image.i-ll.cc/blog/20190311/VHXE7qEivDdO.png)\
    
    3. 编译main包, 生成的文件放到bin/my.exe下:
        ```shell
        go build -o bin/my.exe code/chapter06/funcdemo/main
        ```
### 函数的调用

#### 函数的调用机制

1. 在调用一个函数时, 会给该函数分配一个新的空间, 编译器会通过自身的处理让这个新的空间和其它的栈的空间区分开来;
2. 在每个函数对应的栈中, 数据空间是独立的, 不会混淆;
3. 当一个函数调用完毕(执行完毕)后, 程序会销毁这个函数对应的栈空间. 

**一个栗子**

```go
package main

import (
	"fmt"
)

func test(n1 int) {
	n1 = n1 + 1
	fmt.Println("n1 test()=", n1) //11
}

func main() {
	n1 := 10
	test(n1)

	fmt.Println("n1 main()=", n1) //10

}
```

输出结果:

```go
n1 test()= 11
n1 main()= 10
```

#### return语句详解



```go
func 函数名(形参列表) (返回值类型列表){
    执行语句...
    return 返回值列表
}
```

1. 如果返回多个值, 在接收时, 希望忽略某个返回值, 则使用_符号表示占位忽略;
2. 如果返回值只有一个, 则返回值列表可以不写括号

**一个栗子**

```go
func getSumAndSub(n1 int, n2 int) (int, int) {

	sum := n1 + n2
	sub := n1 - n2
	return sum, sub
}

func main() {
	var sum int
	var sub int
	sum, sub = getSumAndSub(10, 20)

	fmt.Printf("sum = %v sub = %v", sum, sub)
}
```

如果只想要sum,可以这样写, 因为int类型初始值是0,所以下例中的sub=0:

```go
func getSumAndSub(n1 int, n2 int) (int, int) {

	sum := n1 + n2
	sub := n1 - n2
	return sum, sub
}

func main() {
	var sum int
	var sub int
	sum, _ = getSumAndSub(10, 20)

	fmt.Printf("sum = %v sub = %v", sum, sub)
}
```

### 函数递归调用

大家都听过这个故事:
>从前有座山,山上有座庙,庙里有个老和尚给小和尚将故事,故事内容是从前有座山,山上有座庙,庙里有个老和尚给小和尚讲故事......

一个函数在函数体内又调用了本身,这个过程就叫做函数的递归调用. 与上门那个故事不同的是函数的递归调用需要一个**终点**, 也就是递归停止的条件, 不然就成了死递归.

**递归调用的总结**
1) 执行一个函数时, 就创建一个新的受保护的独立空间(新函数栈);
2) 函数的局部变量是独立的, 不会相互影响;
3) 递归必须向退出递归的条件逼近, 否则就是无限递归, 死龟了;
4) 当一个函数执行完毕, 或者遇到 return, 就会返回, 遵守谁调用, 就将结果返回给谁, 同时当函数执行完毕或者返回时, 该函数运行时所占的空间也会被系统销毁. 

**递归调用的三个经典例题**

题目1:

```go
/*
题1：斐波那契数
请使用递归的方式, 求出斐波那契数 1,1,2,3,5,8,13...
给你一个整数n, 求出它的斐波那契数是多少？
*/

func fibonacci(n int) int {
	if n == 1 || n == 2 {
		return 1
	} else {
		return fibonacci(n-1) + fibonacci(n-2)
	}
}

func main() {
	var n int
	fmt.Println("请输入一个数字n,以求Fibonacci数列第n项")
	fmt.Scanf("%v", &n)
	fmt.Println(fibonacci(n))
}
```

输出结果:

```
请输入一个数字n,以求Fibonacci数列第n项
7
13
```

题目2:

```go
/*

题2：求函数值
已知 f(1)=3; f(n) = 2*f(n-1)+1;
请使用递归的思想编程, 求出 f(n)的值?

*/

func getRes(n int) int {
	if n == 1 {
		return 3
	} else {
		return 2*getRes(n-1) + 1
	}
}

func main() {
	var n int
	fmt.Println("请输入一个数字n,以求f(n)")
	fmt.Scanf("%v", &n)
	fmt.Println(getRes(n))
}
```

输出结果:

```
请输入一个数字n,以求f(n)
3
15
```


**题目3**

```go

/*


题3：猴子吃桃子问题
有一堆桃子, 猴子第一天吃了其中的一半, 并再多吃了一个！
以后每天猴子都吃其中的一半, 然后再多吃一个. 
当到第十天时, 想再吃时（还没吃）, 发现只有 1 个桃子了. 问题：最初共多少个桃子？


思路分析:
1. 第9天的时候桃子数列为n9, 即可推出 n9 - (n9/2+1) = n10 即n9=2*(n10+1)
2. 可以通过n10,算出n9,通过n9算出n8直到算出n1
3. 规律: 第n天的桃子数量就是peach(n) = peach(n+1) * 2



*/


func peach(day int) int {
	if day > 10 || day < 1 {
        fmt.Println("输入错误")
        return 0 //0表示没有得到正确输入
	}
	if day == 10 {
		return 1
	} else {
		return 2 * (peach(day+1) + 1)
	}
}

func main() {
	fmt.Println("第1天的桃子数量为:", peach(1)) //1534
}

```

### 函数注意事项和细节

1) 函数的形参列表可以是多个, 返回值列表也可以是多个;
2) 形参列表和返回值列表的数据类型可以是值类型和引用类型;
3) 函数的命名遵循标识符命名规范, 首字母不能是数字, 首字母大写该函数可以被本包文件和其它包文件使用, 类似 public , 首字母小写, 只能被本包文件使用, 其它包文件不能使用, 类似 privat;
4) 函数中的变量是局部的, 函数外不生效;
5) 基本数据类型和数组默认都是值传递的, 即进行值拷贝. 在函数内修改, 不会影响到原来的值;
	```go
	func test(n1 int) {
		n1 += 10
		fmt.Println("n1 test() = ", n1)
	}

	func main() {

		n1 := 6
		test(n1)
		fmt.Println("n1 main() = ", n1)
	}
	```

	输出结果:
	```
	n1 test() =  16
	n1 main() =  6
	```
6) 如果希望函数内的变量能修改函数外的变量(指的是默认以值传递的方式的数据类型), 可以传入变量的地址&, 函数内以指针的方式操作变量.从效果上看类似引用. 对以上代码稍作修改:
	```go
	func test(n1 *int) {
		*n1 += 10
		fmt.Println("n1 test() = ", *n1)
	}

	func main() {

		n1 := 6
		test(&n1)
		fmt.Println("n1 main() = ", n1)

	}
	```

	输出结果：

	```
	n1 test() =  16
	n1 main() =  16
	```
7) Go函数不支持函数重载, 重复定义函数会报错;
8) 在 Go 中, 函数也是一种数据类型, 可以赋值给一个变量, 则该变量就是一个函数类型的变量了. 通过该变量可以对函数调用;
	```go
	func getSum(n1 int, n2 int) int {
		return n1 + n2
	}

	func main() {

		a := getSum
		fmt.Printf("a的类型是%T,getSum的类型是%T\n", a, getSum) //a和getSum是同一种类型, 都是函数类型

		/*
			a(10,20)等价于getSum(10,20)
		*/
		res1 := a(10, 20)
		res2 := getSum(10, 20)
		fmt.Println(res1)
		fmt.Println(res2)

	}
	```

	输出结果:
	
	```
	a的类型是func(int, int) int,getSum的类型是func(int, int) int
	30
	30
	```
9) 函数既然是一种数据类型, 因此在 Go 中, 函数可以作为形参, 并且调用;
	```go
	func getSum(n1 int, n2 int) int {
		return n1 + n2
	}

	func test(funvar func(int, int) int, n1 int, n2 int) int {
		return funvar(n1, n2)
	}
	func main() {

		res3 := test(getSum, 50, 60)
		fmt.Println("res3=", res3) //110
	}
	```
10) 为了简化数据类型定义, Go支持自定义数据类型;
	```go
    基本语法：type 自定义数据类型名 数据类型  //  相当于一个别名
	案例：type myInt int   // 这时 myInt 就等价 int 来使用了.
	案例：type mySum  func (int, int) int // 这时mySum 就等价一个函数类型 
	```

	Go语言认为自己定义的类型与之前的不是同一个类型
11) Go语言支持对返回值命名;
	```go
	/*
	这个函数可以将它改成另外一种形式, 见下面
	*/
	func getSumAndSub(n1 int, n2 int) (int, int) {

		sum := n1 + n2
		sub := n1 - n2
		return sum, sub
	}

	func getSumAndSub2(n1 int, n2 int) (sum int, sub int) {

		sum := n1 + n2
		sub := n1 - n2
		return
	}
	```

	这样写的好处就是, 不用害怕返回值的顺序写错.
12) 使用_标识符, 忽略返回值;
13) Go支持可变参数.
	```go
	//支持0到多个参数
	func 函数名(args...int) (sum int){
		执行语句
	}
	//支持1到多个参数
	func 函数名(n1 int, args...int) (sum int) {
		执行语句
	}
	```
	说明: 
	1. **args是slice切片**,通过args[index],可以访问各个值.
	2. 如果一个函数的形参列表中有可变参数, 则可变参数需要放在形参列表最后.

	**一个栗子,求多个int类型值的和**

	```go
	func sum(n1 int, args ...int) int {
		sum := n1
		for i := 0; i < len(args); i++ {
			sum += args[0]
		}
		return sum
	}

	func main() {

		//测试可变参数的使用
		res4 := sum(10)
		fmt.Println("res4=", res4)
		res5 := sum(10, 20)
		fmt.Println("res5=", res5)
	}

	```

	输出结果:

	```
	res4= 10
	res5= 30
	```