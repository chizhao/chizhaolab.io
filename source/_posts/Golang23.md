---
title: Go语言编程系列(十八) - 网络编程
date: 2019-04-04 19:09:57
tags:
- Go
- 毕业论文
toc: True
---

### 网络编程基本介绍

#### 网络分层架构

为了减少协议设计的复杂性, 大多数网络模型均采用分层的方式来组织. 每一层都有自己的功能, 就像建筑物一样, 每一层都靠下一层支持. 每一层利用下一层提供的服务来为上一层提供服务, 本层服务的实现细节对上层屏蔽. 越下面的层, 越靠近硬件；越上面的层, 越靠近用户. 

![mark](http://image.i-ll.cc/blog/20190404/sSjG3xDwSCML.png)\

1) 物理层：主要定义物理设备标准, 如网线的接口类型、光纤的接口类型、各种传输介质的传输速率等. 它的主要作用是传输比特流(就是由 1、0 转化为电流强弱来进行传输,  到达目的地后再转化为 1、0, 也就是我们常说的数模转换与模数转换). 这一层的数据叫做比特. 
2) 数据链路层：定义了如何让格式化数据以帧为单位进行传输, 以及如何让控制对物理介质的访问. 这一层通常还提供错误检测和纠正, 以确保数据的可靠传输. 如：串口通信中使用到的 115200、8、N、1
3) 网络层：在位于不同地理位置的网络中的两个主机系统之间提供连接和路径选择. 
Internet 的发展使得从世界各站点访问信息的用户数大大增加, 而网络层正是管理这种连接的层. 
4) 传输层：定义了一些传输数据的协议和端口号(WWW 端口 80 等), 如：TCP(传输控制协议, 传输效率低, 可靠性强, 用于传输可靠性要求高, 数据量大的数据), UDP(用户数据报协议, 与 TCP 特性恰恰相反, 用于传输可靠性要求不高, 数据量小的数据, 如 QQ 聊天数据就是通过这种方式传输的).  主要是将从下层接收的数据进行分段和传输, 到达目的地址后再进行重组. 常常把这一层数据叫做段. 
5) 会话层：通过传输层(端口号：传输端口与接收端口)建立数据传输的通路. 主要在你的系统之间发起会话或者接受会话请求(设备之间需要互相认识可以是 IP 也可以是 MAC 或者是主机名). 
6) 表示层：可确保一个系统的应用层所发送的信息可以被另一个系统的应用层读取. 例如, PC 程序与另一台计算机进行通信, 其中一台计算机使用扩展二一十进制交换码(EBCDIC), 而另一台则使用美国信息交换标准码(ASCII)来表示相同的字符. 如有必要, 表示层会通过使用一种通格式来实现多种数据格式之间的转换. 
7) 应用层：是最靠近用户的 OSI 层. 这一层为用户的应用程序(例如电子邮件、文件传
输和终端仿真)提供网络服务. 

每一层都是为了完成一种功能, 为了实现这些功能, 就需要大家都遵守共同的规则. 大家都遵守这规则, 就叫做“协议”(protocol). 



<!-- more -->

#### 协议

网络的每一层, 都定义了很多协议. 这些协议的总称, 叫“TCP/IP协议”. TCP/IP协议是一个大家族, 不仅仅只有TCP和IP协议, 它还包括其它的协议, 如下图：

![mark](http://image.i-ll.cc/blog/20190404/2AW37kFv56X8.png)\

**每层协议的功能**

![mark](http://image.i-ll.cc/blog/20190404/yvThgk6LOE3s.png)\


1) **链路层**
   
   以太网规定, 连入网络的所有设备, 都必须具有“网卡”接口. 数据包必须是从一块网卡, 传送到另一块网卡. 通过网卡能够使不同的计算机之间连接, 从而完成数据通信等功能. 网卡的地址——MAC  地址, 就是数据包的物理发送地址和物理接收地址. 

2) **网络层**
   
   网络层的作用是引进一套新的地址, 使得我们能够区分不同的计算机是否属于同一个子网络. 这套地址就叫做“网络地址”, 这是我们平时所说的 IP 地址. 这个 IP 地址好比我们的手机号码, 通过手机号码可以得到用户所在的归属地. 


    网络地址帮助我们确定计算机所在的子网络, MAC 地址则将数据包送到该子网络中的目标网卡. 网络层协议包含的主要信息是源 IP 和目的 IP. 

    于是, “网络层”出现以后, 每台计算机有了两种地址, 一种是 MAC 地址, 另一种是网络地址. 两种地址之间没有任何联系, MAC 地址是绑定在网卡上的, 网络地址则是管理员分配的, 它们只是随机组合在一起. 

    网络地址帮助我们确定计算机所在的子网络, MAC 地址则将数据包送到该子网络中的目标网卡. 因此, 从逻辑上可以推断, 必定是先处理网络地址, 然后再处理 MAC  地址. 


3) **传输层**

    当我们一边聊 QQ, 一边聊微信, 当一个数据包从互联网上发来的时候, 我们怎么知道, 它是来自 QQ 的内容, 还是来自微信的内容？

    也就是说, 我们还需要一个参数, 表示这个数据包到底供哪个程序(进程)使用. 这个参数就叫做“端口”(port), 它其实是每一个使用网卡的程序的编号. 每个数据包都发到主机的特定端口, 所以不同的程序就能取到自
    己所需要的数据. 
    端口特点：

    - 对于同一个端口, 在不同系统中对应着不同的进程
    - 对于同一个系统, 一个端口只能被一个进程拥有


4) **应用层**

    应用程序收到“传输层”的数据, 接下来就要进行解读. 由于互联网是开放架构, 数据来源
    五花八门, 必须事先规定好格式, 否则根本无法解读. “应用层”的作用, 就是规定应用程
    序的数据格式. 

    TCP/IP(Transmission Control Protocol/Internet Protocol)的简写,中文译名为传输控制协议/因特网互联协议, 又叫网络通讯协议, 这个协议是 
    Internet 最基本的协议、Internet 国际互联网络的基础, 简单地说, 就是由网络层的 IP 协议和传输层的 TCP 协议组成的. 

#### ip地址

每个internet 上的主机和路由器都有一个ip 地址, 它包括网络号和主机号, ip 地址有ipv4(32位)或者ipv6(128 位). 可以通过ipconfig 来查看

#### 端口(port)介绍

我们这里所指的端口不是指物理意义上的端口, 而是特指 TCP/IP 协议中的端口, 是逻辑意义上的端口. 

1. 只要是做服务程序, 都必须监听一个端口;
2. 该端口就是其他程序和该服务通讯的通道;
3. 一台电脑有65535个端口 1~65535;(在TCP报头那边,端口只有两个字节存储)
4. 一旦一个端口被某个程序监听(占用), 那么其他的程序就不能在该端口监听. 

#### 端口(port)分类


- 0 号是保留端口.
- 1-1024 是固定端口(程序员不要使用)又叫有名端口,即被某些程序固定使用,一般程序员不使用. 22: SSH 远程登录协议 23: telnet 使用 21: ftp 使用25: smtp 服务使用    80: iis 使用 7: echo 服务
- 1025-65535 是动态端口. 这些端口, 程序员可以使用.

#### 端口(port)-使用注意

1) 在计算机(尤其是做服务器)要尽可能的少开端口;
2) 一个端口只能被一个程序监听;
3) 如果使用 netstat –an 可以查看本机有哪些端口在监听;
4) 可以使用 netstat –anb 来查看监听端口的 pid,在结合任务管理器关闭不安全的端口.

### tcp socket 编程的快速入门

**socket**

Socket起源于Unix，而Unix基本哲学之一就是“一切皆文件”，都可以用“打开open –> 读写write/read –> 关闭close”模式来操作。Socket就是该模式的一个实现，网络的Socket数据传输是一种特殊的I/O，Socket也是一种文件描述符。Socket也具有一个类似于打开文件的函数调用：Socket()，该函数返回一个整型的Socket描述符，随后的连接建立、数据传输等操作都是通过该Socket实现的。

常用的Socket类型有两种：流式Socket(SOCK_STREAM)和数据报式Socket (SOCK_DGRAM)。流式是一种面向连接的Socket，针对于面向连接的TCP服务应用；数据报式Socket是一种无连接的Socket，对应于无连接的UDP服务应用。


需要用到[net](https://studygolang.com/static/pkgdoc/pkg/net.htm)包

![mark](http://image.i-ll.cc/blog/20190404/cKKUbSBso9jK.png)\

#### 服务端的处理流程

1) 监听端口 8888;
2) 接收客户端的 tcp 链接，建立客户端和服务器端的链接;
3) 创建 goroutine，处理该链接的请求(通常客户端会通过链接发送请求包).

#### 客户端的处理流程

1) 建立与服务端的链接;
2) 发送请求数据(终端)，接收服务器端返回的结果数据;
3) 关闭链接. 

#### 简单的程序示意图

![mark](http://image.i-ll.cc/blog/20190404/1wIeFzYwwLHz.png)\

#### 代码的实现

**服务端功能**

- 编写一个服务器端程序，在8888 端口监听
- 可以和多个客户端创建链接
- 链接成功后，客户端可以发送数据，服务器端接受数据，并显示在终端上.
- 先使用telnet 来测试，然后编写客户端程序来测试

**客户端功能**

- 编写一个客户端端程序，能链接到服务器端的8888 端口
- 能通过终端输入数据(输入一行发送一行), 并发送给服务器端[]
- 在终端输入exit,表示退出程序.


**server.go**xxx

```go
package main

import (
	"fmt"
	"net"
)

func process(conn net.Conn) {
	//这里循环接受客户端发送的数据
	defer conn.Close() //关闭conn

	for {
		//创建一个新的切片
		buf := make([]byte, 1024)
		//1. 等待客户端通过conn发送信息
		//2. 如果客户端没有write, 那么协程就阻塞在这里
		n, err := conn.Read(buf) //从conn读取
		if err != nil {
			fmt.Println("服务器的Read err=", err)
			return
		}
		//3. 显示客户端发送的内容到服务器的终端
		fmt.Printf("客户端%s发来消息:", conn.RemoteAddr().String())
		fmt.Print(string(buf[:n])) //buf[:n]细节
	}
}

func main() {

	fmt.Println("服务器开始监听....")
	//1. tcp表示使用多个网络协议是tcp
	//2. 0.0.0.0:8888 表示在本地监听8888
	listener, err := net.Listen("tcp", "0.0.0.0:8888")
	if err != nil {
		fmt.Println("listen error ", err)
		return
	}
	defer listener.Close() //延时关闭listener
	//循环等待客户端来连接
	for {
		//等待客户端链接
		conn, err := listener.Accept()
		if err != nil {
			fmt.Println("Accept() err=", err)
		} else {
			fmt.Printf("客户端%v上线了\n", conn.RemoteAddr().String())
		}
		//这里准备一个协程为客户端服务
		go process(conn)
	}
}
```

**client.go**

```go
package main

import (
	"bufio"
	"fmt"
	"net"
	"os"
	"strings"
)

func main() {
	conn, err := net.Dial("tcp", "192.168.50.75:8888")
	if err != nil {
		fmt.Println("client dial err=", err)
		return
	}
	//fmt.Println("connect success conn=", conn)
	//功能1: 客户端可以发送单行数据, 然后就退出
	reader := bufio.NewReader(os.Stdin) //os.Stdin 代表标准输入(终端)
	for {
		//从终端读取一行用户输入,并准备发送给服务器
		line, err := reader.ReadString('\n')
		if err != nil {
			fmt.Println("readString err=", err)
		}
		line = strings.Trim(line, "\r\n")
		//如果用户输入的是exit就退出
		if line == "exit" {
			fmt.Println("客户端退出")
			break
		}

		//再将line发送给服务器
		_, err = conn.Write([]byte(line + "\r\n"))
		if err != nil {
			fmt.Println("conn.Write err=", err)
		}
	}
}
```