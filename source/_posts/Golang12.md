---
title: Go语言编程系列(十) - 函数(三)
date: 2019-03-12 14:50:43
tags:
- Go
- 毕业论文
toc: True
---


### 字符串常用的系统函数


```go
package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {

	/*
		1. 统计字符串的长度,按字节 len(str)
		golang的编码统一为UTF-8,(ASCII的字符(字母和数字)占一个字节, 汉字占用三个字节)
	*/
	str1 := "Hello 北京"
	fmt.Println("1. str1 len = ", len(str1)) //12

	/*
		2.字符串遍历, 同时处理有中文的问题r := []rune(str)
	*/
	str2 := []rune(str1)
	for i := 0; i < len(str2); i++ {
		fmt.Printf("2. str2_i = %c\n", str2[i])
	}
	/*
		3. 字符串转整数: n, err := strconv.Atoi("12")
	*/
	n, err := strconv.Atoi("Hello")
	if err != nil {
		fmt.Println("3. 转换错误") //因为 "Hello" 不是字符串类型的数字, 所以会提示转换错误
	} else {
		fmt.Println("3. 转换的结果是:", n)
	}
	/*
		4. 整数转字符串: str := strconv.ItoA(12345)
	*/
	str4 := strconv.Itoa(12345)
	fmt.Printf("4. str4 %T", str4)
	/*
		5. 字符串转[]byte: var bytes = []bytes("hello go")
	*/
	var bytes = []byte("hellogo")
	fmt.Printf("5. bytes=%v\n", bytes)
	/*
		6. []byte 转 字符串: str := string([]byte{97, 98, 99})
	*/
	str6 := string([]byte{97, 98, 99})
	fmt.Printf("6. str6 = %v", str6)
	/*
		7. 10 进制转 2, 8, 16 进制:  str = strconv.FormatInt(123, 2) // 2-> 8 , 16
	*/
	str7_2 := strconv.FormatInt(16, 2)   //2进制 10000
	str7_8 := strconv.FormatInt(16, 8)   //8进制  20
	str7_16 := strconv.FormatInt(16, 16) //16进制 10
	fmt.Println("7. str7_2", str7_2)
	fmt.Println("7. str7_8", str7_8)
	fmt.Println("7. str7_16", str7_16)
	/*
		8. 查找子串是否在指定的字符串中: strings.Contains("hellogo", "go") //true
	*/
	fmt.Println("8.", strings.Contains("hellogo", "go"))    //true
	fmt.Println("8.", strings.Contains("helloworld", "go")) //false

	/*
		9. 统计一个字符串有几个指定的子串： strings.Count("ceheese", "e") //4
	*/
	fmt.Println("9.", strings.Count("ceheese", "e"))  //4
	fmt.Println("9.", strings.Count("ceheese", "pp")) //0
	/*
		10. 不区分大小写的字符串比较(== 是区分字母大小写的):fmt.Println(strings.EqualFold("abc","Abc")) // true
	*/
	fmt.Println("10.", strings.EqualFold("abc", "Abc")) // true
	fmt.Println("10.", "abc" == "ABC")                  //false

	/*
		11. 返回子串在字符串第一次出现的index值, 如果没有返回-1 : strings.Index("NLT_abc", "abc") // 4
	*/
	fmt.Println("11.", strings.Index("NLT_abc", "abc")) //4
	fmt.Println("11.", strings.Index("NLT_abc", "bcd")) //-1

	/*
		12.返回子串在字符串最后一次出现的index, 如没有返回-1 : strings.LastIndex("go golang", "go")
	*/

	fmt.Println("12.", strings.LastIndex("hello gogogo", "go")) //10
	/*
		13.将指定的子串替换成另外一个子串: strings.Replace("hello gogogo", "go", "go 语言", n) n 可以指定你希望替换几个, 如果n=-1 表示全部替换
	*/

	str13 := strings.Replace("hello gogogo", "go", "Golang", 1)
	fmt.Println("13.", str13) //hello Golanggogo
	str13 = strings.Replace("hello gogogo", "go", "Golang", 2)
	fmt.Println("13.", str13) //hello GolangGolanggo
	str13 = strings.Replace("hello gogogo", "go", "Golang", -1)
	fmt.Println("13.", str13) //hello GolangGolangGolang
	/*
		14. 按照指定的某个字符, 为分割标识,  将一个字符串拆分成字符串数组：strings.Split("hello,wrold", ",")
	*/
	str14 := "Hello,world"
	str14Arr := strings.Split(str14, ",")
	fmt.Printf("14. str14Arr = %v\n", str14Arr) //[Hello world]
	for i := 0; i < len(str14Arr); i++ {
		fmt.Println("14.", str14Arr[i])
	}

	/*
		15. 将字符串的字母进行大小写的转换: strings.ToLower("ABC") // abc   strings.ToUpper("abc") // ABC
	*/

	fmt.Println("15.", strings.ToLower("ABC")) // abc
	fmt.Println("15.", strings.ToUpper("abc")) // ABC
	/*
		16. 将字符串左右两边的空格去掉： strings.TrimSpace("   biubiubiu   ")
	*/
	fmt.Println("16.", strings.TrimSpace("   biubiubiu   ")) // biubiubiu

	/*
		17. 将字符串左右两边指定的字符去掉： strings.Trim("! biubiubiu! ", " !") // ["hello"] //将左右两边!和" "去掉
	*/
	fmt.Println("17.", strings.Trim("! biubiubiu! ", "! ")) // biubiubiu
	/*
		18. 将字符串左边指定的字符去掉： strings.TrimLeft("! hello! ", " !") // ["hello"] //将左边! 和" "去掉
	*/
	fmt.Println("18.", strings.TrimLeft("! biubiubiu! ", "! ")) // biubiubiu!

	/*
		19. 将字符串右边指定的字符去掉： strings.TrimRight("! hello! ", " !") // ["hello"] //将右边! 和" "去掉
	*/
	fmt.Println("19.", strings.TrimRight("! biubiubiu! ", "! ")) // ! biubiubiu
	/*
		20. 判断字符串是否以指定的字符串开头: strings.HasPrefix("http://127.0.0.1", "http") // true
	*/
	fmt.Println("20.", strings.HasPrefix("http://127.0.0.1", "http")) //  true
	/*
		21. 判断字符串是否以指定的字符串结束: strings.HasSuffix("NLT_abc.jpg", "abc") //false
	*/
	fmt.Println("21.", strings.HasSuffix("NLT_abc.jpg", "abc")) //  false

}
```


<!-- more -->

输出结果:

```
1. str1 len =  12
2. str2_i = H
2. str2_i = e
2. str2_i = l
2. str2_i = l
2. str2_i = o
2. str2_i =
2. str2_i = 北
2. str2_i = 京
3. 转换错误
4. str4 string5. bytes=[104 101 108 108 111 103 111]
6. str6 = abc7. str7_2 10000
7. str7_8 20
7. str7_16 10
8. true
8. false
9. 4
9. 0
10. true
10. false
11. 4
11. -1
12. 10
13. hello Golanggogo
13. hello GolangGolanggo
13. hello GolangGolangGolang
14. str14Arr = [Hello world]
14. Hello
14. world
15. abc
15. ABC
16. biubiubiu
17. biubiubiu
18. biubiubiu!
19. ! biubiubiu
20. true
21. false
```


### 时间和日期相关函数

时间和日期相关函数需要引入time包. 

**这里有很多栗子**

```go
package main

import (
	"fmt"
	"time"
)

func main() {

	/*
		1. 看看当前时间
	*/
	now := time.Now()
	fmt.Printf("1. 当前时间%v tpye=%T\n", now, now)
	/*
		2. 获取其他日期信息

		通过now可以获取到年月日 时分秒
	*/
	fmt.Printf("2. 年=%v\n", now.Year())
	fmt.Printf("2. 月=%v\n", now.Month())
	fmt.Printf("2. 月=%v\n", int(now.Month()))
	fmt.Printf("2. 日=%v\n", now.Day())
	fmt.Printf("2. 时=%v\n", now.Hour())
	fmt.Printf("2. 分=%v\n", now.Minute())
	fmt.Printf("2. 秒=%v\n", now.Second())
	/*
		3. 格式化日期和时间
			1. 使用fmt.Printf()或者fmt.Sprintf() 二者的区别是后者会将结果保存成字符串的形式
			2. 使用time.Format() 见以下栗子
	*/
	//格式化时间和日期的方式1
	fmt.Printf("3.1 当前年月日 %d-%d-%d %d:%d:%d \n", now.Year(), (now.Month()), now.Day(),
		now.Hour(), now.Minute(), now.Second())
	dateStr := fmt.Sprintf("3.1 当前年月日 %d-%d-%d %d:%d:%d \n", now.Year(), (now.Month()),
		now.Day(), now.Hour(), now.Minute(), now.Second())
	fmt.Printf("%v", dateStr)
	//格式化时间和日期的方式2
	//数字不能改,固定写法但是这个字符串各个数字可以自由的组合, 这样可以按程序需求来返回时间和日期;
	fmt.Printf(now.Format("2006/01/02 15:04:05"))
	fmt.Println()
	fmt.Printf(now.Format("2006/01/02"))
	fmt.Println()
	fmt.Printf(now.Format("15:04:05"))
	fmt.Println()
	/*
		4. 时间的常量

		const (
			Nanosecond  Duration = 1                  //纳秒
			Microsecond          = 1000 * Nanosecond  // 微 秒
			Millisecond          = 1000 * Microsecond // 毫 秒
			Second               = 1000 * Millisecond // 秒
			Minute               = 60 * Second        // 分 钟
			Hour                 = 60 * Minute        // 小 时
		)

		常量的作用: 在程序中可用于获取指定单位的时间, 比如说 想得到100毫秒 100*time.Millisecond 而不能使用time.Second/10

		如下栗子: 每隔1毫秒 打印一个数字 打印到10就停止
	*/
	i := 0
	for {
		i++
		fmt.Println("4.", i)
		//休眠
		time.Sleep(time.Millisecond)
		if i == 10 {
			break
		}
	}
	/*
		5. time 的 Unix 和 UnixNano 的方法获取当前unix时间戳和unixnano时间戳(作用是可以获取随机数字)

	*/
	fmt.Printf("5. unix时间戳=%v unixnano时间戳=%v\n", now.Unix(), now.UnixNano())
}
```

输出结果:

```
1. 当前时间2019-03-12 17:28:23.7727101 +0800 CST m=+0.008991701 tpye=time.Time
2. 年=2019
2. 月=March
2. 月=3
2. 日=12
2. 时=17
2. 分=28
2. 秒=23
3.1 当前年月日 2019-3-12 17:28:23
3.1 当前年月日 2019-3-12 17:28:23
2019/03/12 17:28:23
2019/03/12
17:28:23
4. 1
4. 2
4. 3
4. 4
4. 5
4. 6
4. 7
4. 8
4. 9
4. 10
5. unix时间戳=1552382903 unixnano时间戳=1552382903772710100
```

**一个最佳实践- 判断test()执行所耗费的时间**

```go
package main

import (
	"fmt"
	"strconv"
	"time"
)

func test() {

	str := ""
	for i := 0; i < 100000; i++ {
		str += "hello" + strconv.Itoa(i)
	}

}

func main() {
	//获取当前unix时间戳
	start := time.Now().Unix()
	test()
	end := time.Now().Unix()
	fmt.Printf("执行test()耗费时间为%v秒", end-start)
}
```

输出结果:

```
执行test()耗费时间为7秒
```

### Golang内置函数

Golang 设计者为了编程方便, 提供了一些函数, 这些函数可以直接使用, 我们称为 Go 的内置函数. 文档：[https://studygolang.com/pkgdoc](https://studygolang.com/pkgdoc) -> builtin, 或者[https://golang.org/pkg/builtin/](https://golang.org/pkg/builtin/)


1) len：用来求长度, 比如 string、array、slice、map、channel;
2) new：用来分配内存, 主要用来分配**值类型**, 比如 int、float32,struct...返回的是指针举例说明 new 的使用：
    ```go
    func main() {
        num1 := 100
        fmt.Printf("num1的类型是%T , num1的值是%v , num1的地址是%v", num1, num1, &num1)
        fmt.Println()
        /*
            num2的类型是 *int
            num2的值是 0xc0000620a0 (系统分配)
            num2的地址是 0xc000088020 (系统分配)
            num2指向的值是0
        */
        num2 := new(int)
        fmt.Printf("num2的类型是%T , num2的值是%v , num2的地址是%v, num2指向的值是%v", num2, num2, &num2, *num2)

        fmt.Println()
        *num2 = 100 //修改num2指向的值
        fmt.Printf("num2的类型是%T , num2的值是%v , num2的地址是%v, num2指向的值是%v", num2, num2, &num2, *num2)
    }
    ```

    输出结果:

    ```go
    num1的类型是int , num1的值是100 , num1的地址是0xc000062058
    num2的类型是*int , num2的值是0xc0000620a0 , num2的地址是0xc000088020, num2指向的值是0
    num2的类型是*int , num2的值是0xc0000620a0 , num2的地址是0xc000088020, num2指向的值是100
    ```
3) make：用来分配内存, 主要用来分配**引用类型**, 比如 channel、map、slice.


### Go错误处理

**一个栗子**

```go
package main

import (
	"fmt"
)

func test() {
	num1 := 10
	num2 := 0
	res := num1 / num2
	fmt.Println(res)
}

func main() {
	test()
	fmt.Println("下面的代码...")
}
```

输出错误:

```
panic: runtime error: integer divide by zero

goroutine 1 [running]:
main.test()
        F:/GoProjects/src/code/chapter06/erro/main.go:10 +0x11
main.main()
        F:/GoProjects/src/code/chapter06/erro/main.go:15 +0x29
exit status 2
```

1. 默认情况下, 当发生错误后(panic), 程序就会终止;
2. 如果我们希望: 当发生错误后, 可以捕获错误, 并进行处理, 保证程序可以继续执行. 还可以在捕获到错误后, 给管理员一个提示;
3. 这里引出错误处理机制. 

**Golang错误处理机制**

1) Go 语言追求简洁优雅, 所以, Go 语言不支持传统的 try…catch…finally 这种处理;
2) Go 中引入的处理方式为：defer, panic, recover;
3) 这几个异常的使用场景可以这么简单描述：Go 中可以抛出一个 panic 的异常, 然后在 defer 中通过 recover 捕获这个异常, 然后正常处理. 

**下面是使用defer+recover对以上错误的处理:**

```go
package main

import (
	"fmt"
)

func test() {
	//使用defer + recover 来捕获和处理异常
	defer func() {
		err := recover() //recover是内置函数,可以捕获到异常
		if err != nil {  //说明捕获到错误
            fmt.Println(err)
            //这里就可以将错误信息发送给管理员
		}
	}()
	num1 := 10
	num2 := 0
	res := num1 / num2
	fmt.Println(res)
}

func main() {
	test()
	fmt.Println("下面的代码...")
}
```

执行结果:

```
runtime error: integer divide by zero
下面的代码...
```

**进行错误处理后, 程序不会轻易挂掉, 如果加入预警代码, 就可以让程序更加的健壮.**

**自定义错误**

Go 程序中, 也支持自定义错误, 使用 errors.New 和 panic 内置函数. 

1) errors.New("错误说明") , 会返回一个 error 类型的值, 表示一个错误;
2) panic 内置函数 ,接收一个 interface{}类型的值（也就是任何值了）作为参数. 可以接收 error 类型的变量, 输出错误信息, 并退出程序.

```go
package main

import (
	"errors"
	"fmt"
)


/*
需求: 读取一个名为config.ini的文件的信息;
如果文件名传入不正确, 返回一个自定义的错误.
*/

func readConf(name string) (err error) {
	if name == "config.ini" {
		return nil
	} else {
		return errors.New("文件名错误,读取失败")
	}
}

func test() {
	err := readConf("config2.ini")
	if err != nil {
		panic(err)
	}
	fmt.Println("test()继续执行")
}

func main() {

	test()
	fmt.Println("main()继续执行")
}
```

输出结果:

```
panic: 文件名错误,读取失败

goroutine 1 [running]:
main.test()
        F:/GoProjects/src/code/chapter06/erro/main.go:38 +0x79
main.main()
        F:/GoProjects/src/code/chapter06/erro/main.go:45 +0x29
exit status 2
```

