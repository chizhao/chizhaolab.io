---
title: Go语言编程系列(四) - 基本数据类型之间的转换
date: 2019-03-05 18:57:41
tags:
- Go
- 毕业论文
toc: True
---


### Golang数值类型的转换

Golang中的数据类型不能自动转换, Go语言中强制要求显示转换. 转换方法如下

表达式 T(v) 将值 v 转换为类型 T.

一些关于数值的转换：

```go
//Golang中数值的转换
var x, y int = 3, 4
var f float64 = math.Sqrt(float64(x*x + y*y))
var z uint = uint(f)
fmt.Println(x, y, z)
var i int32 = 100
var j float32 = float32(i)
fmt.Printf("x,y,z,f,i,i的类型分别为 %T,%T,%T,%T,%T,%T", x, y, z, f, i, j)
```

输出结果:
```
3 4 5
x,y,z,f,i,i的类型分别为 int,int,uint,float64,int32,float32
```

<!-- more -->

更简单的写法

```go
i := 42
f := float64(i)
u := uint(f)
fmt.Printf("i,f,u的数据类型分别为%T,%T,%T", i, f, u)
```

输出结果:

>i,f,u的数据类型分别为int,float64,uint

**细节问题:**

1. 表达式 T(v) 将值 v 转换为类型 T, v的类型不变;
2. Go中, 数据类型的转换可以从精度高的类型转到精度低的类型, 反之亦然;
3. 如果精度低的类型不能够表示精度高类型的值, 计算机不会报错, 结果按照溢出处理.
    ```go
    var n1 int64 = 9999999
    var n2 int8 = int8(n1)
    fmt.Println("n2=", n2)
    ```
    输出结果:

    >n2= 127
4. 类型不同的变量不能运算. 例如
    ```go
    var n1 int64 = 20
	var n2 int8
	n2 = n1 + 64
	fmt.Println("n2=", n2)
    ```

    ![mark](http://image.i-ll.cc/blog/20190305/j49azoqyvH0N.png)\
5. 在声明一个变量而不指定其类型时(即使用不带类型的 := 语法或 var = 表达式语法)，变量的类型由右值推导得出. 当右值声明了类型时, 新变量的类型与其相同. 如下
    ```go
    var i int
    j := i // j 也是一个 int
    ```
    输出结果:

    >j的类型是int

### Golang数值类型和String类型的转换

在程序开发中, 经常要将基本数据类型转成string, 或者将string转成基本数据类型. 


#### 数值类型转String类型 

**第一种方法: 使用fmt.Sprintf()方法:**

通过查阅Go官方文档得到:

![mark](http://image.i-ll.cc/blog/20190305/MhpCrP0olwv2.png)\

栗子如下:

```go

	var num1 int = 99
	var num2 float64 = 23.456
	var b bool = true
	var myChar byte = 'h'
	var str string //空的str

	/*
	使用fmt.Sprintf()方法
	其中%q表示该值对应的单引号括起来的go语法字符字面值，
	必要时会采用安全的转义表示
	*/

	str = fmt.Sprintf("%d", num1)
	fmt.Printf("str type is %T str =%q\n", str, str)

	str = fmt.Sprintf("%f", num2)
	fmt.Printf("str type is %T str =%q\n", str, str)

	str = fmt.Sprintf("%t", b)
	fmt.Printf("str type is %T str =%q\n", str, str)

	str = fmt.Sprintf("%c", myChar)
    fmt.Printf("str type is %T str =%q\n", str, str)
```
输出结果:
```
str type is string str ="99"
str type is string str ="23.456000"
str type is string str ="true"
str type is string str ="h"
```

**第二种方法: 使用strconv包的函数**

同样也是查阅Go官方文档, 主要使用以下四种函数

![mark](http://image.i-ll.cc/blog/20190305/hPIeK7fHgP3U.png)\

![mark](http://image.i-ll.cc/blog/20190305/ubdblz8zVG0V.png)\

![mark](http://image.i-ll.cc/blog/20190305/OcWNhXlfML5D.png)\

![mark](http://image.i-ll.cc/blog/20190305/4hmOFLyd21jF.png)\

![mark](http://image.i-ll.cc/blog/20190305/RYorkNiegVTz.png)\

除此之外还有一个strconv.Itoa函数:

![mark](http://image.i-ll.cc/blog/20190305/crwgGu7YsJsC.png)\

注意这个函数的参数是int类型.

```go
//第二种方式 使用strconv包种的函数
var num3 int = 99
var num4 float64 = 23.456
var b2 bool = true
str = strconv.FormatInt(int64(num3), 10)
fmt.Printf("str type is %T str =%q\n", str, str)
// 'f'代表一种格式, 10表示小数位保留10位, 64表示这个小数是float64
str = strconv.FormatFloat(num4, 'f', 10, 64)
fmt.Printf("str type is %T str =%q\n", str, str)
str = strconv.FormatBool(b2)
fmt.Printf("str type is %T str =%q\n", str, str)
```
输出结果:
```
str type is string str ="99"
str type is string str ="23.4560000000"
str type is string str ="true"
```

#### String类型转数值类型

同样用strconv包中的函数:

![mark](http://image.i-ll.cc/blog/20190305/Y0amIQupWoRN.png)\

```go

var str string = "true"
var b bool
// strconv.ParseBool(str)函数会返回两个值,(value bool , err error)
// 只要第一个值, 所以使用_忽略第二个值
b, _ = strconv.ParseBool(str)
fmt.Printf("b type %T b=%v\n", b, b)

var str2 string = "12345690"
var n1 int64
var n2 int
n1, _ = strconv.ParseInt(str2, 10, 64)
fmt.Printf("n1 type %T n1=%v\n", n1, n1)
n2 = int(n1) 
fmt.Printf("n2 type %T n2=%v\n", n2, n2)

var str3 string = "123.456"
var f1 float64
f1, _ = strconv.ParseFloat(str3, 64)
fmt.Printf("f1 type %T f1=%v\n", f1, f1)
```

输出结果:
```
b type bool b=true
n1 type int64 n1=12345690
n2 type int n2=12345690
f1 type float64 f1=123.456
```

**String类型转数值类型的注意事项**

在将String类型转成基本数据类型时, 要确保String类型能够转成有效的数据, 比如我们可以把"123" , 转成一个整数, 但是不能把"hello" 转成一个整数, 如果这样做, Golang直接将其转成0. 同理可以把"True"或"False"转成bool型, 但是不能把"hello"转成bool型, 如果这样做, Golang直接将其转成"False",其它类型也是一样的.
```go
var str4 string = "hello"
var n3 int64
n3, _ = strconv.ParseInt(str4, 10, 64)
fmt.Printf("n3 type %T n3=%v\n", n3, n3)

var str string = "hello"
var b bool = true
b, _ = strconv.ParseBool(str)
fmt.Printf("b type %T b=%v\n", b, b)
```

输出结果:
```
n3 type int64 n3=0
b type bool b=false
```