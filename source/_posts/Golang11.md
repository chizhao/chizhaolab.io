---
title: Go语言编程系列(十) - 函数(二)
date: 2019-03-12 10:40:26
tags: 
- Go
- 毕业论文
toc: True
---


### init函数

每一个源文件都可以包含一个init函数, 该函数会在main函数执行前, 被Go运行框架调用, 也就是说init会在main函数前被调用. 通常可以在init函数中完成程序初始化工作. 



**init函数使用注意事项和细节**

1. 如果一个文件中同时包含**全局变量定义, init函数和main函数**, 则执行的流程顺序是 全局变量定义-> init函数 -> main函数;
2. init函数的作用主要是完成一些初始化的工作;
3. 如果 main.go和utils.go都含有 变量定义, init 函数时, 执行的流程又是怎么样的呢？
    1. utils.go中的变量定义;
    2. utils.go中的init函数;
    3. main.go中的变量定义;
    4. main.go中的init函数;
    5. main.go中的main函数.
<!-- more -->

### 匿名函数

匿名函数是指没有定义名字符号的函数.

除没有名字外, 匿名函数和普通函数完全相同. 最大区别是, 我们可在函数内部定义匿名函数, 形成类似嵌套效果. 匿名函数可直接调用, 保存到变量, 作为参数或返回值. 

**匿名函数的使用方式1 - 直接执行**

```go
/*
    在定义匿名函数时就直接调用,这种匿名函数只能调用一次;
*/

//案例演示: 求两个数的和, 使用匿名函数的方式完成

res1 := func(n1 int, n2 int) int {
    return n1 + n2
}(10, 20)
fmt.Println("res1=", res1)

//案例演示: 打印出Hello Golang
func(s string) {
    println(s)
}("Hello Golang!")
```


**匿名函数的使用方式2 - 赋值给变量**

```go
/*
    以上两个栗子的第二种方式
*/
add := func(x, y int) int {
    return x + y
}

fmt.Println(add(10, 20))

str := func(s string) {
    fmt.Println(s)
}
str("Hello Golang!")
```

**全局匿名函数**

```go
var (
    //func1就是一个全局匿名函数
	func1 = func(n1 int, n2 int) int {
		return n1 * n2
	}
)
func main(){
    //全局匿名函数的使用
    res2 := func1(10, 20)
	fmt.Println("res2=", res2)
}
```

### 闭包

Go 函数可以是一个闭包. 闭包是一个函数值, 它引用了其函数体之外的变量. 该函数可以访问并赋予其引用的变量的值, 换句话说, 该函数被“绑定”在了这些变量上. 

闭包就是**一个函数**和**与其相关的引用环境**组合的一个整体(实体). 以下是Go指南的一个栗子. 函数 adder 返回一个闭包. 每个闭包都被绑定在其各自的 sum 变量上. 

```go
package main

import "fmt"

func adder() func(int) int {
	sum := 0
	return func(x int) int {
		sum += x
		return sum
	}
}

func main() {
	pos, neg := adder(), adder()
	for i := 0; i < 10; i++ {
		fmt.Println(
			pos(i),
			neg(-2*i),
		)
	}
}
```

以下是对以上代码的解释:

1. adder是一个函数, 返回的数据类型是func(int) int;
2. adder返回的是一个匿名函数, 这个匿名函数引用到了函数外的变量sum, 因此这个匿名函数就和sum形成一个整体. 构成闭包;
3. 可以这样理解: 闭包是类, 函数是操作, sum是字段,函数和它使用到的sum构成闭包;
4. 当我们反复调用pos()和neg()函数时, sum只初始化一次, 因此每调用一次就进行一次累计;
5. 搞清闭包的关键就是要分析出返回的函数,具体引用了哪些变量, 因为函数和它所引用到的变量共同构成闭包.
6. 函数 adder 返回一个闭包, 每个闭包都被绑定在其各自的 sum 变量上.

以下是上述例子输出:

```
0 0
1 -2
3 -6
6 -12
10 -20
15 -30
21 -42
28 -56
36 -72
45 -90
```


**Go指南的一个练习: 斐波那契闭包**

```go
package main

import "fmt"

/*
	让我们用函数做些好玩的事情. 

	实现一个 fibonacci 函数, 它返回一个函数（闭包）, 该闭包返回一个斐波纳契数列 `(0, 1, 1, 2, 3, 5, ...)`. 

*/

func fibonacci() func() int {

	a, b := 1, 0
	return func() int {
		a, b = b, a+b
		return b
	}

}

func main() {
	f := fibonacci()
	for i := 0; i < 10; i++ {
		fmt.Println(f())
	}
}
```

输出结果:

```
1
1
2
3
5
8
13
21
34
55
```

**闭包的最佳实践**

用到以下函数:

![mark](http://image.i-ll.cc/blog/20190312/93KQ0DrEdxnA.png)\

```go
/*

请编写一个程序, 具体要求如下

1) 编写一个函数 makeSuffix(suffix string)   可以接收一个文件后缀名(比如.jpg), 并返回一个闭包
2)  调用闭包, 可以传入一个文件名, 如果该文件名没有指定的后缀(比如.jpg) ,则返回 文件名.jpg ,  如果已经有.jpg 后缀, 则返回原文件名. 
3) 要求使用闭包的方式完成
4) strings.HasSuffix , 该函数可以判断某个字符串是否有指定的后缀. 

*/
func makeSuffix(suffix string) func(string) string {

	return func(name string) string {
		//如果name 没有指定后缀, 则加上,否则就返回原来的名字
		if strings.HasSuffix(name, suffix) {
			return name
		} else {
			return name + suffix
		}
	}
}
func main() {

	//测试makeSuffix的使用

	str1 := "myphoto.jpg"
	str2 := "herphoto"

	f := makeSuffix(".jpg")
	fmt.Println("文件名处理后=", f(str1)) //myphoto.jpg
	fmt.Println("文件名处理后=", f(str2)) //herphoto.jpg

}

```

对以上代码的说明:

1. 返回的匿名函数和makeSuffix(suffix string)的suffix变量组成一个闭包, 因为返回的函数引用了suffix变量;
2. 如果使用传统的方法, 也可以轻松实现这个功能, 但是传统方法需要每次都传入 后缀名, 比如 .jpg ,而闭包因为可以保留上次引用的某个值, 所以我们传入一次就可以反复使用.


### defer(延时机制)


在函数中, 程序员经常需要创建资源(比如: 数据库连接、文件句柄、锁等) , 为了在函数执行完毕后, 及时的释放资源, Go 的设计者提供 defer (延时机制). 

defer 语句会将函数推迟到外层函数返回之后执行. 

推迟调用的函数其参数会立即求值, 但直到外层函数返回前该函数都不会被调用.

推迟的函数调用会被压入一个栈中. 当外层函数返回时, 被推迟的函数会按照后进先出的顺序调用. 

**一个栗子**

```go
package main

import "fmt"

func main() {
	fmt.Println("counting")

	for i := 0; i < 10; i++ {
		defer fmt.Println(i)
	}

	fmt.Println("done")
}
```

输出结果:

```
counting
done
9
8
7
6
5
4
3
2
1
0
```

**另外一个栗子:**

```go
package main

import "fmt"

func sum(n1 int, n2 int) int {

	//当执行到defer时, 暂时不执行, 会将defer后面的语句压入到独立的栈(defer栈)
	//当函数执行完毕后, 再从defe栈, 按照先进后出的方式出栈 执行
	defer fmt.Println("n1=", n1) //n1入栈
	defer fmt.Println("n2=", n2) //n2入栈

	res := n1 + n2

	fmt.Println("res=", res)
	return res

}

func main() {
	fmt.Println("counting")

	sum(20, 60)

	fmt.Println("done")
}
```
输出结果:

```
counting
res= 80
n2= 60
n1= 20
done
```

**defer的注意事项和细节**


1) 当 go 执行到一个 defer 时, 不会立即执行 defer 后的语句, 而是将 defer 后的语句压入到一个栈中[我为了讲课方便, 暂时称该栈为 defer 栈], 然后继续执行函数下一个语句. 
2) 当函数执行完毕后, 在从 defer 栈中, 依次从栈顶取出语句执行(注: 遵守栈 先入后出的机制);

3) 在 defer 将语句放入到栈时, 也会将相关的值拷贝同时入栈,对上面的代码稍作修改:
    ```go
    package main

    import "fmt"

    func sum(n1 int, n2 int) int {

        //当执行到defer时, 暂时不执行, 会将defer后面的语句压入到独立的栈(defer栈)
        //当函数执行完毕后, 再从defe栈, 按照先进后出的方式出栈 执行
        defer fmt.Println("n1=", n1) //n1入栈
        defer fmt.Println("n2=", n2) //n2入栈

        //增加两句话
        n1++
        n2++

        res := n1 + n2

        fmt.Println("res=", res)
        return res

    }

    func main() {
        fmt.Println("counting")

        sum(20, 60)

        fmt.Println("done")
    }
    ```

    输出结果:

    ```
    counting
    res= 82
    n2= 60
    n1= 20
    done
    ```

**defer的最佳实践**

defer 最主要的价值是在, 当函数执行完毕后, 可以及时的释放函数创建的资源..

1) 在 golang 编程中的通常做法是, 创建资源后, 比如(打开了文件, 获取了数据库的链接, 或者是锁资源),  可以执行 defer file.Close() defer connect.Close();
2) 在 defer 后, 可以继续使用创建资源;
3) 当函数完毕后, 系统会依次从 defer 栈中, 取出语句, 关闭资源;
4) 这种机制, 非常简洁, 程序员不用再为在什么时机关闭资源而烦心.


### 函数参数的传递方式

**两种传递方式**

在Golang中函数参数的传递方式有两种:

    1. 值传递
    2. 引用传递

不管是值传递还是引用传递, 传递给函数的都是变量的副本, 不同的是, 值传递的是值的拷贝, 引用传递的是地址的拷贝, 一般来说, 地址拷贝效率高, 因为数据量小, 而值拷贝决定拷贝的数据大小, 数据越大, 效率越低. 


**值类型和引用类型**


1) 值类型: 基本数据类型 int 系列, float 系列, bool, string 、数组和结构体 struct. **值类型默认是值传递**: 变量直接存储值, 内存通常在栈中分配;
2) 引用类型: 指针、slice 切片、map、管道 chan、interface 等都是引用类型. **引用类型默认是引用传递**, 变量存储的是一个地址, 这个地址对应的空间才真正存储数据(值), 内存通常在堆上分配. 当没有任何变量引用这个地址时, 该地址对应的数据空间就会变成一个垃圾, 由GC回收.
3) 如果希望函数内的变量能修改函数外的变量(指的是默认以值传递的方式的数据类型), 可以传入变量的地址&, 函数内以指针的方式操作变量.从效果上看类似引用. 
    ```go
    func test(n1 *int) {
        *n1 += 10
        fmt.Println("n1 test() = ", *n1)
    }

    func main() {

        n1 := 6
        test(&n1)
        fmt.Println("n1 main() = ", n1)

    }
    ```

    输出结果:

    ```
    n1 test() =  16
    n1 main() =  16
    ```

### 变量作用域

1. **局部变量:** 函数内部定义/声明的变量, 作用域仅限函数内部;
2. **全局变量:** 函数外部定义/声明的变量, 作用域在整个包都有效, 如果其首字母大写, 则作用域在整个程序都有效;
3. 如果变量是在一个代码块, 比如 for / if 中, 那么这个变量的的作用域就在该代码块;
4. 函数外部不能使用赋值语句. 

