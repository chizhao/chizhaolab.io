---
title: Go语言编程系列(六) - 值类型和引用类型, 标识符
date: 2019-03-08 23:22:30
tags:
- Go
- 毕业论文
toc: True
---


### 值类型和引用类型

1. 值类型: 基本数据类型 int 系列, float 系列, bool, string, 数组和结构体struct;
2. 引用类型: 指针、slice 切片、map、管道 chan、interface 等都是引用类型.

**使用特点:**

1. 值类型: 变量直接存储值, 变量通常在栈中分配;
2. 引用类型: 变量存储的是一个地址, 这个地址对应的空间存储数据. 内存通常在**堆**上分配, 当没有任何变量引用这个地址时, 该地址对应的数据空间就会成为一个垃圾, 由**GC**来回收.


<!-- more -->

### 标识符


#### 标识符定义

1. Golang 对各种变量、方法、函数等命名时使用的字符序列称为标识符;
2. 凡是自己可以起名字的地方都叫标识符. 


#### 标识符的命名规则

1) 由 26 个英文字母大小写, 0-9 , _ 组成; 
2) 数字不可以开头; 
    ```go
    var numint//ok 
    var 3numint//error 
    ```
3) Golang 中严格区分大小写, 在 golang 中, num 和 Num 是两个不同的变量;
    ```go
    var numint 
    var Numint 
    ```
4) 标识符不能包含空格;
5) 下划线"_"本身在 Go 中是一个特殊的标识符, 称为空标识符. 可以代表任何其它的标识符, 但是它对应的值会被忽略(比如：忽略某个返回值). 所以**仅能被作为占位符使用, 不能作为标识符使用**;
6) 自己起的名字不能与**关键字**重名. 不推荐与**预定义标识符**重名.

**关键字**

Go语言仅25个保留关键字(keyword)保留关键字不能用作常量、变量、函数名, 以及结构字段等标识符.

```
break   default    func   interface select
case    defer      go    map   struct
chan    else   goto   package    switch
const   fallthrough if    range      type
continue for   import     return       var
```

**预定义标识符**

除了关键字, 还有大约 30 多个预定义标识符, 比如 int 和 true 等, 主要对应内建的常量、类型和函数

```
内建常量:   
    true false iota nil 
内建类型:
    int int8 int16 int32 int64
    uint uint8 uint16 uint32 uint64 uintptr
    float32 float64 complex128 complex64 
    bool byte rune string error 
内建函数:
    make len cap new append copy close delete
    complex real imag      
    panic recover
```


#### 标识符命名注意事项


1. 包名: 保持 package 的名字和目录保持一致, 尽量采取有意义的包名, 简短, 有意义, 不要和 标准库不要冲突 fmt;
2. 变量名、函数名、常量名: 采用驼峰法, 栗子如下:
    ```go
    var stuName string = "Tom"
    var goodPrice float = 123.4
    ```

3. 如果变量名、函数名、常量名**首字母大写**, 则可以被其他的包访问；如果首字母小写, 则只能在本包中使用 ( 注: 可以简单的理解成, 首字母大写是公开的, 首字母小写是私有的),在 golang 没有 public,private 等关键字. 