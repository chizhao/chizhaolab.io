---
title: Go语言编程系列(十六) - Go并发编程
date: 2019-04-02 18:12:58
tags:
- Go
- 毕业论文
toc: True
---

### 并发与并行

在此之前, 先说一下**进程和线程**.

**进程:** 是指计算机中已运行的程序. 进程为曾经是分时系统的基本运作单位. 在面向进程设计的系统中, 进程是程序的基本执行实体；在面向线程设计的系统中, 进程本身不是基本运行单位, 而是线程的容器. 程序本身只是指令、数据及其组织形式的描述, 进程才是程序的真正运行实例

**线程:** 线程是操作系统能够进行运算调度的最小单位. 它被包含在进程之中, 是进程中的实际运作单位. 一条线程指的是进程中一个单一顺序的控制流, 一个进程中可以并发多个线程, 每条线程并行执行不同的任务. 

一个进程包含了所有可能分配的常用资源. 这些资源包括但不限于内存地址空间、文件和设备的句柄以及线程. 一个进程可以创建和销毁多个线程, 同一个进程中的多个线程可以并发执行. 线程是程序的实际执行者,一个线程是一个执行空间, 这个空间会被操作系统调度来运行函数中所写的代码. 每个进程至少包含一个线程, **每个进程的初始线程被称作主线程. 因为执行这个线程的空间是应用程序的本身的空间, 所以当主线程终止时, 应用程序也会终止.**
<!-- more -->


**并发(concurrency):** 指单个CPU上同一时刻只能有一条指令执行, 但多个进程指令被快速的轮换执行, 使得在宏观上具有多个进程同时执行的效果, 但在微观上并不是同时执行的, 只是把时间分成若干段, 使多个进程快速交替的执行. 

**并行(parallel):** 指在同一时刻, 有多条指令在多个处理器上同时执行. 

并行是让不同的代码片段同时在不同的物理处理器上执行. 并行的关键是同时做很多事情, 而并发是指同时管理很多事情. 


- 并行是两个队列同时使用两台咖啡机
- 并发是两个队列交替使用一台咖啡机

![mark](http://image.i-ll.cc/blog/20190402/Sw7fWz99N15u.png)\

### Goroutine(协程)和Go主线程

Go 主线程(有程序员直接称为线程/也可以理解成进程): 一个 Go 线程上, 可以起多个协程, 可以这样理解, 协程是轻量级的线程. 

本质上, goroutine 就是协程(coroutine).  不同的是, Golang 在 runtime、系统调用等多方面对 goroutine 调度进行了封装和处理, 当遇到长时间执行或者进行系统调用时, 会主动把当前 goroutine 的CPU (P) 转让出去, 让其他 goroutine 能被调度并执行, 也就是 Golang 从语言层面支持了协程. 如下图所示: 

![mark](http://image.i-ll.cc/blog/20190403/Dn3O7gmAvXlR.png)\


一个 goroutine 可能体现在底层就是五六个线程, Go 语言内部帮你实现了这些 goroutine 之间的内存共享. 执行 goroutine 只需极少的栈内存(大概是 4~5KB), 当然会根据相应的数据伸缩. 也正因为如此, 可同时运行成千上万个并发任务. goroutine 比 thread 更易用、更高效、更轻便. 

Goroutine的特点:

1) 有独立的栈空间
2) 共享程序堆空间
3) 调度由用户控制
4) 协程是轻量级的线程

协程的特点在于是一个线程执行, 那和多线程比, 协程有以下优势:

1. 协程没有线程切换的开销, 和多线程比, 线程数量越多, 协程的性能优势就越明显. 
2. 不需要多线程的锁机制, 因为只有一个线程, 也不存在同时写变量冲突, 在协程中控制共享资源不加锁, 只需要判断状态就好了, 所以执行效率比多线程高很多. 
3. 因为协程是一个线程执行, 那怎么利用多核CPU呢？最简单的方法是多进程+协程, 既充分利用多核, 又充分发挥协程的高效率, 可获得极高的性能. 

### Goroutine 快速入门

```go
package main

import (
	"fmt"
	"strconv"
	"time"
)

/*
1) 在主线程(可以理解成进程)中, 开启一个 goroutine, 该协程每隔 1 秒输出 "hello,world"
2) 在主线程中也每隔一秒输出"hello,golang", 输出 5 次后, 退出程序
3) 要求主线程和 goroutine 同时执行.
*/

func test() {
	for i := 0; i < 5; i++ {
		fmt.Println("test hello,golong" + strconv.Itoa(i))
		time.Sleep(time.Second)
	}
}

func main() {
	//test() //这种写法是 执行完test()函数之后执行main函数
	go test() //开启一个协程
	for i := 0; i < 5; i++ {
		fmt.Println("main hello,golong" + strconv.Itoa(i))
		time.Sleep(time.Second)
	}
}
```

输出结果:

```
main hello,golong0
test hello,golong0
test hello,golong1
main hello,golong1
main hello,golong2
test hello,golong2
main hello,golong3
test hello,golong3
main hello,golong4
test hello,golong4
```

可以看到主线程和goroutine是同时执行的. 

如果我们将main函数里面的for循环 5改为2 , 输出就会改变成以下内容:

```
main hello,golong0
test hello,golong0
main hello,golong1
test hello,golong1
```

原因在于:

1. 程序的主线程退出了, 所以协程即使没有执行完毕也要退出.
2. 当然协程也可以在主线程没有执行完毕的情况下完成自己的任务自己退出. 

**快速入门小结**


1) 主线程是一个物理线程, 直接作用在 cpu 上的. 是重量级的, 非常耗费 cpu 资源. 
2) 协程从主线程开启的, 是轻量级的线程, 是逻辑态. 对资源消耗相对小. 
3) Golang 的协程机制是重要的特点, 可以轻松的开启上万个协程. 其它编程语言的并发机制是一般基于线程的, 开启过多的线程, 资源耗费大, 这里就突显 Golang 在并发上的优势了

### Goroutine 的调度模型 - MPG模式基本介绍


1. M: 操作系统的主线程(是物理线程);
2. P: 协程执行需要的上下文(需要的资源);
3. G: 协程.

**并行并发的MPG状态**

![mark](http://image.i-ll.cc/blog/20190403/2oPXWbBgQzps.png)\

**阻塞状态详解**

![mark](http://image.i-ll.cc/blog/20190403/Dn3O7gmAvXlR.png)\

1. 原来的线程M2正在执行G4协程, 另外3个协程在队列等待;
2. 如果G4出现阻塞, 如读取文件或者数据库等;
3. 这时候调度器就会将这个线程与主线程分离, 并创建一个新线程M3(也可能是从已有的线程池种取出M3), 并将其绑定到该逻辑处理器上. 将等待的三个协程挂到M3下开始执行. M2下的G4依然做之前的工作;
4. 这样的MPG调度模式, 可以既让G4执行, 同时也不会让队列的其他协程一直阻塞, 仍然可以并发/并行执行.
5. 一旦被阻塞的系统调用执行完成并返回, 对应的goroutine会放回到本地运行队列, 而之前的线程会保存好, 以便之后可以继续使用. 

### 设置Golang 运行的cpu数

需要用到包[runtime](https://studygolang.com/static/pkgdoc/pkg/runtime.htm)

![mark](http://image.i-ll.cc/blog/20190403/mKTlHJXdAzbx.png)\

```
package main

import (
	"fmt"
	"runtime"
)

func main() {
	cpuNum := runtime.NumCPU()
	fmt.Println("cpuNum=", cpuNum)

	//自己设置运行的CPU数目
	runtime.GOMAXPROCS(cpuNum - 1)

}
```

1. Go1.8后程序默认运行在多个核上, 不用自己设置;
2. Go1.8以前需要自己设置. 


### 协程并发(并行)资源竞争问题

如果两个或者多个goroutine在没有互相同步的情况下, 访问某个共享的资源, 并试图同时读和写这个资源, 就处于相互竞争的状态, 这种情况被称作竞争状态（race candition）. 竞争状态的存在是让并发程序变得复杂的地方, 十分容易引起潜在问题. 对一个共享资源的读和写操作必须是原子化的, 换句话说, 同一时刻只能有一个goroutine对共享资源进行读和写操作. 

以下是一个栗子: 程序中出现了资源竞争的问题

```go
package main

import (
	"fmt"
	"time"
)

// 需求: 现在要计算 1-200  的各个数的阶乘, 并且把各个数的阶乘放入到 map 中. 
// 最后显示出来. 要求使用 goroutine 完成

// 思路
// 1. 编写一个函数, 来计算各个数的阶乘, 并放入到 map 中.
// 2. 我们启动的协程多个, 统计的将结果放入到 map 中
// 3. map 应该做出一个全局的.
var (
	myMap = make(map[int]int, 200)
)

// test 函数就是计算 n!, 让将这个结果放入到 myMap
func test(n int) {
	res := 1
	for i := 1; i <= n; i++ {
		res *= i
	}
	//这里我们将 res 放入到 myMap
	myMap[n] = res //concurrent map writes?
}
func main() {

	// 我们这里开启多个协程完成这个任务[200 个]
	for i := 1; i <= 200; i++ {
		go test(i)
	}

	//休眠 10 秒钟 否则主线程结束的比goroutine快, 这也是goroutine的第二个问题
	time.Sleep(time.Second * 10)

	//这里我们输出结果,变量这个结果
	for i, v := range myMap {
		fmt.Printf("map[%d]=%d\n", i, v)
	}
}
```

```sh
$ go run ./main.go
fatal error: concurrent map writes
//省略很多行
$ go build -race　 // 用竞争检测器标志来编译程序
$./main.exe
Found 2 data race(s)
```

上面的案例就是一个资源竞争的问题, 这就引出了另外一个问题**不同goroutine 之间如何通讯?**

1. 锁住共享资源;
2. 使用管道channel来解决.

### 使用全局变量加锁同步改进程序

因为没有对全局变量m 加锁, 因此会出现资源争夺问题, 代码会出现错误, 提示concurrent map writes. 需要用到包[sync](https://studygolang.com/static/pkgdoc/pkg/sync.htm).

对以上代码做以下更改:

```go
var (
	myMap = make(map[int]int, 10)
	//声明一个全局的互斥锁
	//lock是一个全局互斥锁
	lock sync.Mutex //Mutex是一个互斥锁, 可以创建为其他结构体的字段；零值为解锁状态. Mutex类型的锁和线程无关, 可以由不同的线程加锁和解锁. 

)

// test 函数就是计算 n!, 让将这个结果放入到 myMap
func test(n int) {
	res := 1
	for i := 1; i <= n; i++ {
		res *= i
	}
	//这里我们将 res 放入到 myMap
	//加锁
	lock.Lock()
	myMap[n] = res //concurrent map writes?
	lock.Unlock()
}

func main() {

	// 我们这里开启多个协程完成这个任务[200 个]
	for i := 1; i <= 200; i++ {
		go test(i)
	}

	//休眠 10 秒钟 否则主线程结束的比goroutine快, 这也是goroutine的第二个问题
	time.Sleep(time.Second * 3)
	lock.Lock()
	//这里我们输出结果,变量这个结果
	for i, v := range myMap {
		fmt.Printf("map[%d]=%d\n", i, v)
	}
	lock.Unlock()
}
```

**对以上代码的说明:**

main()函数中 for循坏部分加互斥锁的原因是: 按理说10秒钟上门的协程都应该执行完, 后面就不应该出现资源竞争问题了, 但是实际运行过程中还是出现(通过 -race参数, 确实发现存在资源竞争问题), 因为主线程并不知道10秒钟执行完了所有协程, 因此底层可能仍然出现资源争夺, 因此加入互斥锁即可解决问题. 

### channel(管道)

#### 什么需要channel(管道)?

1) 前面使用全局变量加锁同步来解决 goroutine 的通讯, 但不完美
2) 主线程在等待所有 goroutine 全部完成的时间很难确定, 我们这里设置 10 秒, 仅仅是估算. 
3) 如果主线程休眠时间长了, 会加长等待时间, 如果等待时间短了, 可能还有 goroutine 处于工作状态, 这时也会随主线程的退出而销毁
4) 通过全局变量加锁同步来实现通讯, 也并不利用多个协程对全局变量的读写操作. 
5) 上面种种分析都在呼唤一个新的通讯机制-channel

#### channel 的基本介绍

1) channle 本质就是一个数据结构-队列;
2) 数据是先进先出【FIFO : first in first out】
3) 线程安全, 多 goroutine 访问时, 不需要加锁, 就是说 channel 本身就是线程安全的
4) channel 有类型的, 一个 string 的 channel 只能存放 string 类型数据. 
5) channel通过发送和接收需要共享的资源, 在goroutine之间做同步. 当一个资源需要在goroutine之间共享时, 通道在goroutine之间架起了一个管道, 并提供了确保同步交换数据的机制. 

#### channel 定义/声明

```go
var 变量名 chan 数据类型
```

**举例说明**

```go
var intChan chan int //(intChan 用于存放 int 数据)
var mapChan chan map[int]string //(mapChan 用于存放 map[int]string 类型) 
var perChan chan Person
var perChan2 chan *Person
```

**说明:**

1. channel 是引用类型
2. channel 必须初始化才能写入数据, 即 make 后才能使用管道是有类型的, intChan 只能写入整数 int

#### 管道的初始化, 写入数据到管道, 从管道读取数据及基本的注意事项

```go
package main

import (
	"fmt"
)

func main() {
	//演示一下管道的使用
	//1. 创建一个可以存放3个int类型的管道
	var intChan chan int
	intChan = make(chan int, 3)

	//2. 看看intChan是什么
	fmt.Printf("intChan type is %T, value is %v, address is %v\n", intChan, intChan, &intChan)
	//intChan type is chan int value is 0xc000082080 address is 0xc000006028

	//3. 向管道写入数据, 不能超过其容量
	intChan <- 10
	num := 211
	intChan <- num

	//4. 看看管道的长度和cap(容量) map容量可以自动增长, 但是channel不可以
	fmt.Printf("intChan len is %v, cap is %v\n", len(intChan), cap(intChan)) //intChan len is 2, cap is 3

	//5. 从管道取数据, 管道长度变化, 容量不变
	var num2 int
	num2 = <-intChan
	fmt.Println("num2 = ", num2)                                             //10
	fmt.Printf("intChan len is %v, cap is %v\n", len(intChan), cap(intChan)) //intChan len is 1, cap is 3

	//6. 在没有使用协程的情况下, 如果我们的管道数据已经全部取出, 再取就会报告deadlock
	num3 := <-intChan
	fmt.Println("num3 = ", num3) //211
	// num4 := <-intChan //fatal error: all goroutines are asleep - deadlock!
	// fmt.Println("num4 = ", num4)
}
```

**Channel使用的注意事项**

1) channel 中只能存放指定的数据类型
2) channle 的数据放满后, 就不能再放入了
3) 如果从 channel 取出数据后, 可以继续放入
4) 在没有使用协程的情况下, 如果 channel 数据取完了, 再取, 就会报 dead lock

#### Channel 的一个实例

```go
package main

import (
	"fmt"
)

type Cat struct {
	Name string
	Age  int
}

func main() {

	/*
		创建一个管道, 最多可以存放10个任意数据类型的变量
	*/
	//interface可以存放任意类型的变量
	var allChan chan interface{}
	allChan = make(chan interface{}, 10)
	cat1 := Cat{Name: "tom", Age: 18}
	cat2 := Cat{Name: "tom2", Age: 20}
	a := 10.9
	b := 10
	allChan <- cat1
	allChan <- cat2
	allChan <- a
	allChan <- b
	//取cat2出来
	//FIFO先进先出 取cat2,必须先让cat1从管道中出去
	<-allChan
	cat22 := <-allChan
	//下面的写法是错误的
	//fmt.Printf("cat22 type is %T, cat22.Name is %v", cat22, cat22.Name) //cat22.Name undefined (type interface {} is interface with no methods)
	//需要使用类型断言
	cat222 := cat22.(Cat)
	fmt.Printf("cat222 type is %T, cat222.Name is %v", cat22, cat222.Name) //cat22.Name undefined (type interface {} is interface with no methods)

}
```

#### Channel的遍历和关闭

使用内置函数 close 可以关闭 channel, 当 channel 关闭后, 就不能再向 channel 写数据了, 但是仍然可以从该 channel 读取数据

发送者可通过 close 关闭一个信道来表示没有需要发送的值了. 接收者可以通过为接收表达式分配第二个参数来测试信道是否被关闭: 若没有值可以接收且信道已被关闭, 那么在执行完

```go
v, ok := <-chan
```

之后 ok 会被设置为 false. 

循环 for i := range c 会不断从信道接收值, 直到它被关闭. 

**注意: **只有发送者才能关闭信道, 而接收者不能. 向一个已经关闭的信道发送数据会引发程序恐慌（panic）. 

**还要注意: ** 信道与文件不同, 通常情况下无需关闭它们. 只有在必须告诉接收者不再有需要发送的值时才有必要关闭, 例如终止一个 range 循环. 

**Channel的关闭**

```go
package main

import (
	"fmt"
)

func main() {
	intChan := make(chan int, 3)
	intChan <- 1
	intChan <- 2
	close(intChan)
	//intChan <- 3 //通道关闭, 无法写入 panic: send on closed channel
	fmt.Println("intChan is closed")
	a, ok := <-intChan
	fmt.Println(a, ok) //关闭的通道可以取
	b, ok := <-intChan
	fmt.Println(b, ok) //关闭的通道可以取
	c, ok := <-intChan
	fmt.Println(c, ok) //若没有值可以接收, 且信道已关闭 则ok = Flase

}
```

**channel的遍历**


channel 支持 for-range 的方式进行遍历, 请注意两个细节

1) 在遍历时, 如果 channel 没有关闭, 则回出现 deadlock 的错误
2) 在遍历时, 如果 channel 已经关闭, 则会正常遍历数据, 遍历完后, 就会退出遍历. 

```go
package main

import (
	"fmt"
)

func main() {

	intChan2 := make(chan int, 3)
	intChan2 <- 1
	intChan2 <- 2
	intChan2 <- 3
	close(intChan2) //如果管道没有关闭会出现fatal error: all goroutines are asleep - deadlock!
	for v := range intChan2 {
		fmt.Println(v)
	}

}
```

因为channel的长度是动态变化的, 所以我们最好不要使用for循环来对channel进行遍历.

#### Goroutine Channel综合案例一

```go
package main

import (
	"fmt"
	"time"
)

func writeData(intChan chan int) {
	for i := 0; i < 50; i++ {
		intChan <- i
		fmt.Println("写入数据", i)
		time.Sleep(time.Nanosecond)
	}
	close(intChan)
}

func readData(intChan chan int, exitChan chan bool) {
	for {
		v, ok := <-intChan
		if !ok {
			break
		}
		fmt.Println("读取数据", v)
		time.Sleep(time.Nanosecond)
	}
	exitChan <- true
	close(exitChan)

}

func main() {
	/*
		请完成goroutine和channel协同工作的案例, 要求:
		1. 开启一个writeData协程, 向管道intChan中写入50个整数;
		2. 开启一个readData协程, 从管道intChan中读取writeData写入的数据;
		3. 注意: writeData和readData操作的是同一个管道;
		4. 主线程需要等待writeData和readData协程都完成工作才能退出.
	*/
	intChan := make(chan int, 50)
	exitChan := make(chan bool, 1)
	go writeData(intChan)
	go readData(intChan, exitChan)
	for {
		_, ok := <-exitChan
		if !ok {
			break
		}
	}

}
```

**输出结果:**

```
写入数据 0
读取数据 0
写入数据 1
读取数据 1
写入数据 2
读取数据 2
写入数据 3
...
省略很多行
...
```

#### 阻塞

如果只是向管道写入数据,而**没有读取**, 就会出现阻塞而dead lock, 原因是管道容量有限, 而不定的往里面写而不取, 因此代码就会出现阻塞. 如果编译器写管道和读管道的频率不一致的话, 无所谓.

#### 协程求素数

需求: 要求统计1-200000 的数字中, 哪些是素数？这个问题在本章开篇就提出了, 现在我们有goroutine和channel 的知识后, 就可以完成了.

分析思路: 
- 传统的方法, 就是使用一个循环, 循环的判断各个数是不是素数. 
- 使用并发/并行的方式, 将统计素数的任务分配给多个goroutine 去完成, 完成任务时间短. 

```go
package main

import (
	"fmt"
	"time"
)

func putNum(intChan chan int) {
	for i := 1; i <= 200000; i++ {
		intChan <- i
	}
	close(intChan)
}
func primeJudge(intChan chan int, primeChan chan int, exitChan chan bool) {
	var flag bool
	for {
		num, ok := <-intChan
		if !ok { //通道关闭,且没有数据可取 ok = false
			break
		}
		//判断素数 假设是素数
		flag = true
		for i := 2; i < num^1/2; i++ {

			if num%i == 0 { //说明num不是素数
				flag = false
				break
			}

		}
		if flag && num != 1 { //如果是素数, 就放入primeChan, 注意1不是素数
			primeChan <- num
		}
	}
	fmt.Println("有一个协程因为取不到数据, 退出")
	//此时还不能关闭primeChan, 向exitChan写入ture
	exitChan <- true
}
func main() {
	start := time.Now().Unix()
	intChan := make(chan int, 10000)
	primeChan := make(chan int, 5000) //放入结果

	//标识退出的管道
	exitChan := make(chan bool, 8) //8个协程的退出标识
	//开启一个协程, 向intchan放入1-200000个数
	go putNum(intChan)
	//开启八个协程, 从intChan中取出数据, 并判断是否为素数, 如果是就放入primeChan
	for i := 0; i < 8; i++ {
		go primeJudge(intChan, primeChan, exitChan)
	}

	//判断GO判断素数的协程是否都完成了
	go func() {
		for i := 0; i < 8; i++ {
			<-exitChan
		}
		//当从exitChan取出了8个结果, 就可以放心的关闭primeChan了
		close(primeChan)
	}()

	//遍历primeChan, 并统计个数
	var sum int
	for {
		//res, ok := <-primeChan
		_, ok := <-primeChan
		if !ok {
			break
		}
		sum++
		//将结果打印出来
		//fmt.Printf("素数:%v\n", res)
		//time.Sleep(time.Second)
	}
	end := time.Now().Unix()
	fmt.Printf("共%v个 耗时%v秒", sum, end-start)
}
```

```
有一个协程因为取不到数据, 退出
有一个协程因为取不到数据, 退出
有一个协程因为取不到数据, 退出
有一个协程因为取不到数据, 退出
有一个协程因为取不到数据, 退出
有一个协程因为取不到数据, 退出
有一个协程因为取不到数据, 退出
有一个协程因为取不到数据, 退出
共17984个 耗时3秒
```

传统方法:

```go
package main

import (
	"fmt"
	"time"
)

func main() {
	start := time.Now().Unix()
	var sum int
	for num := 1; num < 200000; num++ {
		flag := true
		for i := 2; i < num^1/2; i++ {
			if num%i == 0 { //不是素数
				flag = false
				break
			}

		}
		if flag && num != 1 {
			sum++
		}
	}
	end := time.Now().Unix()
	fmt.Printf("素数共%v个,耗时%v秒", sum, end-start)
}
```

```
素数共17984个,耗时16秒
```

结论: 使用go协程后, 比普通方法提高至少4倍.

#### channel的注意事项和细节

##### Channel的方向

管道可以声明为只读或者只写, 默认情况下, 通道是双向的, 也就是, 既可以往里面发送数据也可以同里面接收数据. 

```go
var ch1 chan int     // ch1 是一个正常的 channel, 不是单向的
var ch2 chan<- float64 // ch2 是单向 channel, 只用于写 float64 数据
var ch3 <-chan int    // ch3 是单向 channel, 只用于读取 int 数据
```
	
- chan<-  表示数据进入管道, 要把数据写进管道, 对于调用者就是输出. 
- <-chan  表示数据从管道出来, 对于调用者就是得到管道的数据, 当然就是输入. 

##### 无缓冲的Channel


无缓冲的通道（unbuffered channel）是指在接收前没有能力保存任何值的通道. 

这种类型的通道要求发送 goroutine 和接收 goroutine 同时准备好, 才能完成发送和接收操作. 如果两个 goroutine 没有同时准备好, 通道会导致先执行发送或接收操作的 
goroutine 阻塞等待. 

这种对通道进行发送和接收的交互行为本身就是同步的. 其中任意一个操作都无法离开另一
个操作单独存在. 

下图展示两个goroutine 如何利用无缓冲的通道来共享一个值: 

![mark](http://image.i-ll.cc/blog/20190403/0fgHuP4OkyCI.png)\



- 在第 1  步, 两个 goroutine  都到达通道, 但哪个都没有开始执行发送或者接收. 
- 在第 2  步, 左侧的 goroutine  将它的手伸进了通道, 这模拟了向通道发送数据的行为. 这时, 这个 goroutine  会在通道中被锁住, 直到交换完成. 
- 在第 3  步, 右侧的 goroutine  将它的手放入通道, 这模拟了从通道里接收数据. 这个
goroutine  一样也会在通道中被锁住, 直到交换完成. 
- 在第 4  步和第 5  步, 进行交换, 并最终, 在第 6  步, 两个 goroutine  都将它们的手从通道里拿出来, 这模拟了被锁住的 goroutine  得到释放. 两个 goroutine  现在都可以去做别的事情了. 

无缓冲的channel 相当于是创建了一个容量为0的channel

```go
make(chan Type) //等价于 make(chan Type, 0)
```

##### 有缓冲的Channel

有缓冲的通道（buffered channel）是一种在被接收前能存储一个或者多个值的通道. 

这种类型的通道并不强制要求 goroutine 
之间必须同时完成发送和接收. 通道会阻塞发送和接收动作的条件也会不同. 只有在通道中没有要接收的值时, 接收动作才会阻塞. 只有在通道没有可用缓冲区容纳被发送的值时, 发送动作才会阻塞. 

这导致有缓冲的通道和无缓冲的通道之间的一个很大的不同: 无缓冲的通道保证进行发送和接收的 goroutine  会在同一时间进行数据交换；有缓冲的通道没有这种保证. 

示例图如下: 

![mark](http://image.i-ll.cc/blog/20190403/hEVwGw7SAzvu.png)\


- 在第 1  步, 右侧的 goroutine  正在从通道接收一个值. 
- 在第 2  步, 右侧的这个 goroutine 独立完成了接收值的动作, 而左侧的 goroutine  正在发送一个新值到通道里. 
- 在第 3  步, 左侧的 goroutine  还在向通道发送新值, 而右侧的 goroutine  正在从通道接收另外一个值. 这个步骤里的两个操作既不是同步的, 也不会互相阻塞. 
- 最后, 在第 4  步, 所有的发送和接收都完成, 而通道里还有几个值, 也有一些空间可以存更多的值. 

##### select作用

Go 里面提供了一个关键字 select, 通过 select 可以监听 channel 上的数据流动. 

select 的用法与 switch 语言非常类似, 由 select 开始一个新的选择块, 每个选择条件由 case语句来描述. 

与 switch 语句可以选择任何可使用相等比较的条件相比,  select 有比较多的限制, 其中最大的一条限制就是每个 case 语句里必须是一个 IO 操作, 大致的结构如下: 

```go
	select {

	case <-chan1:
		// 如果 chan1 成功读到数据, 则进行该 case 处理语句
	case chan2 <- 1:
		// 如果成功向 chan2 写入数据, 则进行该 case 处理语句
	default:
		// 如果上面都没有成功, 则进入 default 处理流程

	}
```

在一个 select 语句中, Go 语言会按顺序从头至尾评估每一个发送和接收的语句. 
 
如果其中的任意一语句可以继续执行(即没有被阻塞), 那么就从那些可以执行的语句中任意选择一条来使用


如果没有任意一条语句可以执行(即所有的通道都被阻塞), 那么有两种可能的情况: 
- 如果给出了 default 语句, 那么就会执行 default 语句, 同时程序的执行会从 select 语句后的语句中恢复. 
- 如果没有 default 语句, 那么 select 语句将被阻塞, 直到至少有一个通信可以进行下去. 

示例代码

```go
package main

import (
	"fmt"
	"time"
)

func main() {

	//使用 select 可以解决从管道取数据的阻塞问题

	//1.定义一个管道 10 个数据int
	intChan := make(chan int, 10)
	for i := 0; i < 10; i++ {
		intChan <- i
	}
	//2.定义一个管道 5 个数据 string
	stringChan := make(chan string, 5)
	for i := 0; i < 5; i++ {
		stringChan <- "hello" + fmt.Sprintf("%d", i)
	}
	//传统的方法在遍历管道时, 如果不关闭会阻塞而导致 deadlock

	//问题, 在实际开发中, 可能我们不好确定什么关闭该管道.
	//可以使用 select 方式可以解决
	//label:
	for {
		select {
		//注意: 这里, 如果 intChan 一直没有关闭, 不会一直阻塞而 deadlock
		//, 会自动到下一个 case 匹配
		case v := <-intChan:
			fmt.Printf("从 intChan 读取的数据%d\n", v)
			//time.Sleep(time.Second)
		case v := <-stringChan:
			fmt.Printf("从 stringChan 读取的数据%s\n", v)
			time.Sleep(time.Second)
		default:
			fmt.Printf("都取不到了, 不玩了, 程序员可以加入逻辑\n")
			time.Sleep(2 * time.Second)
			return
			//break label
		}
	}
}
```

输出结果:

```
从 stringChan 读取的数据hello0
从 intChan 读取的数据1
从 intChan 读取的数据2
从 intChan 读取的数据3
从 intChan 读取的数据4
从 stringChan 读取的数据hello1
从 stringChan 读取的数据hello2
从 intChan 读取的数据5
从 intChan 读取的数据6
从 intChan 读取的数据7
从 stringChan 读取的数据hello3
从 stringChan 读取的数据hello4
从 intChan 读取的数据8
从 intChan 读取的数据9
都取不到了, 不玩了, 程序员可以加入逻辑
```

**注**: 上面结果的输出顺序是无法控制的.

```go
package main

import (
	"fmt"
)

func fibonacci(c, quit chan int) {
	x, y := 1, 1
	for {
		select {
		case c <- x:
			x, y = y, x+y
		case <-quit:
			fmt.Println("quit")
			return
		}
	}
}
func main() {
	c := make(chan int)
	quit := make(chan int)
	go func() {
		for i := 0; i < 6; i++ {
			fmt.Println(<-c)
		}
		quit <- 0
	}()
	fibonacci(c, quit)
}
```

输出结果:

```
1
1
2
3
5
8
quit
```

##### goroutine中使用recover

为了让两个goroutine互不影响,使用recover, 解决协程中出现panic, 导致程序崩溃问题. 

```go
package main

import (
	"fmt"
)

func sayHi() {
	for i := 0; i < 5; i++ {
		fmt.Println("Hi~!", i)
	}
}

func test() {
	defer func() {
		//捕获test抛出的panic
		if err := recover(); err != nil {
			fmt.Println("test() error", err)
		}
	}()
	//定义了一个map
	var myMay map[int]string
	myMay[0] = "golang" //没有make, error
}

func main() {
	go sayHi()
	go test()
	for i := 0; i < 5; i++ {
		fmt.Println("main() ok=", i)
	}
}
```

```
Hi~! 0
Hi~! 1
Hi~! 2
Hi~! 3
Hi~! 4
main() ok= 0
main() ok= 1
main() ok= 2
main() ok= 3
main() ok= 4
test() error assignment to entry in nil map
```