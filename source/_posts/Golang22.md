---
title: Go语言编程系列(十七) - 反射
date: 2019-04-04 13:29:10
tags:
- Go
- 毕业论文
toc: True
---

### 反射的引出



1. 通过反射机制使序列化之后的key-val的key值是结构体Tag的值. 

    ![mark](http://image.i-ll.cc/blog/20190404/mc2SjjcOzGfl.png)\

2. 使用反射机制编写函数的适配器, 桥连接.

    ![mark](http://image.i-ll.cc/blog/20190404/jIqcktUmFY0D.jpg)\

<!-- more -->

### 反射的基本介绍

#### 基本介绍


[reflect包](https://studygolang.com/static/pkgdoc/pkg/reflect.htm)

![mark](http://image.i-ll.cc/blog/20190404/dhWuc3sznVyf.png)\

1) 反射可以在运行时**动态获取变量的各种信息**, 比如变量的类型(type), 类别(kind)
2) 如果是结构体变量, 还可以获取到结构体本身的信息(包括结构体的**字段、方法**)
3) 通过反射, 可以修改变量的值, 可以调用关联的方法. 
4) 使用反射, 需要 import (“reflect”)
5) 示意图
   
   ![mark](http://image.i-ll.cc/blog/20190404/2MxvcpJ4AngW.jpg)\

#### 反射重要的函数和概念

1. reflect.TypeOf(变量名), 获取变量的类型, 返回[reflect.Type](https://studygolang.com/static/pkgdoc/pkg/reflect.htm#Type)类型;
2. reflect.ValueOf(变量名), 获取变量的值, 返回[reflect.Value](https://studygolang.com/static/pkgdoc/pkg/reflect.htm#Value)类型.
3. 变量、interface{} 和reflect.Value 是可以相互转换的, 这点在实际开发中, 会经常使用到. 画出示意图

    ![mark](http://image.i-ll.cc/blog/20190404/nc7QpXhOyvqV.jpg)\

### 反射快速入门


代码如下:

```go
package main

import (
	"fmt"
	"reflect"
)

func reflectTest01(b interface{}) {
	//通过反射获取到传入的遍历的type, kind值
	//1. 先获取到reflect.Type
	rType := reflect.TypeOf(b)
	fmt.Printf("rType type is %T, value is %v\n", rType, rType)

	//2. 获取reflect.Value
	rVal := reflect.ValueOf(b)
	fmt.Printf("rVal type is %T, value is %v\n", rVal, rVal)
	//n2 := 2 + rVal //类型不一致, 无法操作, 正确写法如下
	n2 := 2 + rVal.Int()
	fmt.Println("n2 =", n2)
	//3. 将rVal转为interface{}
	iv := rVal.Interface()
	//将interface{}通过断言转成需要的类型
	num2 := iv.(int)
	fmt.Println("num2 =", num2)

}

type Student struct {
	Name string
	Age  int
}

func reflectTest02(stu interface{}) {

	//通过反射获取到传入的遍历的type, kind值
	//1. 先获取到reflect.Type
	rType := reflect.TypeOf(stu)
	fmt.Printf("rType type is %T, value is %v\n", rType, rType)

	//2. 获取reflect.Value
	rVal := reflect.ValueOf(stu)
	fmt.Printf("rVal type is %T, value is %v\n", rVal, rVal)

	//3. 将rVal转为interface{}
	iv := rVal.Interface()
	fmt.Printf("iv Type is %T, value is %v\n", iv, iv)
	//将interface{}通过断言转成需要的类型
	stu1, ok := iv.(Student)
	if ok {
		fmt.Printf("stu1.Name=%v\n", stu1.Name)
	}
}

func main() {
	//编写一个案例
	//演示对(基本数据类型、interface{}、reflect.Value)进行反射的基本操作

	//1. 定义一个int
	var num int = 100
	reflectTest01(num)

	//请编写一个案例
	//演示对(结构体类型、interface{}、reflect.Value)进行反射的基本操作

	//2. 定义一个结构体

	var stu = Student{"Tom", 20}
	reflectTest02(stu)

}

```

输出结果:

```
rType type is *reflect.rtype, value is int
rVal type is reflect.Value, value is 100
n2 = 102
num2 = 100
rType type is *reflect.rtype, value is main.Student
rVal type is reflect.Value, value is {Tom 20}
iv Type is main.Student, value is {Tom 20}
stu1.Name=Tom
```

### 反射注意事项和细节说明

1. reflect.Value.Kind, 获取变量的类别, 返回的是一个常量; [kind](https://studygolang.com/static/pkgdoc/pkg/reflect.htm#Kind)
2. Type  和 Kind 的区别: Type 是类型, Kind 是类别, Type 和 Kind 可能是相同的, 也可能是不同的. 比如:     var num int = 10   num 的 Type 是 int , Kind 也是 int比如: var stu Student   stu 的 Type 是 pkg1.Student , Kind 是 struct
3. 通过反射可以让变量在interface{}和Refle.Value之间相互转换.
4. 使用反射的方式来获取变量的值(并返回对应的类型), 要求数据类型匹配, 比如x是int, 那么就应该使用reflect.Value(x).Init(), 而不能使用其他的, 否则报panic.
5. 通过反射的来修改变量, 注意当使用SetXxx 方法来设置需要通过对应的指针类型来完成, 这样才能改变传入的变量的值, 同时需要使用到reflect.Value.Elem()方法.
    
    ![mark](http://image.i-ll.cc/blog/20190404/lvHX5OusqxIj.png)\

    ```go
    package main

    import (
        "fmt"
        "reflect"
    )

    //通过反射, 修改
    //num int的值
    //修改 student的值

    func reflect01(b interface{}) {
        //2. 获取到reflect.Value
        rVal := reflect.ValueOf(b)
        fmt.Printf("rVal type is %T value is %v\n", rVal, rVal.Elem())
        //3. 改变外部变量的值
        //rVal.SetInt(20) //reflect: reflect.Value.SetInt using unaddressable value
        /*
            rVal.Elem()类似于取值符* 获取指针指向变量
        */
        rVal.Elem().SetInt(20)
    }

    func main() {
        var num int = 10
        reflect01(&num)
        fmt.Println("num =", num)
    }
    ```

    输出结果:
    
    ```
    rVal type is reflect.Value value is 10
    num = 20
    ```

### 反射的最佳实践


1. 使用反射来遍历结构体的字段, 调用结构体的方法, 并获取结构体标签的值

    ![mark](http://image.i-ll.cc/blog/20190404/4BvH6OuTw0X3.png)\

    ![mark](http://image.i-ll.cc/blog/20190404/t2J0iVzzh1ti.png)\
        
    ```go
    package main

    import (
        "fmt"
        "reflect"
    )

    //定义了一个Monster 结构体
    type Monster struct {
        Name  string  `json:"name"`
        Age   int     `json:"monster_age"`
        Score float32 `json:"成绩"`
        Sex   string
    }

    //方法, 返回两个数的和
    func (s Monster) GetSum(n1, n2 int) int {
        return n1 + n2
    }

    //方法,  接收四个值, 给s 赋值
    func (s Monster) Set(name string, age int, score float32, sex string) {
        s.Name = name
        s.Age = age
        s.Score = score
        s.Sex = sex
    }

    //方法, 显示s 的值
    func (s Monster) Print() {
        fmt.Println("---start~----")
        fmt.Println(s)
        fmt.Println("---end~----")
    }
    func TestStruct(a interface{}) {
        //获取reflect.Type 类型
        typ := reflect.TypeOf(a)
        //获取reflect.Value 类型
        val := reflect.ValueOf(a)
        //获取到a 对应的类别
        kd := val.Kind()
        //如果传入的不是struct, 就退出
        if kd != reflect.Struct {
            fmt.Println("expect struct")
            return
        }
        //获取到该结构体有几个字段
        num := val.NumField()
        fmt.Printf("struct has %d fields\n", num) //4
        //变量结构体的所有字段
        for i := 0; i < num; i++ {
            fmt.Printf("Field %d: 值为=%v\n", i, val.Field(i))
            //获取到struct 标签, 注意需要通过reflect.Type 来获取tag 标签的值
            tagVal := typ.Field(i).Tag.Get("json")
            //如果该字段于tag 标签就显示, 否则就不显示
            if tagVal != "" {
                fmt.Printf("Field %d: tag 为=%v\n", i, tagVal)
            }
        }
        //获取到该结构体有多少个方法
        numOfMethod := val.NumMethod()
        fmt.Printf("struct has %d methods\n", numOfMethod)
        //var params []reflect.Value
        //方法的排序默认是按照函数名的排序（ASCII 码）
        val.Method(1).Call(nil) //获取到第二个方法. 调用它
        //调用结构体的第1 个方法Method(0)
        var params []reflect.Value //声明了[]reflect.Value
        params = append(params, reflect.ValueOf(10))
        params = append(params, reflect.ValueOf(40))
        res := val.Method(0).Call(params) //传入的参数是[]reflect.Value, 返回[]reflect.Value
        fmt.Println("res=", res[0].Int()) //返回结果, 返回的结果是[]reflect.Value*/
    }
    func main() {
        //创建了一个Monster 实例
        var a Monster = Monster{
            Name:  "黄鼠狼精",
            Age:   400,
            Score: 30.8,
        }
        //将Monster 实例传递给TestStruct 函数
        TestStruct(a)
    }
    ```

    输出结果:

    ```
    struct has 4 fields
    Field 0: 值为=黄鼠狼精
    Field 0: tag 为=name
    Field 1: 值为=400
    Field 1: tag 为=monster_age
    Field 2: 值为=30.8
    Field 2: tag 为=成绩
    Field 3: 值为=
    struct has 3 methods
    ---start~----
    {黄鼠狼精 400 30.8 }
    ---end~----
    res= 50
    ```

2. 使用反射的方式来获取结构体的tag 标签, 遍历字段的值, 修改字段值, 调用结构体方法(要求：通过传递地址的方式完成, 在前面案例上修改即可)

    - 这个的核心代码就在于, 将上例中的值传递改为地址传递;
    - 所以在遍历后面的field时需要先取值(也就是val.Elem(), 相当于*)
3. 定义了两个函数test1 和test2, 定义一个适配器函数用作统一处理接口;

    ```go
    package test

    import (
        "reflect"
        "testing"
    )

    func TestReflectFunc(t *testing.T) {
        call1 := func(v1 int, v2 int) {
            t.Log(v1, v2)
        }

        call2 := func(v1 int, v2 int, s string) {
            t.Log(v1, v2, s)
        }
        var (
            function reflect.Value
            inValue  []reflect.Value
            n        int
        )
        bridge := func(call interface{}, args ...interface{}) {
            n = len(args)
            inValue = make([]reflect.Value, n)
            for i := 0; i < n; i++ {
                inValue[i] = reflect.ValueOf(args[i])
            }
            function = reflect.ValueOf(call)
            function.Call(inValue) //看文档 Call方法
        }
        bridge(call1, 1, 2)
        bridge(call2, 1, 2, "test2")

    }
    ```
4. 使用反射操作任意结构体类型:
    ```go
    type user struct {
        Userid string
        Name   string
    }

    func TestReflectStruct(t *testing.T) {
        var (
            model *user
            sv    reflect.Value
        )
        model = &user{}
        sv = reflect.ValueOf(model)
        t.Log("reflect.ValueOf", sv.Kind().String()) //这里不带.String()的结果与带了的结果是一样的, 只是类型不一样
        sv = sv.Elem()
        t.Log("reflect.ValueOf.Elem", sv.Kind().String())
        sv.FieldByName("Userid").SetString("888888")
        sv.FieldByName("Name").SetString("Tom")
        t.Log("model", model)

    }
    ```
5. 使用反射创建并操作结构体
   
    ```go
    type user struct {
        Userid string
        Name   string
    }
    func TestReflectStructPtr(t *testing.T) {
        var (
            model *user
            st    reflect.Type
            elem  reflect.Value
        )
        st = reflect.TypeOf(model)                  //获取类型*user
        t.Log("reflect.TypeOf", st.Kind().String()) //ptr
        st = st.Elem()
        t.Log("reflect.TypeOf.Elem", st.Kind().String()) //struct

        elem = reflect.New(st)                                 //New返回一个Value类型值, 该值持有一个指向类型为st的新申请的零值的指针
        t.Log("reflect.New", elem.Kind().String())             //ptr
        t.Log("reflect.New.Elem", elem.Elem().Kind().String()) //Struct
        //model就是创建的user结构体变量
        model = elem.Interface().(*user) //model是*user类型 他的指向和elem是一样的
        elem = elem.Elem()               //取得elem指向的值
        elem.FieldByName("Userid").SetString("666666")
        elem.FieldByName("Name").SetString("Alice")
        t.Log("model model.Name", model.Name)    //model.Name=(*model).Name
        t.Log("model model.Name", (*model).Name) //model.Name=(*model).Name
    }
    ```

**3 4 5的输出如下**

```
=== RUN   TestReflectFunc
--- PASS: TestReflectFunc (0.00s)
    reflect_test.go:15: 1 2
    reflect_test.go:19: 1 2 test2
=== RUN   TestReflectStruct
--- PASS: TestReflectStruct (0.00s)
    reflect_test.go:46: reflect.ValueOf ptr
    reflect_test.go:48: reflect.ValueOf.Elem struct
    reflect_test.go:51: model &{888888 Tom}
=== RUN   TestReflectStructPtr
--- PASS: TestReflectStructPtr (0.00s)
    reflect_test.go:62: reflect.TypeOf ptr
    reflect_test.go:64: reflect.TypeOf.Elem struct
    reflect_test.go:67: reflect.New ptr
    reflect_test.go:68: reflect.New.Elem struct
    reflect_test.go:74: model model.Name Alice
    reflect_test.go:75: model model.Name Alice
PASS
ok      code/chapter17/reflectdemo03/test       0.297s
```