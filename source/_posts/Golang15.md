---
title: Go语言编程系列(十二) - Map
date: 2019-03-26 12:14:25
tags:
- Go
- 毕业论文
toc: True
---

### map基本介绍

#### map概述

Go 语言中的 map(映射、字典)是一种内置的数据结构, 它是一个无序的 key—value 对的集合, 比如以身份证号作为唯一键来标识一个人的信息. 


![mark](http://image.i-ll.cc/blog/20190326/5yJsItviBMjb.png)\


<!-- more -->

#### map的声明

```go
var map 变量名 map[keytype]valuetype
```


**key 可以是什么类型**

golang 中的 map, 的 key 可以是很多种类型, 比如 bool, 数字, string, 指针, channel , 还可以是只包含前面几个类型的 接口,  结构体,  数组通常 key 为 int 、string
**注意**: slice,  map 还有 function 不可以, 因为这几个没法用 ==  来判断

**valuetype 可以是什么类型**

valuetype 的类型和 key 基本一样, 这里我就不再赘述了通常为: 数字(整数,浮点数),string,map,struct

#### map声明的栗子

```go
var a map[string]string 
var a map[string]int 
var a map[int]string
var a map[string]map[string]string
```

还是刚开始的栗子

```go
//map的声明
var a map[int]string
//声明是不会分配内存的, 初始化需要make , 分配内存后才能赋值和使用. 
fmt.Println(a) //map[]
a = make(map[int]string, 10)
a[110] = "mike"
a[111] = "yoyo"
a[112] = "lily"
a[110] = "mike2"
fmt.Println(a)
```
输出结果

```
map[]
map[110:mike2 111:yoyo 112:lily]
```

对以上代码的补充说明:

1) map 在使用前一定要 make;
2) map 的 key 是不能重复, 如果重复了, 则以最后这个 key-value 为准;
3) map 的 value 是可以相同的;
4) map 的 key-value 是无序;
5) make 内置函数说明:

    ![mark](http://image.i-ll.cc/blog/20190326/qU3O9d4IJKQw.png)\

### map的使用方式

#### map的声明方式

1. 先声明, 再make, 然后复制;
    ```go
    //方式1:先声明
    var a map[int]string
    //声明是不会分配内存的, 初始化需要make , 分配内存后才能赋值和使用. 
    a = make(map[int]string, 10)
    a[110] = "mike"
    a[111] = "yoyo"
    a[112] = "lily"
    fmt.Println(a)//map[110:mike 111:yoyo 112:lily]
    ```
2. 直接make;
    ```go
    //方式2:直接make
    cities := make(map[string]string)
    cities["cit1"] = "北京"
    cities["cit2"] = "上海"
    cities["cit3"] = "广州"
    fmt.Println(cities)//map[cit1:北京 cit2:上海 cit3:广州]
    ```
3. 直接赋值;
    ```go
    //方式3:直接赋值
    heros := map[int]string{
        110: "mike",
        111: "yoyo",
        112: "lily",
    }
    fmt.Println(heros)//map[110:mike 111:yoyo 112:lily]
    ```

**map课堂练习**

```go
/*
    课堂练习：演示一个 key-value 的 value 是 map 的案例
    比如：我们要存放 3 个学生信息, 每个学生有 name 和 sex 信息
*/
//思路: 一个学生又有两个key, 所以可以采用map[string]map[string]string
studentMap := make(map[string]map[string]string)

studentMap["str01"] = make(map[string]string)
studentMap["str01"]["name"] = "tom"
studentMap["str01"]["gender"] = "male"
studentMap["str02"] = make(map[string]string)
studentMap["str02"]["name"] = "Alice"
studentMap["str02"]["gender"] = "female"
fmt.Println(studentMap)
```

输出结果:

```go
map[str01:map[gender:male name:tom] str02:map[gender:female name:Alice]]
```

#### map的增删改查操作

1. map的增加和更新:
    ```go
    map["key"] = value//如果key存在,则更新, 不存在, 则增加, 前面演示过.
    ```
2. map的删除操作: 
    
    delete(map, "key") , delete 是一个内置函数, 如果key 存在, 就删除该key-value,如果key 不存在, 
    ```go
    //map删除
    heros := map[int]string{
        110: "mike",
        111: "yoyo",
        112: "lily",
    }
    fmt.Println(heros) //map[110:mike 111:yoyo 112:lily]
    delete(heros, 110)
    fmt.Println(heros) //map[111:yoyo 112:lily]
    delete(heros, 110) //当指定的key不存在时, 删除不会操作, 也不会报错
    fmt.Println(heros) //map[111:yoyo 112:lily]
    ```
    细节说明:

    如果我们要删除 map 的所有 key ,没有一个专门的方法一次删除, 可以遍历一下 key, 逐个删除或者 map = make(...), make 一个新的, 让原来的成为垃圾, 被 gc 回收


3. map查找:
    ```go
    //map查找
    heros := map[int]string{
        110: "mike",
        111: "yoyo",
        112: "lily",
    }
    val, findRes := heros[110]
    if findRes {
        fmt.Printf("找到了,值为%v", val)
    } else {
        fmt.Println("没找到")
    }
    ```

    如果找到findRes为True,找不到为False.

#### map遍历

```go

	//使用for-range遍历map
	heros := map[int]string{
		110: "mike",
		111: "yoyo",
		112: "lily",
	}
	for k, v := range heros {
		fmt.Printf("k=%v\tv=%v\n", k, v)
	}
	//第一个是key,第二个是value,value可以省略
	for k := range heros {
		fmt.Printf("k=%v\theros[%v]=%v\n", k, k, heros[k])
	}
	//使用for-range遍历复杂的map
	studentMap := make(map[string]map[string]string)

	studentMap["str01"] = make(map[string]string)
	studentMap["str01"]["name"] = "tom"
	studentMap["str01"]["gender"] = "male"
	studentMap["str02"] = make(map[string]string)
	studentMap["str02"]["name"] = "Alice"
	studentMap["str02"]["gender"] = "female"
	for k1, v1 := range studentMap {
		fmt.Println("k1=", k1)
		for k2, v2 := range v1 {
			fmt.Printf("\t k2=%v\tv2=%v\n", k2, v2)
		}
    }
```

输出结果:

```
k=111   v=yoyo
k=112   v=lily
k=110   v=mike
k=112   heros[112]=lily
k=110   heros[110]=mike
k=111   heros[111]=yoyo
k1= str02
         k2=name        v2=Alice
         k2=gender      v2=female
k1= str01
         k2=gender      v2=male
         k2=name        v2=tom
```

#### map长度

![mark](http://image.i-ll.cc/blog/20190326/wUi2d5UYGt6l.png)\

栗子

```go
heros := map[int]string{
    110: "mike",
    111: "yoyo",
    112: "lily",
}

fmt.Println(len(heros)) //3
```

### map切片


切片的数据类型如果是 map, 则我们称为 slice of map, map 切片, 这样使用则 map 个数就可以动态变化了。

来个栗子:

```go
/*
    map 切片的使用案例:
    使用map记录一个学生的信息, 包括name和gender.
*/
var studentMapSlice []map[string]string
studentMapSlice = make([]map[string]string, 2) //准备放入两个学生
if studentMapSlice[0] == nil {
    studentMapSlice[0] = make(map[string]string, 2) //一个学生有两个信息 name和gender
    studentMapSlice[0]["name"] = "tom"
    studentMapSlice[0]["gender"] = "male"
}
if studentMapSlice[1] == nil {
    studentMapSlice[1] = make(map[string]string, 2)
    studentMapSlice[1]["name"] = "Alice"
    studentMapSlice[1]["gender"] = "female"
}
fmt.Println((studentMapSlice))
//两个位置已经满了,所以再一次添加的话不能与上面的写法一样,否则会出现out of range错误,需要用append函数.
newStudent := map[string]string{
    "name":   "Bob",
    "gender": "male",
}
studentMapSlice = append(studentMapSlice, newStudent)
fmt.Println((studentMapSlice))
```

输出结果:

```
[map[gender:male name:tom] map[gender:female name:Alice]]
[map[gender:male name:tom] map[gender:female name:Alice] map[gender:male name:Bob]]
```
### map排序


1) golang 中没有一个专门的方法针对 map 的 key 进行排序;
2) golang 中的 map 默认是无序的, 注意也不是按照添加的顺序存放的, 你每次遍历, 得到的输出可能不一样.(韩顺平老师用的是1.9.2版的Golang, 我此时用的是1.12版的Golang, 试了map[int]int和map[string]string,map是按key进行升序排列的)
    ```go
    /*
        map排序: 默认是按照升序排列.
    */
    map1 := make(map[int]int, 10)
    map1[10] = 100
    map1[11] = 10
    map1[1] = 13
    map1[4] = 56
    map1[8] = 90
    map1[12] = 10

    fmt.Println(map1)

    map2 := make(map[string]string, 10)
    map2["c"] = "bob"
    map2["a"] = "tom"
    map2["b"] = "alice"
    map2["e"] = "jack"
    map2["d"] = "allen"

    fmt.Println(map2)
    ```

    输出结果:

    ```
    map[1:13 4:56 8:90 10:100 11:10 12:10]
    map[a:tom b:alice c:bob d:allen e:jack]
    ```

    看一下老师的栗子:

    ```go
    /*
        map排序: 默认是按照升序排列.
    */
    map1 := make(map[int]int, 10)
    map1[10] = 100
    map1[11] = 10
    map1[1] = 13
    map1[4] = 56
    map1[8] = 90
    map1[12] = 10

    /*
        如果按照map的key的顺序进行排序输出
        1. 先将map的key放入到切片中
        2. 对切片排序
        3. 遍历切片, 然后按照key来输出map的值
    */
    var keys []int
    for k := range map1 { //前面提到过map的value可以省略
        keys = append(keys, k)
    }
    //排序
    sort.Ints(keys)
    fmt.Println(keys)
    for _, k := range keys {
        fmt.Printf("map1[%v]=%v\n", k, map1[k])
    }
    ```

    输出结果:

    ```
    [1 4 8 10 11 12]
    map1[1]=13
    map1[4]=56
    map1[8]=90
    map1[10]=100
    map1[11]=10
    map1[12]=10
    ```

3) golang 中 map 的排序, 是先将 key 进行排序, 然后根据 key 值遍历输出即可.


### map的使用细节

1. map 是引用类型, 遵守引用类型传递的机制, 在一个函数接收map, 修改后, 会直接修改原来的map;
2. map 的容量达到后, 再想map 增加元素, 会自动扩容, 并不会发生panic, 也就是说map 能动态的增长键值对(key-value);
3. map 的value 也经常使用**struct 类型**, 更适合管理复杂的数据(比前面value 是一个map 更好), 比如value 为Student 结构体.
    ```go
    type Student struct {
        Name    string
        Age     int
        Address string
    }

    func main() {
        /*
            map的key为学生的学号是唯一的, map的value为Student结构体包含学生的姓名, 年龄,地址
        */
        studentMap := make(map[int]Student, 3)
        Student1 := Student{"tom", 18, "北京"}
        Student2 := Student{"alice", 19, "西安"}
        Student3 := Student{"bob", 16, "上海"}
        studentMap[001] = Student1
        studentMap[002] = Student2
        studentMap[003] = Student3
        fmt.Println(studentMap)

        //遍历学生信息
        for k, v := range studentMap {
            fmt.Printf("学生的编号是%v\n", k)
            fmt.Printf("学生的名字是%v\n", v.Name)
            fmt.Printf("学生的年龄是%v\n", v.Age)
            fmt.Printf("学生的地址是%v\n", v.Address)
            fmt.Println()
        }
    }
    ```

    输出结果:

    ```
    map[1:{tom 18 北京} 2:{alice 19 西安} 3:{bob 16 上海}]
    学生的编号是1
    学生的名字是tom
    学生的年龄是18
    学生的地址是北京

    学生的编号是2
    学生的名字是alice
    学生的年龄是19
    学生的地址是西安

    学生的编号是3
    学生的名字是bob
    学生的年龄是16
    学生的地址是上海
    ```

### map课堂练习


```go
func modifyUser(users map[string]map[string]string, name string) {

	//判断users中是否有name
	//v, ok := users[name] 这个方法略麻烦
	if users[name] != nil {
		users[name]["pwd"] = "888888"
	} else {
		//没有这个用户
		users[name] = make(map[string]string, 2)
		users[name]["pwd"] = "888888"
		users[name]["nickname"] = "昵称~" + name //示意

	}
}

func main() {

	/*
		1)使用map[string]map[string]sting 的map 类型
		2)key: 表示用户名，是唯一的，不可以重复
		3)如果某个用户名存在，就将其密码修改"888888"，如果不存在就增加这个用户信息,
		（包括昵称nickname 和密码pwd）。
		4)编写一个函数modifyUser(users map[string]map[string]sting, name string) 完成上述功能
	*/
	users := make(map[string]map[string]string, 10)
	users["alice"] = make(map[string]string, 2)
	users["alice"]["pwd"] = "666666"
	users["alice"]["nickname"] = "二哈"
	modifyUser(users, "bob")
	modifyUser(users, "tom")
	modifyUser(users, "jack")
	fmt.Println(users)

}
```

输出结果:

```
map[alice:map[nickname:二哈 pwd:666666] bob:map[nickname:昵称~bob pwd:888888] jack:map[nickname:昵称~jack pwd:888888] tom:map[nickname:昵称~tom pwd:888888]]
```
