---
title: Go语言编程系列(九) - 程序流程控制
date: 2019-03-09 17:51:46
tags:
- Go
- 毕业论文
toc: True
---

### 程序流程控制介绍

在程序中, 程序运行的流程控制决定程序是如何执行的, 是我们必须掌握的, 主要有三大流程控制语句. 

1) 顺序控制: 程序从上到下逐行执行, 没有任何判断和跳转;
2) 分支控制: 让程序有选择的执行;
3) 循环控制: 一段代码的循环执行.

<!-- more -->

### 顺序控制


顺序控制: 程序从上到下逐行执行, 没有任何判断和跳转;Golang 中定义变量时采用合法的前向引用. 如:  

```go
func main() {
var num1 int = 10 //声明了 num1
var num2 int = num1 + 20 //使用 num1 
fmt.Println(num2)
}
```

```go
//错误形式：
func main() {
var num2 int = num1 + 20 //使用 num1 
var num1 int = 10 // 声 明 num1 (×) 
fmt.Println(num2)
}
```

### 分支控制

分支控制 - 让程序有选择的执行, 主要有以下三种:

1. 单分支
2. 双分支
3. 多分支
4. switch分支控制


#### 单分支控制

基本语法:
```go
if 条件表达式 {
    执行代码块
}
```
当条件表达式为True时,执行{}中的语句,即使{}只有一条语句,也一样要加{}.

**栗子在这里:**

```go
func main() {
	//如果一个同学年龄大于等于18,输出"成年了,要对自己的行为负责"
	var age int
	fmt.Println("请输入年龄!")
	fmt.Scanln(&age)
	if age >= 18 {
		fmt.Println("成年了,要对自己的行为负责")
	}
}
```

输出结果:

```
请输入年龄!
18
成年了,要对自己的行为负责
```
```
请输入年龄!
5
```

Golang中支持在if中直接定义一个变量,比如:
```go
if age := 20; age > 18 {
    fmt.Println("你年龄大于18,要对自己的行为负责")
}
```

输出结果:

>你年龄大于18,要对自己的行为负责

#### 双分支控制

基本语法:
```go
if 条件表达式{
    执行代码块1
} else{
    执行代码块2
}
```
当条件表达式为True,执行代码块1,否则执行代码块2. 同样{}必须有. 

**栗子如下:**

```go
//编写一个程序, 如果该同志年龄大于等于18岁,输出"你成年了,要对自己的行为负责"
//否则输出"这次饶了你"
var age int
fmt.Println("请输入年龄!")
fmt.Scanln(&age)
if age >= 18 {
    fmt.Println("你成年了,要对自己的行为负责")
} else {
    fmt.Println("这次饶了你")

}
```
输出结果:
```
请输入年龄!
29
你成年了,要对自己的行为负责
```

```
请输入年龄!
17
这次饶了你
```

**注意:**双分支只会执行其中一个分支.

#### 多分支控制

基本语法:
```go
if 条件表达式1{
    执行代码块1
} else if 条件表达式2{
    执行代码块2
}
......else{
    执行代码块n
}

```


多分支的判断流程如下:

1. 先判断条件表达式 1 是否成立, 如果为真, 就执行代码块1;
2.  如果条件表达式 1 如果为假, 就去判断条件表达式 2 是否成立,  如果条件表达式 2 为真,  就执行代码块 2;
3.  依次类推;
4.  如果所有的条件表达式不成立, 则执行 else 的语句块;
5.  else 不是必须的;
6.  多分支只能有一个执行入口.

**多分支快速入门案例:**

```go
/*
    岳小鹏参加 Golang 考试, 他和父亲岳不群达成承诺：
    如果：
    成绩为 100 分时, 奖励一辆 BMW；
    成绩为(80, 99]时, 奖励一台 iphone7plus； 当成绩为[60,80]时, 奖励一个 iPad；
    其它时, 什么奖励也没有. 
    请从键盘输入岳小鹏的期末成绩, 并加以判断

*/
var score int
fmt.Println("请输入成绩")
fmt.Scanln(&score)
if score == 100 {
    fmt.Println("BMW")
} else if score > 80 && score < 99 {
    fmt.Println("iphone7plus")
} else if score >= 60 && score <= 80 {
    fmt.Println("ipad")
} else {
    fmt.Println("none")
}
```

#### 嵌套分支

在一个分支结构中又完整的嵌套了另一个完整的分支结构, 里面的分支结构成为内层分支, 外面的分支结构成为外层分支.

基本语法:
```go
if 条件表达式1{
    if 条件表达式2{
        ...
    } else{
        ...
    }
} else{
    ...
}
```

嵌套分支不宜过多,建议控制在3层以内.

**栗子如下:**

```go
/*
    4_10  旺季：
    成人（18-60）：60
    儿童（<18）:半价
    老人（>60）:1/3
    淡季：
    成人：40 其他：20
*/
var month byte
var age byte
var price = 60.0
fmt.Println("请输入季节和游客年龄中间用空格隔开!")
fmt.Scanf("%d %d", &month, &age)
if month >= 4 && month <= 10 {
    //旺季
    if age < 18 {
        fmt.Printf("月份%v,年龄%v,应收您%v元", month, age, price/2)
    } else if age > 60 {
        fmt.Printf("月份%v,年龄%v,应收您%v元", month, age, price/3)
    } else {
        fmt.Printf("月份%v,年龄%v,应收您%v元", month, age, price)
    }
} else {
    //淡季
    if age > 18 {
        fmt.Printf("月份%v,年龄%v,应收您40元", month, age)
    } else {
        fmt.Printf("月份%v,年龄%v,应收您20元", month, age)
    }
}
```

输出结果:
```
请输入季节和游客年龄中间用空格隔开!
12 66
月份12,年龄66,应收您40元
```

#### Switch分支控制

1. switch语句用于基于不同条件执行不同动作, 是编写一连串 if - else 语句的简便方法. 它运行第一个值等于条件表达式的 case 语句. 每一个 case 分支都是唯一的, 从上到下顺次执行, 直到匹配为止,如果没有匹配到, 执行default语句块;
2. 匹配项后面**不需要加break**;
3. Go 中的case后的表达式可以有多个, 使用逗号间隔;
4. Go 的另一点重要的不同在于 switch 的 case 无需为常量, 且取值不必为整数.

**基本语法**

```go
switch 表达式{
    case 表达式1,表达式2,...:
        语句块1
    case 表达式3,表达式4,...:
        语句块2
    //这里可以有多个case语句
    default:
        语句块
}
```

**switch快速入门案例**

```go
/*
    请编写一个程序, 该程序可以接收一个字符, 比如:a,b,c,
    根据用户的输入显示相应的信息.要求使用 switch 语句完成
*/

var key byte
fmt.Println("请输入一个字符:a,b,c")
fmt.Scanf("%c", &key)

switch key {
case 'a':
    fmt.Printf("a%c", key)
case 'c':
    fmt.Printf("c%c", key)
case 'b':
    fmt.Printf("b%c", key)
default:
    fmt.Println("输入错误")
}
```

输出结果:
```
请输入一个字符:a,b,c
a
aa
```

```
请输入一个字符:a,b,c
h
输入错误
```

**switch使用的细节问题**

1. case/switch 后是一个表达式(即: 常量值、变量、一个有返回值的函数等都可以);
2. case 后的各个表达式的值的数据类型, 必须和 switch 的表达式数据类型一致;
3. case 后面可以带多个表达式, 使用逗号间隔. 比如 case 表达式 1, 表达式 2...;
4. case 后面的表达式如果是常量值(字面量), 则要求不能重复;
5. case 后面不需要带 break, 程序匹配到一个 case 后就会执行对应的代码块, 然后退出 switch, 如 果一个都匹配不到, 则执行 default;
6. default 语句不是必须的;
7. switch 后也可以不带表达式, 类似 if--else 分支来使用;如下:
    ```go
    var a int
    fmt.Println("请输入a的值")
    fmt.Scanln(&a)
    switch {
    case a == 5:
        fmt.Println("a等于5")
    case a == 10:
        fmt.Println("a等于10")
    default:
        fmt.Println("a等于其他")

    }
    ```
    输出结果:
    ```
    请输入a的值
    5
    a等于5
    ```

    ```
    请输入a的值
    9
    a等于其他
    ```
8. switch 后也可以直接声明/定义一个变量, 分号结束(不推荐);
    ```go
    switch grade := 90; {
	case grade >= 90:
		fmt.Println("成绩优秀")
	case grade >= 80 && grade < 90:
		fmt.Println("成绩良好")
	default:
		fmt.Println("你还不够优秀,继续努力!")
	}
    ```
9. switch穿透 - fallthrough, 如果在 case 语句块后增加 fallthrough, 则会继续执行下一个case,也叫 switch 穿透;
    ```go
	//switch 穿透
	num := 10
	switch num {
	case 10:
		fmt.Println("ok1")
		fallthrough
	case 20:
		fmt.Println("ok2")
	case 30:
		fmt.Println("ok3")
	default:
		fmt.Println("没有匹配到")
	}
    ```
    输出结果:
    ```
    ok1
    ok2
    ```
10. Type Switch: switch 语句还可以被用于 type-switch 来判断某个 interface 变量中实际指向的变量类型.
    ```go
    var x interface{}
    y := 10.0
    x = y
    //x.(type)可以读取接口x实际指向的数据类型
    switch i := x.(type) {
    case nil:
        fmt.Printf("x的类型~:%T", i)
    case int:
        fmt.Printf("x的类型是int")
    case float64:
        fmt.Printf("x的类型是float64")
    case func(int) float64:
        fmt.Printf("x的类型是func(int)")
    case bool:
        fmt.Printf("x的类型是bool")
    case string:
        fmt.Printf("x的类型是string")
    default:
        fmt.Println("未知类型")
    }
    ```

    输出结果:

    >x的类型是float64

**switch和if的比较**

1) 如果判断的具体数值不多, 而且符合整数、浮点数、字符、字符串这几种类型. 建议使用 swtich语句, 简洁高效;
2) 其他情况: 对区间判断和结果为 bool 类型的判断, 使用 if, if 的使用范围更广.


### 循环控制

循环控制, 就是让自己的代码循环执行.

**一个简单的案例:**

```go
/*
    编写一个程序: 输出10次Hello Golang!"
*/
for i := 0; i < 10; i++ {
    fmt.Println("Hello Golang!")
}
```

#### for循环控制

**语法格式**

```go
for 循环变量初始化; 循环条件; 循环变量迭代 { 
    循环操作(语句) 
}
```

**for循环四要素**

1) 循环变量初始化;
2) 循环条件;
3) 循环操作(语句),有人也叫循环体;
4) 循环变量迭代.

**for循环执行顺序**

1. 循环变量初始化, 例如 i := 0;
2. 执行循环条件, 例如 i < 10;
3. 如果循环条件为真, 执行循环操作, 比如fmt.Println("Hello Golang!");
4. 执行循环变量迭代, 例如i++;
5. 反复执行2,3,4步, 直到循环条件为False, 退出循环.

**for循环注意事项及使用细节**

1. 循环条件是返回一个布尔值的表达式;
2. 使用for循环的第一种方式中,变量的作用域旨在for和{}之间;
3. for循环的第二种使用方式, 即将变量初始化和变量迭代写到其他位置;如下:
    ```go
    变量初始化
    for 循环条件 {
        循环执行语句
        变量迭代
    }
    ```

    还是最初的那个案例

    ```go
    i := 0
	for i < 10 {
		fmt.Println("Hello Golang!")
		i++
	}
    ```
3. for循环的第三种使用方式;如下
   ```go
   for {
       循环执行语句
   }
   ```

    这种写法实际上是for ;;{}是一个死循环,通常需要配合 break 语句使用.

    还是最初的案例:

    ```go
    // for 循环第三种写法
	i := 0
	for {
		if i < 10 {
			fmt.Println("Hello Golang!")
		} else {
			break //break是跳出循环,注意if不是循环语句
		}
		i++
    }
    ```
4. Golang 提供 for-range 的方式, 可以方便遍历字符串和数组(**数组的遍历,见后续笔记**)
    ```go
    //for遍历字符串 传统方式
	var str = "Hello Golang!"
	for i := 0; i < len(str); i++ {
		fmt.Printf("%c\n", str[i])
    }
    ```

    输出结果:

    ```
    H
    e
    l
    l
    o

    G
    o
    l
    a
    n
    g
    !
    ```

    ```go
    /*
		for遍历字符串 for-range方式
		str2[0] = 'H'
		str2[1] = 'e'
		str2[2] = 'l'
		str2[3] = 'l'
		str2[4] = 'o'
		str2[5] = ''
		str2[6] = 'G'
		str2[7] = 'o'
		str2[8] = 'l'
		str2[9] = 'a'
		str2[10] = 'n'
		str2[11] = 'g'
		str2[12] = '!'
	*/
	var str2 = "Hello Golang!"
	for index, val := range str2 {
		fmt.Printf("index=%d,val=%c\n", index, val)
	}
    ```

    输出结果

    ```
    index=0,val=H
    index=1,val=e
    index=2,val=l
    index=3,val=l
    index=4,val=o
    index=5,val=
    index=6,val=G
    index=7,val=o
    index=8,val=l
    index=9,val=a
    index=10,val=n
    index=11,val=g
    index=12,val=!
    ```

    **上述代码细节讨论**
    
    如果我们的字符串含有中文, 那么传统遍历字符串的方式是错误的, 会出现乱码. 因为传统的对字符串的遍历方式是按照字节来遍历的, 而汉字在utf8编码中占3个字节. 所以需要将str转为[]rune. 

    ```go
    var str = "你好 Golang!"
	str2 := []rune(str)
	for i := 0; i < len(str2); i++ {
		fmt.Printf("%c\n", str2[i])
	}

    ```

    输出结果:

    ```
    你
    好

    G
    o
    l
    a
    n
    g
    !
    ```

    对于for-range遍历方式而言, 是按照字符的方式遍历. 因此如果字符串中有中文字符, 是可以遍历出来的, 不过是"1个汉字住了3个房间". 

    ```go
	var str = "你好 Golang!"
	for index, val := range str {
		fmt.Printf("index=%d,val=%c\n", index, val)
	}
    ```

    输出结果

    ```
    index=0,val=你
    index=3,val=好
    index=6,val=
    index=7,val=G
    index=8,val=o
    index=9,val=l
    index=10,val=a
    index=11,val=n
    index=12,val=g
    index=13,val=!
    ```

    做了切片处理之后的结果

    ```go
	var str = "你好 Golang!"
	str2 := []rune(str)
	for index, val := range str2 {
		fmt.Printf("index=%d,val=%c\n", index, val)
	}
    ```

    输出结果:

    ```
    index=0,val=你
    index=1,val=好
    index=2,val=
    index=3,val=G
    index=4,val=o
    index=5,val=l
    index=6,val=a
    index=7,val=n
    index=8,val=g
    index=9,val=!
    ```

#### for循环实现while和do...while控制

Go 语言没有 while 和 do...while 语法, 这一点需要同学们注意一下, 如果我们需要使用类似其它语言(比如 java/c 的 while 和 do...while), 可以通过 for 循环来实现其使用效果. 

**for循环实现while**

```go
循环变量初始化
for{
    if 循环条件{
        break //跳出for循环
    }
    循环操作
    循环变量迭代
}
```

同样,还是用for循环最初的栗子:

```go
//for实现while
i := 0
for {
    if i > 10 {
        break
    }
    fmt.Println("Hello Golang!")
    i++
}
```

**for循环实现do...while**

```go
循环变量初始化
for{
    循环操作
    循环变量迭代
    if 循环条件{
        break //跳出for循环
    }
}
```

```go
//for实现do...while
i := 0
for {
    fmt.Println("Hello Golang!")
    i++
    if i > 10 {
        break
    }
}
```

两者的区别是, 无论如何do...while至少执行一次. 将以上两个代码中的> 10都改为<= 0,则do...while方法可以输出一条, while不输出.

#### 多重循环控制

1) 将一个循环放在另一个循环体内, 就形成了嵌套循环. 在外边的 for 称为外层循环在里面的 for 循环称为内层循环. 【建议一般使用两层, 最多不要超过 3 层】; 
2) 实质上, 嵌套循环就是把内层循环当成外层循环的循环体. 当只有内层循环的循环条件为 false时, 才会完全跳出内层循环, 才可结束外层的当次循环, 开始下一次的循环;  
3) 外层循环次数为 m 次, 内层为 n 次, 则内层循环体实际上需要执行 m*n 次. 

**打印金字塔经典案例**

案例说明:打印一个如下所示样子的金字塔:

```
   *
  ***
 *****
```


```go
//for 打印半个金字塔
var n int
fmt.Println("请输入金字塔层数")
fmt.Scanln(&n)
//i表示当前打印第i层
for i := 1; i <= n; i++ {
    //j表示当时第i层打印j个
    for j := 1; j <= i; j++ {
        fmt.Print("*")
    }
	fmt.Println()
}
```

输出结果:

```
请输入金字塔层数
5
*
**
***
****
*****
```

继续改进:

```go
/*
    for 打印整个金字塔 - 思路分析:
    1. 设打n层金字塔
    2. 最后一层有2n - 1个 *
    3. 第n行比第i行多2n-1 - (2i-1)=2(n-i)个*
    4. 每一行的*都要居中
    5. 所以3中第i行少的*用空格代替,且左右平分,也就是说每一层前后都要有n-i个空格

*/
var n int
fmt.Println("请输入金字塔层数")
fmt.Scanln(&n)
//i表示当前打印第i层
for i := 1; i <= n; i++ {
    //打印前面的空格
    for k := 1; k <= n-i; k++ {
        fmt.Print(" ")
    }
    //j表示第i层打印的数量
    for j := 1; j <= 2*i-1; j++ {
        fmt.Print("*")
    }
    //打印后面的空格
    for v := 1; v <= n-i; v++ {
        fmt.Print(" ")
    }
    fmt.Println()
}
```

输出结果:

```
请输入金字塔层数
6
     *
    ***
   *****
  *******
 *********
***********
```

**打印九九乘法表经典案例**

```go
/*
    打印九九乘法表

*/
for i := 1; i <= 9; i++ {
    for j := 1; j <= i; j++ {
        fmt.Printf("%v*%v=%v\t", j, i, i*j)
    }
    fmt.Println()
}
```

### 跳转控制语句

#### 跳转控制语句 - break

break 语句用于终止某个语句块的执行, 用于中断当前 for 循环或跳出 switch 语句. 

一个栗子:

```go
/*
    编写一个程序: 随机生成1-100之间的数字, 如果生成了99,则停止,返回随机生成的次数

    思路分析:
        编写一个无限循环的控制, 不断生成随机数, 当生成99时, break退出循环

*/
count := 0
for {
    n := rand.Intn(101) //产生区间[0,101)的整数
    fmt.Println("n=", n)
    count++
    if n == 99 {
        break //跳出for循环
    }
}
fmt.Printf("产生99用了%v次", count)
```

**break注意事项和使用细节**

1. break 语句出现在多层嵌套的语句块中时,可以通过**标签**指明要终止的是哪一层语句块;
2. 标签使用的演示
    ```go
    lable2:
	for i := 0; i < 4; i++ {
		//lable1:
		for j := 0; j < 10; j++ {
			if j == 2 {
				break lable2 //break默认跳出最近的for循环
			}
			fmt.Println("j=", j)
		}
    }
    ```

    输出结果:

    ```
    0
    1
    ```

    对上面代码的说明: 
    
    1. break默认跳出最近的for循环;
    2. break 后面可以指定标签, 跳出标签对应的 for 循环;
    3. break后面跟lable1与默认是一种情况, 都会输出4个01.

#### 跳转控制语句 - continue

1. continue 语句用于结束本次循环, 继续执行下一次循环;
2. continue 语句出现在多层嵌套的循环语句体中时, 可以通过标签指明要跳过的是哪一层循环 , 这个和前面的 break 标签的使用的规则一样. 两者的区别就是, break是跳出循环, 而continue是跳出本次循环,继续执行下一次循环.

**栗子**

```go
for i := 0; i < 2; i++ {
lable1:
    for j := 0; j < 4; j++ {
        if j == 2 {
            continue lable1 //continue默认最近的for循环中的本次循环,执行下一次循环
        }
        fmt.Println("j=", j)
    }
}
```

输出结果:

```
j= 0
j= 1
j= 3
j= 0
j= 1
j= 3
```

#### 跳转控制语句 - goto

1) Go 语言的 goto 语句可以无条件地转移到程序中指定的行;
2) goto 语句通常与条件语句配合使用. 可用来实现条件转移, 跳出循环体等功能; 
3) 在 Go 程序设计中一般不主张使用 goto 语句,  以免造成程序流程的混乱, 使理解和调试程序都产生困难.

**基本语法**

```go
goto lable

...

lable:lable
```

**栗子**

```go
	fmt.Println("ok1")
	goto lable1
	fmt.Println("ok2")
	fmt.Println("ok3")
lable1:
	fmt.Println("ok4")
	fmt.Println("ok5")
```

输出结果:

```
ok1
ok4
ok5
```

#### 跳转控制语句 - return

return 使用在方法或者函数中, 表示跳出所在的方法或函数. return的详细介绍在后面的函数部分.

**栗子**

```go
for i := 0; i < 10; i++ {
    if i == 2 {
        return
    }
    fmt.Println("Hello Golang!")
}
```

输出结果:

```
Hello Golang!
Hello Golang!
```

**关于return的说明:**

1. 如果 return 是在普通的函数, 则表示跳出该函数, 即不再执行函数中 return 后面代码, 也可以理解成终止函数.  
2. 如果 return 是在 main 函数, 表示终止 main 函数, 也就是说终止程序. 