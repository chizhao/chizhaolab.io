---
title: 计算机中数的表示(二) - 数的定点表示和浮点表示
date: 2019-03-04 18:26:50
tags: 计算机组成原理
toc: True
---


### 数的定点表示

小数点按约定的方式给出.

<!-- more -->


![mark](http://image.i-ll.cc/blog/20190304/BV6EWqxtwvkE.png)\


### 数的浮点表示

**Q: 为什么要引入浮点数表示?**
- 编程困难, 程序员需要调节小数点的位置;
- 数的表示范围小, 为了能表示两个大小相差很大的数据, 需要很长的机器字长

    ![mark](http://image.i-ll.cc/blog/20190304/RHDIB1wyaIig.png)\

- 数据存储单元的利用率很低

**浮点表示**

![mark](http://image.i-ll.cc/blog/20190304/ckchGCc0ozDJ.png)\
在计算机中,第1种和第4种的表示方法是合法的. 尾数的值需要小于等于1.

#### 浮点数的表示形式

![mark](http://image.i-ll.cc/blog/20190304/L7u0b8DO1dos.png)\

#### 浮点数的表示范围

**基值为2的情况下的原码表示(黑色)**

最小负数:
1. 阶符为正,也就是0;
2. 尾数的数值部分全1;
3. 阶码的数值部分全1;

所以得到了: $-2^{(2^m-1)} \times (1-2^{-n})$

最大负数:
1. 阶符为负,也就是1;
2. 尾数的数值部分最后一位为1;
3. 阶码的数值部分全为1.

所以得到了: $-2^{-(2^m-1)} \times 2^{-n}$

最小正数以及最大正数很好分析, 最大负数的绝对值对应的就是最小正数, 最小负数的绝对值对应的就是最大正数.

**当$m=4 n =10$时,浮点数的表示范围(蓝色)**

![mark](http://image.i-ll.cc/blog/20190304/zRJYXCVG7SyJ.png)\

**一个练习题**

![mark](http://image.i-ll.cc/blog/20190304/Y4Iha4md1cq7.png)\

#### 浮点数的规格化形式

![mark](http://image.i-ll.cc/blog/20190304/1ePvFmcw3mUG.png)\

基数不同, 浮点数的规格化形式不同. 同理可以得出, $r=16$ 时, 位数最高4位不全位0. 也就是说尾数的前四位只要能表示1到16之间(不包括16)的任何数字都可以.

#### 浮点数的规格化

![mark](http://image.i-ll.cc/blog/20190304/v1SavWcX3vU6.png)\

**一个栗子,这里表示的是规格化之后浮点数的表示范围,因为$r=2$,所以小数点后面紧跟的必须是1**

![mark](http://image.i-ll.cc/blog/20190304/sq2CYCOmn7lt.png)\

**继续是栗子**

![mark](http://image.i-ll.cc/blog/20190304/CfR7NB34L6Bo.png)\

$+\frac{19}{128}$转化成二进制也就是$\frac{10011}{10000000}$, 即$0.0010011$, 因为$r=2$, 所以对它进行规格化需要保证小数点后面是1, 所以规格化之后的形式为$0.1001100000 \times 2^{-10}$. **注意,这里是2进制**

**依然是栗子**

![mark](http://image.i-ll.cc/blog/20190304/2RkopwEwsLw2.png)\
回顾一下,**移码和补码只有符号位不同.**

#### 机器零

![mark](http://image.i-ll.cc/blog/20190304/TXYNv67HeRLE.png)\

### IEEE 754标准

![mark](http://image.i-ll.cc/blog/20190304/jst8T3JXSF25.png)\